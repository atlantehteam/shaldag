trigger RecruitCycleTrigger on RecruitCycle__c (after insert, after update) {
	System.debug('RecruitCycleTrigger triggered!');
    if (trigger.size > 1) {
        System.debug('RecruitCycle triggered with ' + trigger.size + ' entities.. bailing..');
        return;
    }
    if ((trigger.isInsert && trigger.new[0].Active__c)
       || (trigger.isUpdate && trigger.new[0].Active__c && !trigger.old[0].Active__c)) {
        string entityId = trigger.new[0].Id;
        List<RecruitCycle__c> activeItems = [SELECT Id, Active__c FROM RecruitCycle__c WHERE Active__c = true AND Id != :entityId ];
        for (RecruitCycle__c scholar : activeItems) {
           scholar.Active__c = false;
           update scholar;
       }
    }
}