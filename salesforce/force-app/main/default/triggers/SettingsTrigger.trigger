trigger SettingsTrigger on Settings__c (after insert, after update) {
	System.debug('SettingsTrigger triggered!');
    if (trigger.size > 1) {
        System.debug('SettingsTrigger triggered with ' + trigger.size + ' entities.. bailing..');
        return;
    }
    if ((trigger.isInsert && trigger.new[0].Active__c)
       || (trigger.isUpdate && trigger.new[0].Active__c && !trigger.old[0].Active__c)) {
        string entityId = trigger.new[0].Id;
        List<Settings__c> activeItems = [SELECT Id, Active__c FROM Settings__c WHERE Active__c = true AND Id != :entityId ];
        for (Settings__c scholar : activeItems) {
           scholar.Active__c = false;
           update scholar;
       }
    }
}