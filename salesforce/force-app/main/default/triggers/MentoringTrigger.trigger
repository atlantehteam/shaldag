trigger MentoringTrigger on CustomObject2__c (after insert, after update) {
    System.debug('Mentoring triggered!');
    if (trigger.size > 1) {
        System.debug('mentoring triggered with ' + trigger.size + ' entities.. bailing..');
        return;
    }
    if ((trigger.isInsert && trigger.new[0].Active__c)
       || (trigger.isUpdate && trigger.new[0].Active__c && !trigger.old[0].Active__c)) {
        string entityId = trigger.new[0].Id;
        List<CustomObject2__c> activeItems = [SELECT Id, Active__c FROM CustomObject2__c WHERE Active__c = true AND Id != :entityId ];
        for (CustomObject2__c item : activeItems) {
           item.Active__c = false;
           update item;
       }
    }
}