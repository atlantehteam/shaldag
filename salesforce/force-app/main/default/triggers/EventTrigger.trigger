trigger EventTrigger on Event__c (after insert, after update, after delete, after undelete) {
	System.debug('Event triggered!');
    if (!Test.isRunningTest()) {
		WordpresssNotifierClass.handleEventChange();        
    }
}