trigger sendReceiptTrg on Opportunity (after insert, after update) {
    //note:invoke receipt only if single opportunity trigger
    if (Test.isRunningTest()) {
        return;
    }
    if (trigger.size < 2) {
        Settings__c settings = SettingsProvider.getSettings();
        List<string> receiptPaymentTypes = settings.ReceiptPaymentTypes__c.split(';');
        system.debug(receiptPaymentTypes);
        string newOpRecordTypeId = trigger.new[0].RecordTypeId;
        RecordType oppurtunityType = [SELECT DeveloperName FROM RecordType WHERE SobjectType = 'Opportunity'
                             AND Id = :newOpRecordTypeId LIMIT 1];

        if (!receiptPaymentTypes.contains(oppurtunityType.DeveloperName)) {
            system.debug(oppurtunityType.DeveloperName + ' is not marked for receipt. Bailing out.');
            return;
        }
        

        //note:automatic receipts moved to to payment success handler which generate Field1__c = false
        if ( trigger.isInsert && trigger.new[0].StageName == 'Posted' && trigger.new[0].Field1__c == true) {
            system.debug('New Opportunitiy of type ' + oppurtunityType.DeveloperName + ' inserted.');
        	ReceiptMakerCls rmaker = new ReceiptMakerCls();
        	rmaker.makeReceipt(trigger.new[0]);
        }
        if ( trigger.isUpdate && 
             trigger.new[0].StageName == 'Posted' && 
             trigger.old[0].stageName != 'Posted' && 
             trigger.new[0].Field1__c == true) {
            ReceiptMakerCls rmaker = new ReceiptMakerCls();
        	rmaker.makeReceipt(trigger.new[0]); 
        }
    }
}