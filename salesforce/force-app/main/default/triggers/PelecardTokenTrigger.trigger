trigger PelecardTokenTrigger on PelecardToken__c (after insert, after update) {
    System.debug('PelecardToken triggered!');
    if (trigger.size > 1) {
        System.debug('PelecardToken triggered with ' + trigger.size + ' entities.. bailing..');
        return;
    }
    if (trigger.isInsert) {
        string entityId = trigger.new[0].Id;
        string contactId = trigger.new[0].Contact__c;
        List<PelecardToken__c> oldTokens = [SELECT Id FROM PelecardToken__c WHERE Contact__c = :contactId AND Id != :entityId ];
        for (PelecardToken__c oldToken : oldTokens) {
           delete oldToken;
        }
    }
}