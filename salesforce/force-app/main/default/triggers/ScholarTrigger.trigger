trigger ScholarTrigger on CustomObject3__c (after insert, after update) {
    System.debug('Scolar triggered!');
    if (trigger.size > 1) {
        System.debug('scholar triggered with ' + trigger.size + ' entities.. bailing..');
        return;
    }
    if ((trigger.isInsert && trigger.new[0].Active__c)
       || (trigger.isUpdate && trigger.new[0].Active__c && !trigger.old[0].Active__c)) {
        string entityId = trigger.new[0].Id;
        List<CustomObject3__c> activeItems = [SELECT Id, Active__c FROM CustomObject3__c WHERE Active__c = true AND Id != :entityId ];
        for (CustomObject3__c scholar : activeItems) {
           scholar.Active__c = false;
           update scholar;
       }
    }
}