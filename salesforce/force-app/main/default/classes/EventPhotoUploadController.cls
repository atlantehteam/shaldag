public with sharing class EventPhotoUploadController {

    public boolean displaying { get; set; }
    public Event__c pageEvent;
    public Transient Blob profilePicFile { get; set; }
    public String profilePicName { get; set; }
    public Id currentPicture { get; set; }
    public ContentVersion currentVersion {get; set;}
    
    /** Constructor, grab record, and check/load an existing photo */
    public EventPhotoUploadController(ApexPages.StandardController controller) {
        pageEvent = (Event__c)controller.getRecord();
        displaying = true;
        List<ContentVersion> currentPictures = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Title = :pageEvent.Id and IsLatest = true LIMIT 1];
        if(currentPictures.size() != 0) {
            currentVersion = currentPictures.get(0);
            currentPicture = currentVersion.Id;
        }
    }

    /** toggle switches between the photo display and photo upload form */
    public void toggle() {
        displaying = !displaying;
    }
    
    /** saveFile clears any existing profile picture, retrieves the data from the form, and saves it under the relevant filename*/
    Public Pagereference saveFile() {      
        Id conDocId = null;
        
        // Now, we save the new blob
        ContentVersion conVer = new ContentVersion();
        conVer.ContentLocation = 'S'; // to use S specify this document is in Salesforce, to use E for external files
        conVer.PathOnClient = profilePicName; // The files name, extension is very important here which will help the file in preview.
        conVer.Title = pageEvent.ID; // Display name of the files
        conVer.VersionData = profilePicFile; // converting your binary string to Blog
        if (currentVersion != null) {
            conDocId = currentVersion.ContentDocumentId;
            conVer.ContentDocumentId = conDocId;
        }
        insert conVer; //Insert ContentVersion
        
        if (conDocId == null) {
            conDocId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: conVer.id LIMIT 1].ContentDocumentId;
        }

        //create ContentDocumentLink  record
        if (currentVersion == null) {
            ContentDocumentLink conDocLink = New ContentDocumentLink();
            conDocLink.LinkedEntityId = pageEvent.Id; // Specify RECORD ID here i.e Any Object ID (Standard Object/Custom Object)
            conDocLink.ContentDocumentId = conDocId; 
            conDocLink.shareType = 'V';
            insert conDocLink;
        }

        ContentDistribution cd = new ContentDistribution();
        cd.Name = pageEvent.ID;
        cd.ContentVersionId = conVer.id;
        cd.PreferencesAllowViewInBrowser = true;
        cd.PreferencesLinkLatestVersion = true;
        cd.PreferencesNotifyOnVisit = false;
        cd.PreferencesPasswordRequired = false;
        cd.PreferencesAllowOriginalDownload = true;
        insert cd;
                
        pageEvent.Image__c = [SELECT Id, ContentDownloadUrl FROM ContentDistribution WHERE ContentVersionId =: conVer.id].ContentDownloadUrl;
        pageEvent.ImageName__c = profilePicName;
        update pageEvent;
        
        currentPicture = conVer.Id;
        displaying = true;
        return null;
    }
}