global class PaymentProcessor {
    public PelecardResponse makePayment(string creditCard, string mmyyExp, string cvv, decimal amountAgorot) {
        return makePayment(creditCard, mmyyExp, cvv, amountAgorot, null);
    }
    
    public PelecardResponse makePayment(string creditCard, string mmyyExp, string cvv, decimal amountAgorot, Contact contact) {
        DebitRegularTypePayload debitPayload = new DebitRegularTypePayload();
		debitPayload.creditCard = creditCard;
        debitPayload.creditCardDateMmYy = mmyyExp;
        debitPayload.cvv2 = cvv;
        debitPayload.total = amountAgorot;
        return makePayment(debitPayload, amountAgorot, contact);
    }
    
    public PelecardResponse makeTokenPayment(string token, string cvv, decimal amountAgorot) {
        return makeTokenPayment(token, cvv, amountAgorot, null);
    }
    public PelecardResponse makeTokenPayment(string token, string cvv, decimal amountAgorot, Contact contact) {
        DebitRegularTypePayload debitPayload = new DebitRegularTypePayload();
		debitPayload.token = token;
        debitPayload.total = amountAgorot;
        debitPayload.cvv2 = cvv;
        return makePayment(debitPayload, amountAgorot, contact);
    }
    
    private PelecardResponse makePayment(DebitRegularTypePayload debitPayload, decimal amountAgorot, Contact contact) {
        Settings__c settings = SettingsProvider.getSettings();
        debitPayload.terminalNumber = settings.TerminalNumberInPelecard__c;
        debitPayload.user = settings.UsernameInPelecard__c;
        debitPayload.password = settings.PasswordInPelecard__c;
        
        string debitJson = JSON.serialize(debitPayload);
		debitJson = debitJson.replace('currency_x', 'currency');
        HttpRequest req = new HttpRequest();
        req.setEndpoint('callout:pelecard/services/DebitRegularType');
        req.setMethod('POST');
        req.setbody(debitJson);
        req.setTimeout(30000);
        Http http = new Http();
        HTTPResponse response;
        try {
            response = http.send(req); 
        } catch (Exception e) {
            System.debug('Exception type caught: ' + e.getTypeName()); 
            System.debug('Message: ' + e.getMessage()); 
            System.debug('Cause: ' + e.getCause());
            System.debug('Line number: ' + e.getLineNumber()); 
            System.debug('Stack trace: ' + e.getStackTraceString()); 

            sendFailureEmail(contact, 'יצירת טוקן למשתמש בפלאקרד נכשלה', e.getMessage());
            PelecardResponse pelecardRes = new PelecardResponse();
            pelecardRes.StatusCode = '-10';
            pelecardRes.ErrorMessage = e.getMessage();
            return pelecardRes;
        }
        PelecardResponse pelecardRes = (PelecardResponse) JSON.deserialize(response.getBody(), PelecardResponse.class);
        pelecardRes.FormattedMessage = CObjectsNames.CreditCardErrorsMap.get(pelecardRes.StatusCode);
        handleFailure(pelecardRes, contact, 'תשלום נכשל בפלאקארד');
        
        return pelecardRes;
    }
    
    public PelecardResponse convertToToken(string creditCard, string mmyyExp, Contact contact) {
        Settings__c settings = SettingsProvider.getSettings();
        ConvertToTokenPayload payload = new ConvertToTokenPayload();
        payload.terminalNumber = settings.TerminalNumberInPelecard__c;
        payload.user = settings.UsernameInPelecard__c;
        payload.password = settings.PasswordInPelecard__c;
		payload.creditCard = creditCard;
        payload.creditCardDateMmYy = mmyyExp;
        
        string payloadJson = JSON.serialize(payload);
        HttpRequest req = new HttpRequest();
        req.setEndpoint('callout:pelecard/services/ConvertToToken');
        req.setMethod('POST');
        req.setbody(payloadJson);
        req.setTimeout(60000);
        Http http = new Http();
        HTTPResponse response;
        try {
            response = http.send(req); 
        } catch (Exception e) {
            sendFailureEmail(contact, 'יצירת טוקן למשתמש בפלאקרד נכשלה', e.getMessage());
            PelecardResponse pelecardRes = new PelecardResponse();
            pelecardRes.StatusCode = '-10';
            pelecardRes.ErrorMessage = e.getMessage();
            pelecardRes.FormattedMessage = 'Timeout Exceeded';
            return pelecardRes;
        }
        
        PelecardResponse pelecardRes = (PelecardResponse) JSON.deserialize(response.getBody(), PelecardResponse.class);
        pelecardRes.FormattedMessage = CObjectsNames.CreditCardErrorsMap.get(pelecardRes.StatusCode);
        handleFailure(pelecardRes, contact, 'יצירת טוקן למשתמש בפלאקרד נכשלה');
        
        return pelecardRes;
    }
    
    private void handleFailure(PelecardResponse pelecardRes, Contact contact, string issue) {
        if (pelecardRes.StatusCode == '000') {
            return;
        }
        string err = CObjectsNames.CreditCardErrorsMap.get(pelecardRes.StatusCode);
        sendFailureEmail(contact, issue, err);
    }

    private void sendFailureEmail(Contact contact, string issue, string cause) {
        Settings__c settings = SettingsProvider.getSettings();
        OrgWideEmailAddress orgAddress = [select id, Address, DisplayName from OrgWideEmailAddress limit 1];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		mail.setToAddresses(new list<string> { settings.EmailInCaseOfRefusalMflakard__c });
        mail.setSubject('אתר תשלומים');
        mail.setOrgWideEmailAddressId(orgAddress.Id);
        string body = issue + ':<br/>סיבה: ' + cause + '<br/>';
        if (contact != null) {
            body += 'שם: ' + contact.Name + '<br/>מייל:' + contact.Email + '<br/>טלפון נייד: ' + contact.MobilePhone;
        }
		mail.setHtmlBody(body);
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }

    global class DebitRegularTypePayload {
         public string terminalNumber {get; set;}
         public string user {get; set;}
         public string password {get; set;}
         public string shopNumber {get; set;}
         public string creditCard {get; set;}
         public string token {get; set;}
         public string creditCardDateMmYy {get; set;}
         public decimal total {get; set;}
         public string currency_x {get; set;}
         public string cvv2 {get; set;}
         public DebitRegularTypePayload() {
             shopNumber = '001';
             currency_x = '1';
             token = '';
         }
    }
    
    global class ConvertToTokenPayload {
         string terminalNumber {get; set;}
         string user {get; set;}
         string password {get; set;}
         string shopNumber {get; set;}
         string creditCard {get; set;}
         string creditCardDateMmYy {get; set;}
         string addFourDigits {get; set;}
         public ConvertToTokenPayload() {
             shopNumber = '001';
             addFourDigits = 'false';
         }
    }
}