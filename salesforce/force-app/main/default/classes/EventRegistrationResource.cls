@RestResource(urlMapping='/registerEvent/*')
global with sharing class EventRegistrationResource {
    @HttpPost
	global static void doPost(ResourcePayload registration) {
        ResourcePayload res = registration;
        PelecardSettings__c settings = PelecardSettings__c.getOrgDefaults();
        if (!validatePayload(res, settings)) {
            return;
        }

        Contact contact = (Contact) CSobjectDb.GetSobjectById(res.contactId, Contact.SobjectType);
        if (res.amount > 0) {
            PaymentProcessor processor = new PaymentProcessor();
        	PelecardResponse pelecardRes = processor.makePayment(res.creditCard, res.creditCardDateMmYy, res.cvv, res.amount, contact);
            if (pelecardRes.StatusCode != '000') {
            	handlePaymentFailure(pelecardRes);
                return;
            }
        }
        handlePaymentSuccess(res);
    }
    
    private static void handlePaymentSuccess(ResourcePayload resource) {       
        (new PaymentSuccessHandler()).handleEventPayment(resource.contactId, resource.eventId, resource.amount, resource.numParticipants);      
        sendResponse(new ApiResponse());
    }
    private static void handlePaymentFailure(PelecardResponse pelecardRes) {
        Map<String, String> errors = new Map<String, String>();
        errors.put('paymentFailure', pelecardRes.FormattedMessage);
        handleErrors(errors);
    }
   
    private static boolean validatePayload(ResourcePayload resource, PelecardSettings__c settings) {
        Map<String, String> errors = new Map<String, String>();

        list<Contact> matchingContacts = [Select Id from Contact where Id = :resource.contactId limit 1];
        if (matchingContacts.IsEmpty()) {
            errors.put('contactId', 'invalidContact');
        }
        
        list<Event__c> matchingEvents = [Select Id, Cost__c from Event__c where Id = :resource.eventId limit 1];
        if (matchingEvents.IsEmpty()) {
            errors.put('eventId', 'invalidEvent');
        } else if (resource.amount != matchingEvents[0].Cost__c * resource.numParticipants) {
            errors.put('amount', 'סכום תשלום לא חוקי');
        }
        return handleErrors(errors);
    }
	
    private static void sendResponse(ApiResponse resp) {
        new ApiResponder().sendResponse(resp);
    }
   
    private static boolean handleErrors(Map<String, String> errors) {
        if (errors.isEmpty()) {return true;}
        ApiResponse resp = new ApiResponse(errors);
        sendResponse(resp);
        return false;
    }
    
    global class ResourcePayload {
        public string creditCard {get; set;}
        public string creditCardDateMmYy {get; set;}
        public string cvv {get; set;}
        public string contactId {get; set;}
        public string eventId {get; set;}
        public decimal amount {get; set;}
        public decimal numParticipants {get; set;}
    }
}