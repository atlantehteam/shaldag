global class PelecardResponse {
    public string StatusCode {get; set;}
    public string ErrorMessage {get; set;}
    public string FormattedMessage {get; set;}
    public Map<string, string> ResultData {get; set;}
}