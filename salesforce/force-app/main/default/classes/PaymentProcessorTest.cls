@isTest(SeeAllData=true)
public class PaymentProcessorTest {
    static string expectedCallout = '';
    static string expectedCard = '';
    static string expectedExpiration = '';
    static string expectedCvv = '';
    static string expectedToken = '';
    
    static Map<string, string> expectedResultData;
    
    static void prepareContext(string reqType) {
        RestRequest req = new RestRequest();
        req.addParameter('type', reqType);
        
        RestResponse res = new RestResponse();
        req.addHeader('httpMethod', 'POST');       
        String postData = '{}';
        String JsonMsg=JSON.serialize(postData);
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req; 
        RestContext.response = res;
    }
    
    static ApiResponse getResponse() {
        String resString = RestContext.response.responseBody.toString();
        ApiResponse apiResponse = (ApiResponse) JSON.deserialize(resString, ApiResponse.class);
        return apiResponse;
    }

    
    static testMethod void validatePaymentFlow() {
		
        Test.setMock(HttpcalloutMock.class, new MockGenerator_Test());
        expectedCallout = 'callout:pelecard/services/DebitRegularType';
        expectedCard = '123456';
        expectedExpiration = '1020';
		expectedCvv = '111';
        expectedToken = '';
        
        prepareContext('membership');
        
        Test.startTest();
		
        PaymentProcessor processor = new PaymentProcessor();
        PelecardResponse res = processor.makePayment(expectedCard, expectedExpiration, expectedCvv, 12000);
        System.assertEquals('000', res.StatusCode);
        
        Test.stopTest();
    }
    
    static testMethod void validateTokenFlow() {
		expectedCard = '333333';
        expectedExpiration = '1023';
		expectedCvv = null;
		expectedToken = null;
        
        Test.setMock(HttpcalloutMock.class, new MockGenerator_Test());
        expectedCallout = 'callout:pelecard/services/ConvertToToken';
        
        string token = '5555';
        expectedResultData = new Map<string, string>();
        expectedResultData.put('Token', expectedToken);
        
        prepareContext('membership');
        
        Test.startTest();
		
        PaymentProcessor processor = new PaymentProcessor();
        PelecardResponse res = processor.convertToToken(expectedCard, expectedExpiration, NULL);
        System.assertEquals('000', res.StatusCode);
        System.assertEquals(expectedToken, res.ResultData.get('Token'));
        
        expectedCallout = 'callout:pelecard/services/DebitRegularType';
        expectedCard = null;
        expectedExpiration = null;
		expectedCvv = '123';
        expectedToken = token;
        res = processor.makeTokenPayment(token, expectedCvv, 12000);
        Test.stopTest();
    }
    
    public class MockGenerator_Test implements HttpCalloutMock {
        public httpResponse respond(HTTPrequest req) {
            
            string body = req.getBody();
            PaymentProcessor.DebitRegularTypePayload payload = (PaymentProcessor.DebitRegularTypePayload) JSON.deserialize(body, PaymentProcessor.DebitRegularTypePayload.class);
            
            System.assertEquals(expectedCallout, req.getEndpoint());
            System.assertEquals('POST', req.getMethod());
            System.assertEquals(expectedCard, payload.creditCard);
            System.assertEquals(expectedExpiration, payload.creditCardDateMmYy);
            System.assertEquals(expectedCvv, payload.cvv2);
            System.assertEquals(expectedToken, payload.token);

            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(200);
            
            PelecardResponse pelecardRes = new PelecardResponse();
            pelecardRes.StatusCode = '000';
            pelecardRes.ResultData = expectedResultData;
            res.setBody(JSON.serialize(pelecardRes));
            return res;
        }
	}
}