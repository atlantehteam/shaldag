global class SettingsProvider {
	private static Settings__c settings;
    public static Settings__c getSettings() {
        if (settings == null) {
            list<Settings__c> dbSettings = CSobjectDb.GetSobjectByFilter('Active__c = True limit 1', Settings__c.SobjectType);
            if (!dbSettings.IsEmpty()) {
                settings = dbSettings.get(0);
            } else if (test.isRunningTest()) {
                settings = new Settings__c();
            } else {
                throw new DmlException('No Settings__c found!');
            }
            
        }
        return settings;
    }
}