global class ApiResponder {   
    public void sendResponse(ApiResponse resp) {
        string json = JSON.Serialize(resp);
        RestContext.response.addHeader('Content-Type', 'application/json');
        RestContext.response.responseBody = Blob.valueOf(json);
    }
}