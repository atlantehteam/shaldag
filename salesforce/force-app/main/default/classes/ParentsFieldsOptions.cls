public enum ParentsFieldsOptions
{
	AllFields, 
	CustomFields,
	SystemFields,
	None
}