@isTest(SeeAllData=true)
public class ReceiptMakerTest {

    static testMethod void makeReceipts_TEST() {
        account acc = new account();
        acc.name = 'test';
        insert acc;
        
        contact con = new contact();
        con.lastName = 'testLast';
        con.firstName = 'testLast';
        con.MailingStreet = 'testMailingStreet';
        con.MailingCity = 'testCity';
        con.mailingPostalCode='1234';
        con.email='eyal.kama@gmail.com';
        con.AccountId =  acc.id;
        insert con;
        
        opportunity opp = new opportunity();
        opp.name = 'test';
        opp.CloseDate=system.today();
        opp.StageName='Posted';
        opp.AccountId=acc.id;
        opp.Amount=100;
        opp.email__c='eyal.kama@gmail.com';
        insert opp;
    }
    
}