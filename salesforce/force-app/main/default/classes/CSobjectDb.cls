public virtual class CSobjectDb
{
	public Sobject pSobject { get; set; }
	public Schema.Sobjecttype pSobjectType { get; set; }
	
	public CSobjectDb(ApexPages.StandardController controller)
	{
		pSobject = controller.getRecord();
		pSobjectType = pSobject.getSObjectType();
		if (pSobject.id != null)
			pSobject = GetSobjectAndCustomParentsFieldsById(pSobject.id, pSobjectType);
	}
	
	public CSobjectDb(ApexPages.StandardController controller, ParentsFieldsOptions parentsFields)
	{
		pSobject = controller.getRecord();
		pSobjectType = pSobject.getSObjectType();
		if (pSobject.id != null)
		{
			if (parentsFields == ParentsFieldsOptions.AllFields)
				pSobject = GetSobjectAndAllParentsFieldsById(pSobject.id, pSobjectType);
			else if (parentsFields == ParentsFieldsOptions.None)
				pSobject = GetSobjectById(pSobject.id, pSobjectType);
			else if (parentsFields == ParentsFieldsOptions.SystemFields)
				pSobject = GetSobjectAndSystemParentsFieldsById(pSobject.id, pSobjectType);
			else
				pSobject = GetSobjectAndCustomParentsFieldsById(pSobject.id, pSobjectType);
		}
	}
	
	public virtual void save()
	{
		if (pSobject.id != null)
			update pSobject;
		else
			insert pSobject;
	}
	
	public string GetPageTitle(boolean isMale)
	{
		string pageTitle;
		if (pSobject.id == null)
		{
			pageTitle = pSobject.getSObjectType().getDescribe().getLabel() + ' חדש';
			if(!isMale)
				pageTitle +='ה';
		}
		else
			pageTitle = string.valueOf(pSobject.get('Name'));
			
		return pageTitle;
	}
	
	public static Sobject GetSobjectById(string id, Schema.Sobjecttype sobjectType)
	{
		string query = ' select ' + GetFieldsStrForSobject(sobjectType);
		query += ' from ' + sobjectType + ' where id = \'' + id + '\'';
		return Database.query(query);
	}
	
	public static Sobject GetSobjectById(string id, string extraFields, Schema.Sobjecttype sobjectType)
	{
		string query = ' select ' + GetFieldsStrForSobject(sobjectType) + (extraFields != null && extraFields.trim() != '' ? ', ' + extraFields : '');
		query += ' from ' + sobjectType + ' where id = \'' + id + '\'';
		return Database.query(query);
	}
	
	public static Sobject GetSobjectByIdWithCustomParents(string id, Schema.Sobjecttype sobjectType)
	{
		string query = ' select ' + GetFieldsWithCustomParentsStrForSobject(sobjectType);
		query += ' from ' + sobjectType + ' where id = \'' + id + '\'';
		return Database.query(query);
	}
	
	public static Sobject GetSobjectByIdWithSystemParents(string id, Schema.Sobjecttype sobjectType)
	{
		string query = ' select ' + GetFieldsWithSystemParentsStrForSobject(sobjectType);
		query += ' from ' + sobjectType + ' where id = \'' + id + '\'';
		return Database.query(query);
	}
	
	public static Sobject GetSobjectByIdWithChildrenRelationship(string id, Schema.Sobjecttype sobjectType)
	{
		string query = ' select ' + GetFieldsStrForSobject(sobjectType);
		string childrenStr = GetCustomChildrenRelationshipFields(sobjecttype);
		query += (childrenStr == '' ? '' : ', ' + childrenStr) + ' from ' + sobjectType.getDescribe().getName() + ' where id = \'' + id + '\'';
		return Database.query(query);
	}
	
	public static Sobject GetSobjectByIdWithChildrenRelationshipAndParents(string id, Schema.Sobjecttype sobjectType)
	{
		string query = ' select ' + GetFieldsWithParentsStrForSobject(sobjectType);
		string childrenStr = GetCustomChildrenRelationshipFields(sobjecttype);
		query += (childrenStr == '' ? '' : ', ' + childrenStr) + ' from ' + sobjectType.getDescribe().getName() + ' where id = \'' + id + '\'';
		return Database.query(query);
	}
	
	public static string GetFieldsStrForSobject(Schema.Sobjecttype sobjectType)
	{
		list<Schema.SObjectField> fieldsList = sobjectType.getDescribe().fields.getMap().values();
		string ret = '';
		for (Schema.SObjectField field : fieldsList)
		{
			string fieldName = CObjectsNames.GetFieldNameSoql(field);
			ret += ret == '' ? fieldName : ', ' + fieldName;
			list<Schema.sObjectType> relatedTo = field.getDescribe().getReferenceTo();
			if (fieldName != 'RecurrenceActivityId' && relatedTo.size() == 1 && fieldName != 'BusinessProcessId')
			{
				fieldName = fieldName.endsWith('__c') ? fieldName.substring(0, fieldName.length() - 3) + '__r.' : fieldName;
				fieldName = fieldName.endsWith('Id') ? fieldName.substring(0, fieldName.length() - 2) + '.' : fieldName;
				ret += ', ' + fieldName + 'Name';
			}
		}
		return ret;
	}
	
	public static string GetFieldsWithParentsStrForSobject(Schema.Sobjecttype sobjecttype)
	{
		list<Schema.SObjectField> fieldsList = sobjecttype.getDescribe().fields.getMap().values();
		string ret = '';
		for (Schema.SObjectField field : fieldsList)
		{
			string fieldName = CObjectsNames.GetFieldNameSoql(field);
			ret += ret == '' ? fieldName : ', ' + fieldName;
			list<Schema.sObjectType> relatedTo = field.getDescribe().getReferenceTo();
			if (fieldName != 'RecurrenceActivityId' && relatedTo.size() == 1 && fieldName != 'BusinessProcessId')
			{
				list<Schema.SObjectField> relatedToFieldsList = relatedTo[0].getDescribe().fields.getMap().values();
				fieldName = fieldName.endsWith('__c') ? fieldName.substring(0, fieldName.length() - 3) + '__r.' : fieldName;
				fieldName = fieldName.endsWith('Id') ? fieldName.substring(0, fieldName.length() - 2) + '.' : fieldName;
				for (Schema.SObjectField relatedField : relatedToFieldsList)
				{
					ret += ', ' + fieldName + CObjectsNames.GetFieldNameSoql(relatedField);
				}
			}
		}
		return ret;
	}
	
	public static string GetFieldsWithCustomParentsStrForSobject(Schema.Sobjecttype sobjecttype)
	{
		list<Schema.SObjectField> fieldsList = sobjecttype.getDescribe().fields.getMap().values();
		string ret = '';
		for (Schema.SObjectField field : fieldsList)
		{
			string fieldName = CObjectsNames.GetFieldNameSoql(field);
			ret += ret == '' ? fieldName : ', ' + fieldName;
			list<Schema.sObjectType> relatedTo = field.getDescribe().getReferenceTo();
			if (fieldName != 'RecurrenceActivityId' && relatedTo.size() == 1 && fieldName != 'BusinessProcessId')
			{
				if (fieldName.endsWith('__c'))
				{
					list<Schema.SObjectField> relatedToFieldsList = relatedTo[0].getDescribe().fields.getMap().values();
					fieldName = fieldName.substring(0, fieldName.length() - 3) + '__r.';
					for (Schema.SObjectField relatedField : relatedToFieldsList)
					{
						ret += ', ' + fieldName + CObjectsNames.GetFieldNameSoql(relatedField);
					}
				}
				else if (fieldName.endsWith('Id'))
				{
					fieldName = fieldName.substring(0, fieldName.length() - 2) + '.';
					ret += ', ' + fieldName + 'Name';
				}
			}
		}
		return ret;
	}
	
	public static string GetFieldsWithSystemParentsStrForSobject(Schema.Sobjecttype sobjecttype)
	{
		list<Schema.SObjectField> fieldsList = sobjecttype.getDescribe().fields.getMap().values();
		string ret = '';
		for (Schema.SObjectField field : fieldsList)
		{
			string fieldName = CObjectsNames.GetFieldNameSoql(field);
			ret += ret == '' ? fieldName : ', ' + fieldName;
			list<Schema.sObjectType> relatedTo = field.getDescribe().getReferenceTo();
			if (fieldName != 'RecurrenceActivityId' && relatedTo.size() == 1 && fieldName != 'BusinessProcessId')
			{
				if (fieldName.endsWith('Id'))
				{
					list<Schema.SObjectField> relatedToFieldsList = relatedTo[0].getDescribe().fields.getMap().values();
					fieldName = fieldName.substring(0, fieldName.length() - 2) + '.';
					for (Schema.SObjectField relatedField : relatedToFieldsList)
					{
						ret += ', ' + fieldName + CObjectsNames.GetFieldNameSoql(relatedField);
					}
				}
				else if (fieldName.endsWith('__c'))
				{
					fieldName = fieldName.substring(0, fieldName.length() - 3) + '__r.';
					ret += ', ' + fieldName + 'Name';
				}
			}
		}
		return ret;
	}
	
	public static Sobject GetSobjectAndCustomParentsFieldsById(string id, Schema.Sobjecttype sobjectType)
	{
		try
		{
			string query = ' select ' + GetFieldsWithCustomParentsStrForSobject(sobjectType);
			query += ' from ' + sobjectType + ' where id = \'' + id + '\'';
			return Database.query(query);
		}
		catch(Exception e)
		{
			return null;
		}
	}
	
	public static Sobject GetSobjectAndSystemParentsFieldsById(string id, Schema.Sobjecttype sobjectType)
	{
		try
		{
			string query = ' select ' + GetFieldsWithSystemParentsStrForSobject(sobjectType);
			query += ' from ' + sobjectType + ' where id = \'' + id + '\'';
			return Database.query(query);
		}
		catch(Exception e)
		{
			return null;
		}
	}
	
	public static Sobject GetSobjectAndAllParentsFieldsById(string id, Schema.Sobjecttype sobjectType)
	{
		try
		{
			string query = ' select ' + GetFieldsWithParentsStrForSobject(sobjectType);
			query += ' from ' + sobjectType + ' where id = \'' + id + '\'';
			return Database.query(query);
		}
		catch(Exception e)
		{
			return null;
		}
	}
	
	public static list<Sobject> GetSobjectByFilter(string filter, Schema.Sobjecttype sobjectType)
	{
		return GetSobjectByFilter(filter, '', sobjectType);
	}
	
	public static list<Sobject> GetSobjectByFilter(string filter, string orderBy, Schema.Sobjecttype sobjectType)
	{
		return GetSobjectByFilter(filter, orderBy, '', sobjectType);
	}
	
	public static list<Sobject> GetSobjectByFilter(string filter, string orderBy, string extraFields, Schema.Sobjecttype sobjectType)
	{
		try
		{
			string query = ' select ' + GetFieldsStrForSobject(sobjectType);
			if (filter == null || filter.trim() == '')
				filter = '';
			else if (!filter.trim().startsWith('where'))
				filter = ' where ' + filter;
			if (orderBy == null || orderBy.trim() == '')
				orderBy = '';
			else if (!orderBy.trim().startsWith('order by'))
				orderBy = ' order by ' + orderBy;
			if (extraFields == null || extraFields.trim() == '')
				extraFields = '';
			else if (!extraFields.startsWith(','))
				extraFields = ',' + extraFields;
			query += extraFields + ' from ' + sobjectType + ' ' + filter + ' ' + orderBy;
			return Database.query(query);
		}
		catch(Exception e)
		{
			return new list<Sobject>();
		}
	}
	
	public static list<Sobject> GetSobjectByFilterWithCustomParents(string filter, string orderBy, Schema.Sobjecttype sobjectType)
	{
		string query = ' select ' + GetFieldsWithCustomParentsStrForSobject(sobjectType);
		if (filter == null)
			filter = '';
		else if (!filter.trim().startsWith('where'))
			filter = ' where ' + filter;
		if (orderBy == null)
			orderBy = '';
		else if (!orderBy.trim().startsWith('order by'))
			orderBy = ' order by ' + orderBy;
		query += ' from ' + sobjectType + ' ' + filter + ' ' + orderBy;
		return Database.query(query);
	}
	
	public static list<Sobject> GetSobjectByFilterWithCustomParents(string filter, Schema.Sobjecttype sobjectType)
	{
		string query = ' select ' + GetFieldsWithCustomParentsStrForSobject(sobjectType);
		if (filter == null)
			filter = '';
		else if (!filter.trim().startsWith('where'))
			filter = ' where ' + filter;
		query += ' from ' + sobjectType + ' ' + filter;
		return Database.query(query);
	}
	
	public static list<Sobject> GetSobjectByFilterWithChildrenRelationshipAndParents(string filter, Schema.Sobjecttype sobjectType)
	{
		string query = ' select ' + GetFieldsWithParentsStrForSobject(sobjectType);
		string childrenStr = GetCustomChildrenRelationshipFields(sobjecttype);
		if (filter == null)
			filter = '';
		else if (!filter.trim().startsWith('where'))
			filter = ' where ' + filter;
		query += (childrenStr == '' ? '' : ', ' + childrenStr) + ' from ' + sobjectType.getDescribe().getName() + filter;
		return Database.query(query);
	}
	
	public static list<Sobject> GetSobjectByFilterWithChildrenRelationshipAndCustomParents(string filter, Schema.Sobjecttype sobjectType)
	{
		string query = ' select ' + GetFieldsWithCustomParentsStrForSobject(sobjectType);
		string childrenStr = GetCustomChildrenRelationshipFields(sobjecttype);
		if (filter == null)
			filter = '';
		else if (!filter.trim().startsWith('where'))
			filter = ' where ' + filter;
		query += (childrenStr == '' ? '' : ', ' + childrenStr) + ' from ' + sobjectType.getDescribe().getName() + filter;
		return Database.query(query);
	}
	
	public static list<Sobject> GetSobjectByFilterWithChildrenRelationshipAndSystemParents(string filter, Schema.Sobjecttype sobjectType)
	{
		string query = ' select ' + GetFieldsWithSystemParentsStrForSobject(sobjectType);
		string childrenStr = GetCustomChildrenRelationshipFields(sobjecttype);
		if (filter == null)
			filter = '';
		else if (!filter.trim().startsWith('where'))
			filter = ' where ' + filter;
		query += (childrenStr == '' ? '' : ', ' + childrenStr) + ' from ' + sobjectType.getDescribe().getName() + filter;
		return Database.query(query);
	}
	
	public static list<Sobject> GetSobjectByFilterWithChildrenRelationship(string filter, Schema.Sobjecttype sobjectType)
	{
		string query = ' select ' + GetFieldsStrForSobject(sobjectType);
		string childrenStr = GetCustomChildrenRelationshipFields(sobjecttype);
		if (filter == null)
			filter = '';
		else if (!filter.trim().startsWith('where'))
			filter = ' where ' + filter;
		query += (childrenStr == '' ? '' : ', ' + childrenStr) + ' from ' + sobjectType.getDescribe().getName() + filter;
		return Database.query(query);
	}
	
	public static string GetCustomChildrenRelationshipFields(Schema.Sobjecttype sobjecttype)
	{
		list<Schema.ChildRelationship> childrenRelationship = sobjecttype.getDescribe().getChildRelationships();
		string ret = '';
		for (Schema.ChildRelationship relationship : childrenRelationship)
		{
			string relationshipName = relationship.getRelationshipName();
			if (relationshipName != null && relationshipName.endsWith('__r'))
			{
				string relationshipStr = '(select ' + GetFieldsStrForSobject(relationship.getChildSObject())
										 + ' from ' + relationshipName + ')';
				ret += ret == '' ? relationshipStr : ', ' + relationshipStr;
			}
		}
		return ret;
	}
	
	public string mRetUrl;
	public string pRetUrl
	{
		get
		{
			if (mRetUrl == null)
			{
				mRetUrl = apexpages.currentpage().getParameters().get('RetUrl');
				if (mRetUrl == null || mRetUrl.trim() == '')
				{
					mRetUrl = null;
					return CObjectsNames.GetBaseUrl() + '/' + pSobject.id;
				}
			}
			return mRetUrl;
		}
	}
}