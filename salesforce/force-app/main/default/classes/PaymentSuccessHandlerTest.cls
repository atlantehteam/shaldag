@isTest(SeeAllData=true)
public class PaymentSuccessHandlerTest {
    static testMethod void validateInsertingOpportunitiesForPayment() {
        decimal donationAmount = 180;
        decimal amount = 110;
        
        Contact c = new Contact();
        c.FirstName = 'cFirst';
        c.LastName = 'cLast';
        c.Email = 'e@e.com';
        insert c;
        
        PaymentSuccessHandler handler = new PaymentSuccessHandler();
        handler.handle(c.Id, amount, donationAmount);
        
        RecordType grantRecordType = [select Id from RecordType where SobjectType = 'Opportunity' and DeveloperName = 'Grant' limit 1];
        RecordType memberRecordType = [select Id from RecordType where SobjectType = 'Opportunity' and DeveloperName = 'Membership' limit 1];
            
        Contact dbContact = [SELECT Id, Field12__c, AccountId from Contact WHERE Id = :c.Id];
        List<Opportunity> donationOps = [SELECT Id, StageName, Amount from Opportunity WHERE AccountId = :dbContact.AccountId AND RecordTypeId = :grantRecordType.Id];
        System.assertEquals(true, dbContact.Field12__c.Contains('תורם'));
        System.assertEquals(1, donationOps.size());
        System.assertEquals(donationAmount, donationOps.get(0).Amount);
        
        List<Opportunity> memberOps = [SELECT Id, StageName, Amount from Opportunity WHERE AccountId = :dbContact.AccountId AND RecordTypeId = :memberRecordType.Id];
        System.assertEquals(true, dbContact.Field12__c.Contains('חבר עמותה'));
        System.assertEquals(1, memberOps.size());
        System.assertEquals(amount, memberOps.get(0).Amount);
    }
    
    static testMethod void validateInsertingOpportunitiesForEvent() {
        decimal amount = 130;
        decimal participants = 5;
        
        Contact c = new Contact();
        c.FirstName = 'cFirst';
        c.LastName = 'cLast';
        c.Email = 'e@e.com';
        insert c;
        
        Event__c event = new Event__c();
        event.Name = 'Test Event';
        insert event;
        
        PaymentSuccessHandler handler = new PaymentSuccessHandler();
        handler.handleEventPayment(c.Id, event.Id, amount, participants);
        
        RecordType eventRecordType = [select Id from RecordType where SobjectType = 'Opportunity' and DeveloperName = 'Event' limit 1];
            
        Contact dbContact = [SELECT Id, Field12__c, AccountId from Contact WHERE Id = :c.Id];
        List<Opportunity> eventOps = [SELECT Id, StageName, Amount from Opportunity WHERE AccountId = :dbContact.AccountId AND RecordTypeId = :eventRecordType.Id];
        EventRegistration__c registration = [SELECT Id, Event__c, NumParticipants__c, Opportunity__c from EventRegistration__c WHERE Contact__c = :c.Id];
        
        System.assertEquals(1, eventOps.size());
        System.assertEquals(amount, eventOps.get(0).Amount);
        System.assertEquals(eventOps.get(0).Id, registration.Opportunity__c);
        System.assertEquals(participants, registration.NumParticipants__c);
        System.assertEquals(event.Id, registration.Event__c);
    }
}