public class ReceiptMakerCls implements Queueable {

    private Opportunity opp = null;

    public ReceiptMakerCls() {}
    
    public ReceiptMakerCls(Opportunity opp) {
        this.opp = opp;
    }

    public void execute(QueueableContext context) {
        makeReceipt(opp);   
    }

    public void makeReceipt(opportunity opp) {
    	system.debug('Making receipt from opportunity ' + opp.Name);
        List<Account> mContact = [select id , (select lastName,firstName,MailingStreet,MailingCity,mailingPostalCode,email from account.contacts where email=:opp.email__c)
                              	 from account where id = :opp.accountid];
        
         //List<Account> mContact = [select id , (select lastName,firstName,MailingStreet,MailingCity,mailingPostalCode,email from account.contacts)

        Contact con;
        try {
            con = mContact[0].contacts[0]; //expect a single contact for the opportunity's account
            //BELOW LINE ADDED
            //con.email = opp.email__c;
        } catch (ListException le) {
            string errorMsg = 'Invalid Opportunity: ' + opp.Id + ' It is not tied to an account. Cannot send receipt';	
            ReceiptMakerCls.sendFailureEmail(errorMsg, le.getMessage());
 			return;
        }
        
        string api_token = SettingsProvider.getSettings().api_token__c;
            
        string comments = '';
        if ( Schema.SObjectType.Opportunity.RecordTypeInfosByName.get('תרומה').RecordTypeId == opp.recordtypeid)
           comments = 'למוסר אישור מס הכנסה לעניין תרומות לפי סעיף 46 לפקודת מס הכנסה';
            
        string payment_type;
        if (opp.Field3__c == 'שיק') payment_type = '1';
           else if (opp.Field3__c == 'מזומן') payment_type = '2';
           else if (opp.Field3__c == 'Paypal') payment_type = '10';
           else if (opp.Field3__c == 'העברה בנקאית') payment_type = '9';
           else if (opp.Field3__c == 'כרטיס אשראי') payment_type = '6'; 
           else payment_type='11';
             
        //map<paramName,{paramValue, paramType}>> or map<paramArrayName,{paramName, paramValue, paramType}>> 
		Map<string,list<string>> receiptParm = new Map<string,list<string>> 
		{'last_name' => new list<string> {con.lastName,'string'},
		 'first_name' => new list<string> {con.firstName,'string'},
		 'address' => new list<string> {con.MailingStreet,'string'},
		 'city' => new list<string> {con.MailingCity,'string'},
		 'zipcode' => new list<string> {con.MailingPostalCode,'integer'},    
		 'email_to' => new list<string> {con.email,'string'},
		 'api_token' => new list<string> {api_token,'string'},
		 'document_type' => new list<string> {'2','integer'},
		 'price_include_vat' => new list<string> {'true','boolean'},
		 'customer_id' => new list<string> {'0', 'integer'},
		 'comments' => new list<string> {comments,'string'},
		 'items' => new list<string> {'description', opp.name, 'string',
									  'price_nis',string.valueof(opp.Amount),'decimal',
									  'quantity', '1', 'integer'}, 
		 'payments' => new list<string> {'payment_type', payment_type, 'integer',
										 'amount_nis',string.valueof(opp.amount),'decimal'}
		};
            
        string receiptJson = prepareReceipt(receiptParm);
        
        if (!Test.isRunningTest()) {
            ReceiptMakerCls.callRivhit(receiptJson, opp.id, opp.Id);
        }
    }     
    
    public string prepareReceipt(Map<string,list<string>> receiptParm) {
        
        JSONGenerator gen = JSON.createGenerator(true); 
        gen.writeStartObject();

        for(string elem: receiptParm.keySet()) {
            //system.debug('key=' + elem + ' value=' + receiptParm.get(elem)[0] );
            if (receiptParm.get(elem).size() == 2 && !string.isEmpty(receiptParm.get(elem)[0])) {
                //simple element
                if (receiptParm.get(elem)[1] == 'string')
                    gen.writeStringField(elem,receiptParm.get(elem)[0]);
                if (receiptParm.get(elem)[1] == 'integer' )
                    gen.writeNumberField(elem,integer.valueof(receiptParm.get(elem)[0]));
                if (receiptParm.get(elem)[1] == 'decimal')
                    gen.writeNumberField(elem,decimal.valueof(receiptParm.get(elem)[0]));
                if (receiptParm.get(elem)[1] == 'boolean')
                    gen.writeBooleanField(elem,boolean.valueOf(receiptParm.get(elem)[0]));
            }
            if (receiptParm.get(elem).size() > 2) {
                //array element
                system.debug('elem:' + elem);
                gen.writeFieldName(elem);
                gen.writeStartArray();
                gen.writeStartObject();
                for(integer i=0 ; receiptParm.get(elem).size() > i;i=i+3) {
                    system.debug(receiptParm.get(elem) + ' i=' + i + ' fname=' + receiptParm.get(elem)[i] + ' fval=' + receiptParm.get(elem)[i+1] + ' ftype=' + receiptParm.get(elem)[i+2]);      
                    if (!string.isempty(receiptParm.get(elem)[i+1])) {
                        if (receiptParm.get(elem)[i+2] == 'string')
                           gen.writeStringField(receiptParm.get(elem)[i],receiptParm.get(elem)[i+1]);
                        else if (receiptParm.get(elem)[i+2] == 'integer')
                            gen.writeNumberField(receiptParm.get(elem)[i],integer.valueof(receiptParm.get(elem)[i+1]));
                        else if (receiptParm.get(elem)[i+2] == 'decimal')
                            gen.writeNumberField(receiptParm.get(elem)[i],decimal.valueof(receiptParm.get(elem)[i+1]));
                        else if (receiptParm.get(elem)[i+2] == 'boolean')
                           gen.writeBooleanField(receiptParm.get(elem)[i],boolean.valueOf(receiptParm.get(elem)[i+1]));
                    }
                 }
                 gen.writeEndObject();
                 gen.writeEndArray();
            }         
         }
             
        return gen.getAsString();
    }

    @future(callout=true)
    public static void callRivhit(string request, id opp, string opportunityId) {
        system.debug('!!! roi Calling rivhit:');
        system.debug('!!! roi json:' + request);
        Http http = new Http();
        HttpRequest req = new HttpRequest();
            
        req.setEndpoint('callout:rivhit_online/Document.New');
        req.setHeader('Content-Type','application/json');
        req.setMethod('POST');
        req.setBody(request);
        req.setTimeout(120000);
            
        HttpResponse res = new HttpResponse() ;
        
        receipthistory__c rh = new receipthistory__c();
        
        try { 
                res = http.send(req);
            
                System.debug(res.getBody());
                system.debug('!!! roi status string:' + res.toString());
                system.debug('!!! roi status:' + res.getStatus());
                system.debug('!!! roi status code:' + res.getStatusCode());
                system.debug('!!! roi body:' + res.getBody());
                
                Dom.Document doc = new Dom.Document();
                doc.load(res.getBody());
                Dom.XMLNode respDtl= doc.getRootElement();
                string nms = respDtl.getNamespace();
                        
                if (res.getStatusCode() == 200) {
                    rh.urlrivihit__c = respDtl.getChildElement('data', nms).getChildElement('document_link',nms).gettext();
                    rh.urlrivihit_status__c = 'Success';
                    
                } else if (res.getStatusCode() == 500) {
                    rh.urlrivihit_status__c = respDtl.getChildElement('client_message', nms).gettext();
                } else {
                    rh.urlrivihit_status__c = res.toString();
                }
            
        } catch (System.CalloutException e) {
                system.debug('!!!!roi Callout error: '+ e);
                rh.urlrivihit_status__c = e.getMessage();
                string errorMsg = 'Failed to send receipt for opportunity: ' + opportunityId + '. Call revhit failed';
                ReceiptMakerCls.sendFailureEmail(errorMsg, e.getMessage());
        }          
        
        rh.opportunity__c =  opp;
        insert rh; 
        
        Opportunity p = new Opportunity();
        p.id = opp;
        p.Field1__c = true;
        update p;
    }

    private static void sendFailureEmail(string issue, string cause) {
        Settings__c settings = SettingsProvider.getSettings();
        OrgWideEmailAddress orgAddress = [select id, Address, DisplayName from OrgWideEmailAddress limit 1];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new list<string> { settings.EmailInCaseOfRefusalMflakard__c });
        mail.setSubject('אתר תשלומים');
        mail.setOrgWideEmailAddressId(orgAddress.Id);
        string body = issue + ':<br/>סיבה: ' + cause + '<br/>';

        mail.setHtmlBody(body);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}