public class YearlyMembershipRenewalProcessor implements Queueable, Database.AllowsCallouts, Database.Batchable<PelecardToken__c> {	
    
    public void execute(QueueableContext context) {	
        Database.executeBatch(this, 1);
    }
    
    public List<PelecardToken__c> start(Database.BatchableContext context) {
        date today = system.today();	
        List<PelecardToken__c> pelecardTokens = [SELECT TokenEncrypted__c,CVV__c,Amount__c,Contact__c from PelecardToken__c WHERE Contact__r.npo02__MembershipEndDate__c < :today limit 45];

        System.Debug('[YearlyMembershipRenewalProcessor]: Found ' + pelecardTokens.Size() + ' records to process.');
        
        return pelecardTokens;
    }
    	
    public void execute(Database.BatchableContext context, PelecardToken__c[] scope) {
        PelecardToken__c pelToken = scope[0];
        renewSingleMembership(pelToken.Contact__c, pelToken.Amount__c, pelToken.TokenEncrypted__c, pelToken.CVV__c);
    }
    
    public void renewSingleMembership(string contactId, decimal amount, string token, string cvv) {	
        date lastEndDate = system.today();	
        Contact contact = (Contact) CSobjectDb.GetSobjectById(contactId, Contact.SobjectType);
        
        Settings__c settings = SettingsProvider.getSettings();
        string status = settings.RecurringPaymentStatus__c;
        
        System.Debug('[YearlyMembershipRenewalProcessor]: Making payment for contact ' + contact.Name + '. Status is: ' + status);
        if (status == 'Disabled') {
            System.Debug('[YearlyMembershipRenewalProcessor]: RecurringPaymentStatus is disabled. Bailing..');
            return;
        }
        PaymentProcessor processor = new PaymentProcessor();	
        decimal total = amount * 100;	
        PelecardResponse pelecardRes = null;
        if (status == 'ReceiptOnly') {
            System.Debug('[YearlyMembershipRenewalProcessor]: RecurringPaymentStatus is ReceiptOnly. Skipping payment..');
            pelecardRes = new PelecardResponse();
            pelecardRes.StatusCode = '000';
        } else {
            pelecardRes = processor.makeTokenPayment(token, cvv, total, contact);	    
        }
        if (pelecardRes.StatusCode == '000') {
            System.Debug('[YearlyMembershipRenewalProcessor]: Payment complete for customer ' + contact.Name);	
            (new PaymentSuccessHandler()).handle(contactId, amount, 0);	
        }
    }
    
    public void finish(Database.BatchableContext context) {
    }
}