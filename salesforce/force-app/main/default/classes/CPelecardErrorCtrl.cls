public with sharing class CPelecardErrorCtrl
{
	public Settings__c pSettings { get; set; }
	public PelecardSession__c pPelecardSession { get; set; }
	
	public CPelecardErrorCtrl()
	{
		SetSettings();
		SetPelecardSession();
	}
	
	private void SetSettings()
	{
		pSettings = SettingsProvider.getSettings();
	}
	
	private void SetPelecardSession()
	{
		string id = Apexpages.currentPage().getParameters().get('session');
		if (id != null)
			pPelecardSession = (PelecardSession__c)CSobjectDb.GetSobjectByIdWithChildrenRelationshipAndParents(id, PelecardSession__c.SobjectType);
		else
			pPelecardSession = new PelecardSession__c();
	}
	
	public void SendMail()
	{
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		mail.setToAddresses(new list<string> { pSettings.EmailInCaseOfRefusalMflakard__c });
		string err = '';
		try
		{
			err = CObjectsNames.CreditCardErrorsMap.get(Apexpages.currentPage().getParameters().get('result').substring(0, 3));
		}
		catch (Exception e)
		{
			err = CObjectsNames.CreditCardErrorsMap.get(Apexpages.currentPage().getParameters().get('result'));
		}
		mail.setSubject('אתר תשלומים');
		mail.setHtmlBody('תשלום נכשל בפלאקארד:<br/>סיבה: ' + err + '<br/>שם: ' + pPelecardSession.Contact__r.Name + '<br/>מייל:' + pPelecardSession.Contact__r.Email + '<br/>טלפון נייד: ' + pPelecardSession.Contact__r.MobilePhone);
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}
}