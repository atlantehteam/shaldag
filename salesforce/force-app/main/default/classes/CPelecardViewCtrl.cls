public with sharing class CPelecardViewCtrl
{
	public PelecardSession__c pPelecardSession { get; set; }
	public Settings__c pSettings { get; set; }
	public string pError { get; set; } // only error view
	
	public CPelecardViewCtrl()
	{
		SetPelecardSession();
		SetSettings();
		SetError();
	}
	
	private void SetPelecardSession()
	{
		string id = Apexpages.currentPage().getParameters().get('session');
		if (id != null)
			pPelecardSession = (PelecardSession__c)CSobjectDb.GetSobjectById(id, PelecardSession__c.SobjectType);
		else
			pPelecardSession = new PelecardSession__c();
	}
	
	private void SetSettings()
	{
		pSettings = SettingsProvider.getSettings();
	}
	
	private void SetError()
	{
		string result = Apexpages.currentPage().getParameters().get('result');
		try
		{
			pError = CObjectsNames.CreditCardErrorsMap.get(result.substring(0, 3));
		}
		catch (Exception e)
		{
			pError = CObjectsNames.CreditCardErrorsMap.get(result);
		}
	}
}