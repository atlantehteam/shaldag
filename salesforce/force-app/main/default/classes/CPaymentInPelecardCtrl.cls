public with sharing class CPaymentInPelecardCtrl
{
	public PelecardSession__c pPelecardSession { get; set; }
	public Settings__c pSettings { get; set; }
	public string pHtml { get; set; }
	
	public CPaymentInPelecardCtrl()
	{
		SetPelecardSession();
		SetSettings();
		SetHtml();
	}
	
	private void SetPelecardSession()
	{
		string id = Apexpages.currentPage().getParameters().get('session');
		if (id != null)
			pPelecardSession = (PelecardSession__c)CSobjectDb.GetSobjectById(Apexpages.currentPage().getParameters().get('session'), PelecardSession__c.SobjectType);
		else
			pPelecardSession = new PelecardSession__c();
	}
	
	private void SetSettings()
	{
        pSettings = SettingsProvider.getSettings();
	}
	
	private void SetHtml()
	{
		string userName = '';
		if (pSettings.UsernameInPelecard__c != null)
			userName = pSettings.UsernameInPelecard__c;
		string password = '';
		if (pSettings.PasswordInPelecard__c != null)
		{
			password = pSettings.PasswordInPelecard__c;
			password = password.Replace('+', '`9`');
			password = password.Replace('&', '`8`');
			password = password.Replace('%', '`7`'); 
		}
		string termNo = '';
		if (pSettings.TerminalNumberInPelecard__c != null)
			termNo = pSettings.TerminalNumberInPelecard__c;
		string maxPayments = string.valueOf(pSettings.MaxPaymentInPelecard__c);
		string minPaymentsNo = '1';
		string creditTypeFrom = '6';
		string goodUrl = CObjectsNames.getBaseUrl() + page.PelecardGoodUrlView.getUrl() + '?session=' + pPelecardSession.Id;
		string errorUrl = CObjectsNames.getBaseUrl() + page.PelecardErrorUrlView.getUrl() + '?session=' + pPelecardSession.Id;
		string ValidateLink = CObjectsNames.getBaseUrl() + page.PelecardGoodUrl.getUrl() + '?session=' + pPelecardSession.Id;
		string ErrorLink = CObjectsNames.getBaseUrl() + page.PelecardErrorUrl.getUrl() + '?session=' + pPelecardSession.Id;
		string total = string.valueOf(integer.valueOf(pPelecardSession.Amount__c * 100));
		string rCurrency = '1';
		string Logo = '';
		string smallLogo = '';
		if (pSettings.Logo__c != null)
		{
			Logo = pSettings.Logo__c;
			smallLogo = pSettings.Logo__c;
		}
		string hidePciDssLogo = 'True';
		string hidePelecardLogo = 'True';
		string theme = '2';
		string background = '0';
		string backgroundImage = '';
		string topMargin = '50px';
		string supportedCardTypes = 'True,True,True,True,True';
		string Parmx = '';
		string hideParmx = 'False';
		string Text1 = '';
		string Text2 = '';
		string Text3 = '';
		string Text4 = '';
		string Text5 = '';
		string cancelUrl = '';
		string MustConfirm = 'False';
		string ConfirmationText = '';
		string SupportPhone = '';
		string errorText = '';
		string id = (pSettings.IsShowIdInPelecard__c ? '' : 'Hide');
		string cvv2 = (pSettings.IsShowCvv2InPelecard__c ? '' : 'Hide');
		string authNum = '';
		string shopNo = '001';
		string frmAction = '';
		string TokenForTerminal = '';
		string J5 = 'False';
		string keepSSL = 'True';
		string sessionIdNumber = '';
		string showTotalOnRight = '';
		if (!test.isRunningTest())
		{
			string postData = 'userName=' + EncodingUtil.urlEncode(userName, 'UTF-8') + '&password=' + EncodingUtil.urlEncode(password, 'UTF-8') + 
					'&termNo=' + EncodingUtil.urlEncode(termNo, 'UTF-8') + '&pageName=ajaxPage' +
					'&goodUrl=' + EncodingUtil.urlEncode(goodUrl, 'UTF-8') + '&errorUrl=' + EncodingUtil.urlEncode(errorUrl, 'UTF-8') + '&ValidateLink=' + 
					 EncodingUtil.urlEncode(ValidateLink, 'UTF-8') + '&ErrorLink=' + EncodingUtil.urlEncode(ErrorLink, 'UTF-8') +
					'&total=' + EncodingUtil.urlEncode(total, 'UTF-8') + '&currency=' + EncodingUtil.urlEncode(rCurrency, 'UTF-8') +
					'&maxPayments=' + EncodingUtil.urlEncode(maxPayments, 'UTF-8') + '&minPaymentsNo=' + EncodingUtil.urlEncode(minPaymentsNo, 'UTF-8') + '&creditTypeFrom=' + 
					 EncodingUtil.urlEncode(creditTypeFrom, 'UTF-8') +
					'&Logo=' + EncodingUtil.urlEncode(Logo, 'UTF-8') + '&smallLogo=' + EncodingUtil.urlEncode(smallLogo, 'UTF-8') + '&hidePciDssLogo=' + 
					 EncodingUtil.urlEncode(hidePciDssLogo, 'UTF-8') + '&hidePelecardLogo=' + EncodingUtil.urlEncode(hidePelecardLogo, 'UTF-8') +
					'&theme=' + EncodingUtil.urlEncode(theme, 'UTF-8') + '&background=' + EncodingUtil.urlEncode(background, 'UTF-8') + '&backgroundImage=' + 
					 EncodingUtil.urlEncode(backgroundImage, 'UTF-8') + '&topMargin=' + EncodingUtil.urlEncode(topMargin, 'UTF-8') +
					'&supportedCardTypes=' + EncodingUtil.urlEncode(supportedCardTypes, 'UTF-8') + '&Parmx=' + EncodingUtil.urlEncode(Parmx, 'UTF-8') + '&hideParmx=' + 
					 EncodingUtil.urlEncode(hideParmx, 'UTF-8') +
					'&Text1=' + EncodingUtil.urlEncode(Text1, 'UTF-8') + '&Text2=' + EncodingUtil.urlEncode(Text2, 'UTF-8') + '&Text3=' + 
					 EncodingUtil.urlEncode(Text3, 'UTF-8') + '&Text4=' + 
					 EncodingUtil.urlEncode(Text4, 'UTF-8') + '&Text5=' + EncodingUtil.urlEncode(Text5, 'UTF-8') +
					'&cancelUrl=' + EncodingUtil.urlEncode(cancelUrl, 'UTF-8') + '&MustConfirm=' + EncodingUtil.urlEncode(MustConfirm, 'UTF-8') + '&ConfirmationText=' + 
					 EncodingUtil.urlEncode(ConfirmationText, 'UTF-8') + '&SupportPhone=' + EncodingUtil.urlEncode(SupportPhone, 'UTF-8') + '&errorText=' + 
					 EncodingUtil.urlEncode(errorText, 'UTF-8') +
					'&id=' + EncodingUtil.urlEncode(id, 'UTF-8') + '&cvv2=' + EncodingUtil.urlEncode(cvv2, 'UTF-8') + '&authNum=' + EncodingUtil.urlEncode(authNum, 'UTF-8') + 
					'&shopNo=' + EncodingUtil.urlEncode(shopNo, 'UTF-8') +
					'&frmAction=' + EncodingUtil.urlEncode(frmAction, 'UTF-8') + '&TokenForTerminal=' + EncodingUtil.urlEncode(TokenForTerminal, 'UTF-8') + '&J5=' + 
					 EncodingUtil.urlEncode(J5, 'UTF-8') + '&keepSSL=' + EncodingUtil.urlEncode(keepSSL, 'UTF-8') +
					'&sessionIdNumber=' + EncodingUtil.urlEncode(sessionIdNumber, 'UTF-8') + '&showTotalOnRight=' + EncodingUtil.urlEncode(showTotalOnRight, 'UTF-8');
			Http http = new Http();
			HttpRequest req = new HttpRequest();
			req.setEndpoint('https://gateway.pelecard.biz/PayProcess');
			req.setMethod('POST');
			req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
			req.setBody(postData);
			HttpResponse res = http.send(req);
			pHtml = res.getBody().replace('<!DOCTYPE html>', '');
		}
		else
		{
			pHtml = '';
		}
	}
}