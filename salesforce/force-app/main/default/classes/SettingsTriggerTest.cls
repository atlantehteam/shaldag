@isTest(SeeAllData=true)
public class SettingsTriggerTest {
	static testMethod void validate_SettingsTrigger_clears_active_Test() {
        Test.startTest();
		Settings__c item1 = generateSettings('My item1');
        item1.Active__c = true;
        insert item1;
        
        Settings__c item2 = generateSettings('My item2');
        item2.Active__c = true;
        insert item2;
        
        Settings__c item1FromDB = [SELECT Active__c from Settings__c where Id = :item1.Id];
       
        System.assertEquals(false, item1FromDB.Active__c);
        
        Test.stopTest();
    }
    
    private static Settings__c generateSettings(String name) {
        Settings__c developerSettings = new Settings__c();
        developerSettings.Name = name;
        developerSettings.EmailInCaseOfRefusalMflakard__c = 'a@a.com';
        developerSettings.PaymentOptions__c = '120_once | 120 - תשלום חד פעמי\n120_yearly | 120 - תשלום שנתי מתחדש\n60_once | 60 - תשלום חד פעמי - סטודנטים';
        developerSettings.API_TOKEN__c = '1234';
        developerSettings.DonationPaymentOptions__c = '';
        developerSettings.MaxPaymentInPelecard__c = 1;
        developerSettings.PasswordInPelecard__c = '1111';
        developerSettings.UsernameInPelecard__c = 'user';
        developerSettings.TerminalNumberInPelecard__c = '8888';
        developerSettings.ReceiptPaymentTypes__c = 'MajorGift';
        developerSettings.SplitPayment__c = 120;
        developerSettings.RecurringPaymentStatus__c = 'Active';
        
        return developerSettings;
    }
}