@isTest(SeeAllData=true)
public class RecruitCycleTriggerTest {
	static testMethod void validate_RecruitCycle_clears_active_Test() {
        Test.startTest();
		RecruitCycle__c rc1 = new RecruitCycle__c();
        rc1.Name = 'My rc1';
        rc1.Active__c = true;
        insert rc1;
        
        RecruitCycle__c rc2 = new RecruitCycle__c();
        rc2.Name = 'My rc2';
        rc2.Active__c = true;
        insert rc2;
        
        RecruitCycle__c rc1FromDB = [SELECT Active__c from RecruitCycle__c where Id = :rc1.Id];
       
        System.assertEquals(false, rc1FromDB.Active__c);
        
        Test.stopTest();
    }
}