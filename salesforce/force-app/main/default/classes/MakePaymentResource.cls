@RestResource(urlMapping='/makepayment/*')
global with sharing class MakePaymentResource {
    @HttpPost
	global static void doPost(MakePaymentResourcePayload payment) {
        string type = RestContext.request.params.get('type');
        Settings__c settings = SettingsProvider.getSettings();
        if (!validatePayload(payment, type, settings)) {
            return;
        }

        boolean isYearly = (payment.amount != null) ? payment.amount.endsWith('yearly') : false;
        payment.amountReal = calculateAmount(payment);
        decimal total = calculateTotal(payment);
        Contact contact = (Contact) CSobjectDb.GetSobjectById(payment.contactId, Contact.SobjectType);

        if (isYearly) {
            MakeYearlyPayment(payment, total, contact);
        } else {
            MakeOneTimePayment(payment, total, contact);
        }
    }
    
    private static void MakeYearlyPayment(MakePaymentResourcePayload payment, decimal total, Contact contact) {
        PaymentProcessor processor = new PaymentProcessor();
        PelecardResponse tokenRes = processor.convertToToken(payment.creditCard, payment.creditCardDateMmYy, contact);
        if (tokenRes.StatusCode != '000') {
            handlePaymentFailure(tokenRes);
            return;
        }
        string token = tokenRes.ResultData.get('Token');
        PelecardResponse pelecardRes = processor.makeTokenPayment(token, payment.cvv, total, contact);
        if (pelecardRes.StatusCode != '000') {
            handlePaymentFailure(pelecardRes);
        } else {
            HandleYearlyPaymentSuccess(token, payment.cvv, contact, payment);
            handlePaymentSuccess(pelecardRes, payment);
        }
    }
    
    private static void HandleYearlyPaymentSuccess(string token, string cvv, Contact contact, MakePaymentResourcePayload payment) {
        List<PelecardToken__c> currentTokens = [SELECT Id from PelecardToken__c WHERE Contact__c = :contact.id];
        PelecardToken__c pelecardToken = new PelecardToken__c();
        pelecardToken.TokenEncrypted__c = token;
        pelecardToken.CVV__c = cvv;
        pelecardToken.Amount__c = payment.amountReal;
        pelecardToken.Contact__c = contact.Id;
        insert pelecardToken;
        
        contact.IsYearlyMembership__c = true;
        update contact;
    }

    private static void MakeOneTimePayment(MakePaymentResourcePayload payment, decimal total, Contact contact) {
        PaymentProcessor processor = new PaymentProcessor();
        PelecardResponse pelecardRes = processor.makePayment(payment.creditCard, payment.creditCardDateMmYy, payment.cvv, total, contact);
        if (pelecardRes.StatusCode != '000') {
            handlePaymentFailure(pelecardRes);
        } else {
            handlePaymentSuccess(pelecardRes, payment);
        }
    }
    private static decimal calculateAmount(MakePaymentResourcePayload payment) {
        if (payment.amount == null) {
            return 0;
        }
        return Decimal.valueOf(payment.amount.split('_')[0]);
    }
    private static decimal calculateTotal(MakePaymentResourcePayload payment) {
        decimal total = 0;
        total += payment.amountReal;
        total += payment.donationAmount == null ? 0 : payment.donationAmount;
        total *= 100;
        return total;
    }
    private static void handlePaymentSuccess(PelecardResponse pelecardRes, MakePaymentResourcePayload payment) {
        (new PaymentSuccessHandler()).handle(payment.contactId, payment.amountReal, payment.donationAmount);
        sendResponse(new ApiResponse());
    }
    private static void handlePaymentFailure(PelecardResponse pelecardRes) {
        Map<String, String> errors = new Map<String, String>();
        errors.put('paymentFailure', pelecardRes.FormattedMessage);
        handleErrors(errors);
    }
   
    private static boolean validatePayload(MakePaymentResourcePayload payment, string type, Settings__c settings) {
        Map<String, String> errors = new Map<String, String>();
        if (payment.donationAmount != null && payment.donationAmount < 0) {
            errors.put('donationAmount', 'סכום תרומה לא חוקי');
        } 

        if (type == 'membership') {
            boolean amountValid = false;
            string[] options = settings.PaymentOptions__c.split('\n');
            for (string opt : options) {
                string validAmount = opt.split(' ')[0];
                if (payment.amount == validAmount) {
                    amountValid = true;
                    break;
                }
            }
            if (!amountValid) {
                errors.put('amount', 'סכום תשלום לא חוקי');
            }
        } else if (type == 'donation') {
            if (payment.donationAmount <= 0) {
                errors.put('donationAmount', 'סכום תרומה לא חוקי');
            }
            if (payment.amount != null) {
                errors.put('amount', 'סכום תשלום לא חוקי');
            }
        }
        
        list<Contact> matchingContacts = [Select Id from Contact where Id = :payment.contactId limit 1];
        if (matchingContacts.IsEmpty()) {
            errors.put('contactId', 'invalidContact');
        }
        return handleErrors(errors);
    }
	
    private static void sendResponse(ApiResponse resp) {
        new ApiResponder().sendResponse(resp);
    }
   
    private static boolean handleErrors(Map<String, String> errors) {
        if (errors.isEmpty()) {return true;}
        ApiResponse resp = new ApiResponse(errors);
        sendResponse(resp);
        return false;
    }
    
    global class MakePaymentResourcePayload {
        public string creditCard {get; set;}
        public string creditCardDateMmYy {get; set;}
        public string cvv {get; set;}
        public string contactId {get; set;}
        public string amount {get; set;}
        public decimal amountReal {get; set;}
        public decimal donationAmount {get; set;}
    }

}