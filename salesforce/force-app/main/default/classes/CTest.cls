@isTest(SeeAllData=true)
public class CTest
{
	static testMethod void CJoinAndRenovationOfMemberCtrlTest()
	{
		CreateSettings();
		CJoinAndRenovationOfMemberCtrl joinAndRenovationOfMemberCtrl = new CJoinAndRenovationOfMemberCtrl();
		joinAndRenovationOfMemberCtrl.pContact.FirstName = '123';
		joinAndRenovationOfMemberCtrl.pContact.LastName = '123';
		joinAndRenovationOfMemberCtrl.pContact.Field15__c = '1234';
		joinAndRenovationOfMemberCtrl.pPaymentType = 'Donation';
		joinAndRenovationOfMemberCtrl.pAmount = 20;
		joinAndRenovationOfMemberCtrl.GoToPaymentInPelecard();
	}
	
	static testMethod void CPaymentInPelecardCtrlTest()
	{
		CreateSettings();
		PelecardSession__c newPelecardSession = new PelecardSession__c();
		newPelecardSession.Amount__c = 10;
		insert newPelecardSession;
		Apexpages.currentPage().getParameters().put('session', newPelecardSession.Id);
		CPaymentInPelecardCtrl paymentInPelecardCtrl = new CPaymentInPelecardCtrl();
	}
	
	static testMethod void CPelecardErrorCtrlTest()
	{
		CreateSettings();
		CPelecardErrorCtrl pelecardErrorCtrl = new CPelecardErrorCtrl();
	}
	
	static testMethod void CPelecardGoodCtrlTest()
	{
		CreateSettings();
		CPelecardGoodCtrl pelecardGoodCtrl = new CPelecardGoodCtrl();
	}
	
	static testMethod void CPelecardViewCtrlTest()
	{
		CreateSettings();
		CPelecardViewCtrl pelecardViewCtrl = new CPelecardViewCtrl();
	}
	
	static testMethod void CObjectsNamesTest()
	{
		CreateSettings();
		CObjectsNames.ForTest();
	}
	
	static void CreateSettings()
	{
		Settings__c settings = new Settings__c();
		settings.EmailInCaseOfRefusalMflakard__c = 'test@test.test';
		settings.PaymentOptions__c = '10\n20';
		settings.IsShowCvv2InPelecard__c = true;
		settings.IsShowIdInPelecard__c = true;
		settings.Logo__c = 'http://www.logo.com';
		settings.SplitPayment__c = 10;
		settings.TerminalNumberInPelecard__c = 'test';
		settings.MaxPaymentInPelecard__c = 10;
		settings.PasswordInPelecard__c = 'test';
		settings.UsernameInPelecard__c = 'test';
		settings.RecurringPaymentStatus__c = 'Active';
	}
}