public with sharing class CPelecardGoodCtrl
{
	public PelecardSession__c pPelecardSession { get; set; }
	public Settings__c pSettings { get; set; }
	
	public CPelecardGoodCtrl()
	{
		SetPelecardSession();
		SetSettings();
	}
	
	private void SetPelecardSession()
	{
		string id = Apexpages.currentPage().getParameters().get('session');
		if (id != null)
			pPelecardSession = (PelecardSession__c)CSobjectDb.GetSobjectByIdWithChildrenRelationshipAndParents(Apexpages.currentPage().getParameters().get('session'), PelecardSession__c.SobjectType);
		else
			pPelecardSession = new PelecardSession__c();
	}
	
	private void SetSettings()
	{
		pSettings = SettingsProvider.getSettings();
	}
	
	public void InsertOpportunity()
	{
		if (!pPelecardSession.JoinToShaldag__c || pPelecardSession.Amount__c - pSettings.SplitPayment__c > 0)
		{
			if (pPelecardSession.Campaign__c != null)
			{
				InsertOppCamping();
			}
			else
			{
				InsertOppDonation();
			}
		}
		if (pPelecardSession.JoinToShaldag__c)
		{
			InsertOppJoinToShaldag();
		}
	}
	
	private void InsertOppCamping()
	{
		Opportunity newOpportunity = new Opportunity();
		newOpportunity.AccountId = pPelecardSession.Contact__r.AccountId;
		newOpportunity.email__c = pPelecardSession.Contact__r.Email;
		newOpportunity.StageName = 'Posted';
		newOpportunity.CloseDate = System.today();
		newOpportunity.Amount = (pPelecardSession.JoinToShaldag__c ? pPelecardSession.Amount__c - pSettings.SplitPayment__c : pPelecardSession.Amount__c);
		newOpportunity.Name = 'תשלום עבור ' + pPelecardSession.Campaign__r.Name + '- ' + pPelecardSession.Contact__r.FirstName + ' ' + pPelecardSession.Contact__r.LastName;
		newOpportunity.RecordTypeId = [select Id from RecordType where SobjectType = 'Opportunity' and DeveloperName = 'general' limit 1].Id;
		newOpportunity.CampaignId = pPelecardSession.Campaign__c;
		upsert newOpportunity;
		pPelecardSession.opportunity__c = newOpportunity.Id;
		InsertPaymentsByOppId(newOpportunity.Id);
		CampaignMember newCampaignMember = new CampaignMember();
		newCampaignMember.CampaignId = pPelecardSession.Campaign__c;
		newCampaignMember.ContactId = pPelecardSession.Contact__r.Id;
		upsert newCampaignMember;
		update pPelecardSession;
	}
	
	private void InsertOppDonation()
	{
		Opportunity newOpportunity = new Opportunity();
		newOpportunity.AccountId = pPelecardSession.Contact__r.AccountId;
		newOpportunity.email__c = pPelecardSession.Contact__r.Email;
		newOpportunity.StageName = 'Posted';
		newOpportunity.CloseDate = System.today();
		newOpportunity.Amount = (pPelecardSession.JoinToShaldag__c ? pPelecardSession.Amount__c - pSettings.SplitPayment__c : pPelecardSession.Amount__c);
		if (pPelecardSession.Contact__r.Field12__c == null)
		{
			pPelecardSession.Contact__r.Field12__c = 'תורם;';
			upsert pPelecardSession.Contact__r;
		}
		else if (!pPelecardSession.Contact__r.Field12__c.Contains('תורם'))
		{
			pPelecardSession.Contact__r.Field12__c += ';תורם;';
			upsert pPelecardSession.Contact__r;
		}
		newOpportunity.Name = 'תרומה- ' + pPelecardSession.Contact__r.FirstName + ' ' + pPelecardSession.Contact__r.LastName;
		newOpportunity.RecordTypeId = [select Id from RecordType where SobjectType = 'Opportunity' and DeveloperName = 'Grant' limit 1].Id;
		insert newOpportunity;
		InsertPaymentsByOppId(newOpportunity.Id);
	}
	
	private void InsertOppJoinToShaldag()
	{
		if (pPelecardSession.Contact__r.Field12__c == null)
		{
			pPelecardSession.Contact__r.Field12__c = 'חבר עמותה;';
			upsert pPelecardSession.Contact__r;
		}
		else if (!pPelecardSession.Contact__r.Field12__c.Contains('חבר עמותה'))
		{
			pPelecardSession.Contact__r.Field12__c += ';חבר עמותה;';
			upsert pPelecardSession.Contact__r;
		}
		Opportunity newOpportunity = new Opportunity();
		newOpportunity.AccountId = pPelecardSession.Contact__r.AccountId;
		newOpportunity.email__c = pPelecardSession.Contact__r.Email;
		newOpportunity.StageName = 'Posted';
		newOpportunity.CloseDate = System.today();
		newOpportunity.Amount = (pPelecardSession.Amount__c > pSettings.SplitPayment__c ? pSettings.SplitPayment__c : pPelecardSession.Amount__c);
		newOpportunity.Name = 'תשלום דמי חבר עמותה- ' + pPelecardSession.Contact__r.FirstName + ' ' + pPelecardSession.Contact__r.LastName;
		newOpportunity.RecordTypeId = [select Id from RecordType where SobjectType = 'Opportunity' and DeveloperName = 'Membership' limit 1].Id;
		date startDate = GetLastEndDate();
		newOpportunity.npe01__Membership_Start_Date__c = startDate;
		newOpportunity.npe01__Membership_End_Date__c = startDate.addYears(1);
		upsert newOpportunity;
		InsertPaymentsByOppId(newOpportunity.Id);
	}
	
	private date GetLastEndDate()
	{
		date lastEndDate = system.today();
		Opportunity[] oldOpportunities = [select npe01__Membership_End_Date__c from Opportunity where npe01__Membership_End_Date__c > :lastEndDate and AccountId = :pPelecardSession.Contact__r.AccountId];
		for (Opportunity opp : oldOpportunities)
		{
			if (opp.npe01__Membership_End_Date__c  > lastEndDate)
				lastEndDate = opp.npe01__Membership_End_Date__c;
		}
		return lastEndDate.addDays(1);
	}
	
	private void InsertPaymentsByOppId(id oppId)
	{
		list<npe01__OppPayment__c> payments = new list<npe01__OppPayment__c>();
		integer numPayment = integer.valueOf(Apexpages.currentPage().getParameters().get('result').substring(93, 95)) + 1;
		for (integer i = 0; i < numPayment; i++)
		{
			npe01__OppPayment__c newPayment = new npe01__OppPayment__c();
			newPayment.npe01__Opportunity__c = oppId;
			newPayment.npe01__Payment_Date__c = system.today().addMonths(i);
			newPayment.npe01__Payment_Amount__c = pPelecardSession.Amount__c / numPayment;
			newPayment.npe01__Paid__c = true;
			payments.add(newPayment);
		}
		insert payments;
	}
}