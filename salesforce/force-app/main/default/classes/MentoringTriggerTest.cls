@isTest(SeeAllData=true)
public class MentoringTriggerTest {
	static testMethod void validate_ScholarTrigger_clears_active_Test() {
        Test.startTest();
		CustomObject2__c item1 = new CustomObject2__c();
        item1.Name = 'My item1';
        item1.Active__c = true;
        insert item1;
        
        CustomObject2__c item2 = new CustomObject2__c();
        item2.Name = 'My item2';
        item2.Active__c = true;
        insert item2;
        
        CustomObject2__c item1FromDB = [SELECT Active__c from CustomObject2__c where Id = :item1.Id];
       
        System.assertEquals(false, item1FromDB.Active__c);
        
        Test.stopTest();
    }
}