public with sharing class CContactCtrlExtention {

    private Contact con;
    
    public void UpdateCon() {
        List<Contact> conTemp = [select id from Contact where Field15__c = :Con.Field15__c];
        if ( ! conTemp.isEmpty() ) {
            con.id = conTemp[0].id;
            update con;
        } else {
            con.addError('חבר עמותה לא קיים');
        }
    } 
    
    public CContactCtrlExtention (ApexPages.StandardController controller) {
        con = (contact)controller.getRecord();
    }    
}