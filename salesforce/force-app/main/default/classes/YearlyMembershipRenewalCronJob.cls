global class YearlyMembershipRenewalCronJob implements Schedulable {
    //mandataory function called by the Apex Scheduler
	global void execute(SchedulableContext SC) {
    	System.enqueueJob(new YearlyMembershipRenewalProcessor());
    }
}