public class WordpresssNotifierClass {
	@future(callout=true)
    public static void handleEventChange() {
        HttpRequest request = new HttpRequest();
        // Set the endpoint URL.
        request.setEndPoint('callout:wp_site/wp-admin/admin-ajax.php?action=import_events');
        // Set the HTTP verb to GET.
        request.setMethod('GET');
        // Send the HTTP request and get the response.
        HttpResponse response = new HTTP().send(request);
    }
}