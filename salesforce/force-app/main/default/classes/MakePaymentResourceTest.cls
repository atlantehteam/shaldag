@isTest(SeeAllData=true)
public class MakePaymentResourceTest {
    static void prepareContext(string reqType) {
        RestRequest req = new RestRequest();
        req.addParameter('type', reqType);
        
        RestResponse res = new RestResponse();
        req.addHeader('httpMethod', 'POST');       
        String postData = '{}';
        String JsonMsg=JSON.serialize(postData);
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req; 
        RestContext.response = res;
    }
    
    static ApiResponse getResponse() {
        String resString = RestContext.response.responseBody.toString();
        ApiResponse apiResponse = (ApiResponse) JSON.deserialize(resString, ApiResponse.class);
        return apiResponse;
    }

    static Settings__c prepareSettings() {
        Settings__c developerSettings = new Settings__c();
        developerSettings.Name = 'Developer';
        developerSettings.EmailInCaseOfRefusalMflakard__c = 'a@a.com';
        developerSettings.PaymentOptions__c = '120_once | 120 - תשלום חד פעמי\n120_yearly | 120 - תשלום שנתי מתחדש\n60_once | 60 - תשלום חד פעמי - סטודנטים';
        developerSettings.API_TOKEN__c = '1234';
        developerSettings.DonationPaymentOptions__c = '';
        developerSettings.MaxPaymentInPelecard__c = 1;
        developerSettings.PasswordInPelecard__c = '1111';
        developerSettings.UsernameInPelecard__c = 'user';
        developerSettings.TerminalNumberInPelecard__c = '8888';
        developerSettings.ReceiptPaymentTypes__c = 'MajorGift';
        developerSettings.SplitPayment__c = 120;
        developerSettings.Active__c = True;
        developerSettings.RecurringPaymentStatus__c = 'Active';
        
        insert developerSettings;
        
        return developerSettings;
    }
   
    static MakePaymentResource.MakePaymentResourcePayload getValidPayload() {
        Contact c = new Contact();
        c.FirstName = 'cFirst';
        c.LastName = 'cLast';
        insert c;
        
        MakePaymentResource.MakePaymentResourcePayload payload = new MakePaymentResource.MakePaymentResourcePayload();
        payload.amount = '120_once';
        payload.creditCard = '12345678';
        payload.creditCardDateMmYy = '0120';
        payload.cvv = 'cvv';
        payload.donationAmount = 100;
        payload.contactId = c.Id;
        return payload;
    }
	static testMethod void validateInvalidDonationAmount() {
		
        prepareContext('membership');
        Test.startTest();
		
        Settings__c settings = prepareSettings();
        MakePaymentResource.MakePaymentResourcePayload payload = getValidPayload();
        payload.donationAmount = -10;
        
        MakePaymentResource.doPost(payload);
        
		ApiResponse apiResponse = getResponse();
        System.assertEquals(false, apiResponse.success);
        System.assertEquals('donationAmount', apiResponse.errors.get(0).key);
        
        Test.stopTest();
    }
    
    static testMethod void validateInvalidAmount() {
		
        prepareContext('membership');
        Test.startTest();
		
        Settings__c settings = prepareSettings();
        MakePaymentResource.MakePaymentResourcePayload payload = getValidPayload();
        payload.amount = 'invalid';
        
        MakePaymentResource.doPost(payload);
        
		ApiResponse apiResponse = getResponse();
        System.assertEquals(false, apiResponse.success);
        System.assertEquals('amount', apiResponse.errors.get(0).key);
        
        Test.stopTest();
    }
    
    static testMethod void validateDonationInvalidAmount() {
		
        prepareContext('donation');
        Test.startTest();
		
        Settings__c settings = prepareSettings();
        MakePaymentResource.MakePaymentResourcePayload payload = getValidPayload();
        payload.amount = 'invalid';
        
        MakePaymentResource.doPost(payload);
        
		ApiResponse apiResponse = getResponse();
        System.assertEquals(false, apiResponse.success);
        System.assertEquals('amount', apiResponse.errors.get(0).key);
        
        Test.stopTest();
    }
    
    static testMethod void validateDonationInvalidZeroAmount() {
		
        prepareContext('donation');
        Test.startTest();
		
        Settings__c settings = prepareSettings();
        MakePaymentResource.MakePaymentResourcePayload payload = getValidPayload();
        payload.amount = null;
        payload.donationAmount = 0;
        
        MakePaymentResource.doPost(payload);
        
		ApiResponse apiResponse = getResponse();
        System.assertEquals(false, apiResponse.success);
        System.assertEquals('donationAmount', apiResponse.errors.get(0).key);
        
        Test.stopTest();
    }
    
    /*static testMethod void validateInvalidContact() {
		
        prepareContext('membership');
        Test.startTest();
		
        Settings__c settings = prepareSettings();
        MakePaymentResource.MakePaymentResourcePayload payload = getValidPayload();
        payload.contactId = '111';
        
        MakePaymentResource.doPost(payload);
        
		ApiResponse apiResponse = getResponse();
        System.assertEquals(false, apiResponse.success);
        System.assertEquals('contactId', apiResponse.errors.get(0).key);
        
        Test.stopTest();
    }*/
}