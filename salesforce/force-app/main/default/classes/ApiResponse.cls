global class ApiResponse {
    public boolean success {get {return this.errors == NULL || this.errors.IsEmpty();}}
    public list<ApiError> errors {get; set;}
    
    public ApiResponse() {
        
    }
    public ApiResponse(Map<String, String> errors) {
        this.errors = new list<ApiError>(); 
        for ( string key : errors.keySet() ){
            this.errors.add(new ApiError(key, errors.get(key)));
        }
    }
    
    global class ApiError {
        public string key {get; set;}
        public string value {get; set;}
        public ApiError(string key, string value) {
            this.key = key;
            this.value = value;
        }
    }
}