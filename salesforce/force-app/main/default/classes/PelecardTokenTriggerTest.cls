@isTest(SeeAllData=true)
public class PelecardTokenTriggerTest {
	static testMethod void validate_PelecardTokenTrigger_clears_old_tokens() {
        Test.startTest();
        Contact c1 = new Contact();
        c1.LastName = 'c1Last';
        c1.FirstName = 'c1First';
        Contact c2 = new Contact();
        c2.LastName = 'c2Last';
        c2.FirstName = 'c2First';
        insert c1;
        insert c2;
        
        PelecardToken__c c1Token = new PelecardToken__c();
        c1Token.Contact__c = c1.Id;
        insert c1Token;
        
        PelecardToken__c c2Token = new PelecardToken__c();
        c2Token.Contact__c = c2.Id;
        insert c2Token;
        
        PelecardToken__c c3Token = new PelecardToken__c();
        c3Token.Contact__c = c1.Id;
        insert c3Token;
        
        List<PelecardToken__c> c1TokenList = [SELECT Id from PelecardToken__c where Id = :c1Token.Id];
       	List<PelecardToken__c> c2TokenList = [SELECT Id from PelecardToken__c where Id = :c2Token.Id];
        List<PelecardToken__c> c3TokenList = [SELECT Id from PelecardToken__c where Id = :c3Token.Id];
        
        System.assertEquals(0, c1TokenList.size());
        System.assertEquals(1, c2TokenList.size());
        System.assertEquals(1, c3TokenList.size());
        
        Test.stopTest();
    }
}