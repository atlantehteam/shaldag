public with sharing class CJoinAndRenovationOfMemberCtrl
{
    public Contact pContact { get; set; }
    public Settings__c pSettings { get; set; }
    public list<SelectOption> pPaymentTypeOptions { get; set; }
    public string pPaymentType { get; set; }
    public decimal pMinAmountJoin { get; set; }
    public list<SelectOption> pAmountOptionsDonation { get; set; }
    public decimal pAmount { get; set;} 
	public decimal pAmountTickets { get; set;} 
    public list<SelectOption> pAmountOptionsCampaign { get; set; }
	public string pSpouse { get; set; }
	

	public List<SelectOption> getNumberOfEventTickets() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('50','כרטיס אחד - 50 שקל'));
        options.add(new SelectOption('100','שני כרטיסים - 100 שקל'));
        return options;
    }


    public CJoinAndRenovationOfMemberCtrl()
    {
        pContact = new Contact();
        SetSettings();
        SetPaymentTypeOptions();
        SetMinAmountJoinAndAmountOptionsDonation();
		pSpouse = '--ללא--';
		pAmountTickets = 0;

    }
    
    private void SetMinAmountJoinAndAmountOptionsDonation()
    {
       /*
        pAmountOptionsDonation = new list<SelectOption>();
        decimal min;
        list<string> options = (pSettings.PaymentOptions__c != null ? pSettings.PaymentOptions__c.split('\n') : new list<string>());
        for (string option : options)
        {
            pAmountOptionsDonation.add(new SelectOption(option.split(' ')[0].trim(), option));
            decimal optionVal = decimal.valueOf(option.split(' ')[0].trim());
            if (min == null)
                min = optionVal;
            else if (min > optionVal)
                min = optionVal;
        }
        pAmountOptionsDonation.add(new SelectOption('אחר', 'אחר'));
        if (min == null)
            pMinAmountJoin = 0;
        else
            pMinAmountJoin = min;
		*/
    }
    
    private void SetSettings()
    {
        /*
        list<Settings__c> settings = CSobjectDb.GetSobjectByFilter('Id != null limit 1', Settings__c.SobjectType);
        if (settings.size() > 0)
            pSettings = settings[0];
        else
            pSettings = new Settings__c();
		*/
    }
    
    private void SetPaymentTypeOptions()
    {
        /*
        pPaymentTypeOptions = new list<SelectOption>();
        pPaymentTypeOptions.add(new SelectOption('', '-- לא נבחר --'));
        if (pSettings.IsJoinToShaldagInSite__c)
            pPaymentTypeOptions.add(new SelectOption('JoinToShaldag', 'הצטרפות/חידוש לעמותה - דמי חבר'));
        if (pSettings.IsDonationInSite__c)
            pPaymentTypeOptions.add(new SelectOption('Donation', 'תרומה לעמותה'));
        list<Campaign> campaigns = [select Id, Name from Campaign where Field5__c = true and IsActive = true];
        for (Campaign cam : campaigns)
        {
            pPaymentTypeOptions.add(new SelectOption(cam.id, 'תשלום לאירוע ' + cam.Name));
        }
		*/
    }
    
    public void ChangePaymentType()
    {
        /*
        if (pPaymentType.left(3) == '701' ) //701 = campaign record
        {
            Campaign cam = [select Id, PaymentOptions__c from Campaign where Id = :pPaymentType limit 1];
            pAmountOptionsCampaign = new list<SelectOption>();
            list<string> options = (cam.PaymentOptions__c != null ? cam.PaymentOptions__c.split('\n') : new list<string>());
            for (string option : options)
            {
                pAmountOptionsCampaign.add(new SelectOption(option.split(' ')[0].trim(), option));
            }
            pAmountOptionsCampaign.add(new SelectOption('אחר', 'אחר'));
        }
        else
        {
            pAmountOptionsCampaign = null;
        }
		*/
    }
    
    private Boolean Validator()
    {
        
        boolean ret = true;
        /*
        if (pContact.FirstName == null)
        {
            ret = false;
            pContact.FirstName.addError('שדה זה לא יכול להישאר ריק');
        }
        if (pContact.LastName == null)
        {
            ret = false;
            pContact.LastName.addError('שדה זה לא יכול להישאר ריק');
        }
        if (pContact.Field15__c == null)
        {
            ret = false;
            pContact.Field15__c.addError('שדה זה לא יכול להישאר ריק');
        }
        if (pContact.Email == null)
        {
            ret = false;
            pContact.Email.addError('שדה זה לא יכול להישאר ריק');
        }
        if (pPaymentType == null || pPaymentType == '')
        {
            ret = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'השדה סוג תשלום לא יכול להישאר ריק'));
        }
        if (pPaymentType != 'Donation' && pPaymentType != 'JoinToShaldag')
        {
            list<CampaignMember> oldCampaignMember = CSobjectDb.GetSobjectByFilter('CampaignId = \'' + pPaymentType + '\' and ContactId = \'' + pContact.Id + '\'', CampaignMember.SobjectType);
            if (oldCampaignMember.size() > 0)
            {
                ret = false;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'כבר שילמת על אירוע זה.'));
            }
        }
        if (pMinAmountJoin > pAmount && pPaymentType == 'JoinToShaldag')
        {
            ret = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'השדה סכום לחיוב חייב להיות גדול מ' + pMinAmountJoin));
        }
*/
        return ret;
		
    }
    
    public Pagereference GoToPaymentInPelecard()
    {
        /*
        if (Validator())
        {
            SetContactById();
            upsert pContact;
       
            PelecardSession__c newPelecardSession = new PelecardSession__c();
            newPelecardSession.Contact__c = pContact.Id;
            newPelecardSession.Amount__c = pAmount + pAmountTickets;
            if (pPaymentType == 'JoinToShaldag')
                newPelecardSession.JoinToShaldag__c = true;
            else if (pPaymentType != 'Donation') {
				//campaign
                newPelecardSession.Campaign__c = pPaymentType;
				newPelecardSession.spouse__c = pSpouse;
			}
            insert newPelecardSession;
            
            return new Pagereference(page.PaymentInPelecart.getUrl() + '?session=' + newPelecardSession.Id);
        }
        else
        {
            return null;
        }
		*/
        return null;
    }
    
    public void SetContactById()
    {
        /*
        list<Contact> contacts = CSobjectDb.GetSobjectByFilter('Field15__c = \'' + pContact.Field15__c + '\'', Contact.SobjectType);
        if (contacts.size() > 0)
        {
            map<string, Schema.SObjectField> contactsFieldsMap = Schema.SObjectType.Contact.fields.getMap();
            for (string fieldName : contactsFieldsMap.keySet())
            {
                Schema.SObjectField field = contactsFieldsMap.get(fieldName);
                if (field.getDescribe().isUpdateable() && pContact.get(field) != null)
                    contacts[0].put(field, pContact.get(field));
            }
            pContact = contacts[0];
        }
        else if (pPaymentType == 'JoinToShaldag')
        {
            pContact.Field16__c = 'נדרש אישור';
         }
		*/
    }
	
}