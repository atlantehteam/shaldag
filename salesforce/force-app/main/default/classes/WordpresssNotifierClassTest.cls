@isTest public class WordpresssNotifierClassTest {
    @isTest static void testCallout() {
        Test.setMock(HttpcalloutMock.class, new MockGenerator_Test());
        Test.startTest();
        Event__c event = new Event__c();
        // event.Date__c = system.today();
        event.Name = 'Test Name';
        insert event;
        Test.stopTest();
    }
    
    public class MockGenerator_Test implements HttpCalloutMock {
        public httpResponse respond(HTTPrequest req) {
            
            System.assertEquals('callout:wp_site/wp-admin/admin-ajax.php?action=import_events', req.getEndpoint());
            System.assertEquals('GET', req.getMethod());
            
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(200);
            return res;
        }
	}
}