public with sharing class PaymentSuccessHandler {
	public void handle(string contactId, decimal amount, decimal donationAmount)
	{
        Contact contact = (Contact) CSobjectDb.GetSobjectById(contactId, Contact.SobjectType);
		if (donationAmount > 0)
		{
            InsertOppDonation(contact, donationAmount);
		}
		if (amount > 0)
		{
			InsertOppJoinToShaldag(contact, amount);
		}
	}

    public void handleEventPayment(string contactId, string eventId, decimal amount, decimal numParticipants) {
        Contact contact = (Contact) CSobjectDb.GetSobjectById(contactId, Contact.SobjectType);
        Event__c event = (Event__c) CSobjectDb.GetSobjectById(eventId, Event__c.SobjectType);
        string opportunityId = NULL;
        if (amount > 0) {
            opportunityId = InsertOppEvent(contact, event, amount);
        }
        EventRegistration__c er = new EventRegistration__c();
        er.Name = event.Name + ' - ' + contact.Name;
        er.Event__c = event.Id;
        er.Contact__c = contact.Id;
        er.NumParticipants__c = numParticipants;
        er.Opportunity__c = opportunityId;
        insert er;
    }
	
	private void InsertOppJoinToShaldag(Contact contact, decimal amount)
	{
		if (contact.Field12__c == null)
		{
			contact.Field12__c = 'חבר עמותה;';
			upsert contact;
		}
		else if (!contact.Field12__c.Contains('חבר עמותה'))
		{
			contact.Field12__c += ';חבר עמותה;';
			upsert contact;
		}
		Opportunity newOpportunity = new Opportunity();
		newOpportunity.AccountId = contact.AccountId;
		newOpportunity.email__c = contact.Email;
		newOpportunity.StageName = 'Posted';
		newOpportunity.CloseDate = System.today();
		newOpportunity.Amount = amount;
		newOpportunity.Name = 'תשלום דמי חבר עמותה- ' + contact.FirstName + ' ' + contact.LastName;
		newOpportunity.RecordTypeId = [select Id from RecordType where SobjectType = 'Opportunity' and DeveloperName = 'Membership' limit 1].Id;
		date startDate = GetLastEndDate(contact);
		newOpportunity.npe01__Membership_Start_Date__c = startDate;
		newOpportunity.npe01__Membership_End_Date__c = startDate.addYears(1);

		upsert newOpportunity;

        System.enqueueJob(new ReceiptMakerCls(newOpportunity));
	}
	
    private void InsertOppDonation(Contact contact, decimal donationAmount)
	{
		Opportunity newOpportunity = new Opportunity();
		newOpportunity.AccountId = contact.AccountId;
		newOpportunity.email__c = contact.Email;
		newOpportunity.StageName = 'Posted';
		newOpportunity.CloseDate = System.today();
		newOpportunity.Amount = donationAmount;
		if (contact.Field12__c == null)
		{
			contact.Field12__c = 'תורם;';
			upsert contact;
		}
		else if (!contact.Field12__c.Contains('תורם'))
		{
			contact.Field12__c += ';תורם;';
			upsert contact;
		}
		newOpportunity.Name = 'תרומה- ' + contact.FirstName + ' ' + contact.LastName;
		newOpportunity.RecordTypeId = [select Id from RecordType where SobjectType = 'Opportunity' and DeveloperName = 'Grant' limit 1].Id;
		insert newOpportunity;

        System.enqueueJob(new ReceiptMakerCls(newOpportunity));
	}
    
    private string InsertOppEvent(Contact contact, Event__c event, decimal donationAmount)
	{
		Opportunity newOpportunity = new Opportunity();
		newOpportunity.AccountId = contact.AccountId;
		newOpportunity.email__c = contact.Email;
		newOpportunity.StageName = 'Posted';
		newOpportunity.CloseDate = System.today();
		newOpportunity.Amount = donationAmount;

		newOpportunity.Name = 'הרשמה לאירוע - ' + event.Name + '(' +contact.FirstName + ' ' + contact.LastName +')';
		newOpportunity.RecordTypeId = [select Id from RecordType where SobjectType = 'Opportunity' and DeveloperName = 'Event' limit 1].Id;
		insert newOpportunity;
        
        System.enqueueJob(new ReceiptMakerCls(newOpportunity));

        return newOpportunity.Id;
	}
    
	private date GetLastEndDate(Contact contact)
	{
		date lastEndDate = system.today();
		Opportunity[] oldOpportunities = [select npe01__Membership_End_Date__c from Opportunity where npe01__Membership_End_Date__c > :lastEndDate and AccountId = :contact.AccountId];
		for (Opportunity opp : oldOpportunities)
		{
			if (opp.npe01__Membership_End_Date__c  > lastEndDate)
				lastEndDate = opp.npe01__Membership_End_Date__c;
		}
		return lastEndDate.addDays(1);
	}
}