(function($) {
  function makeAjaxRequest(url, data) {
    return new Promise((resolve, reject) => {
      $.post(url, data)
      .done(resolve)
      .fail(err => {
        console.error(err);
        reject(err);
      })
    })
  }

  function signupForm() {
    $(document).on('submit', '.sf-signup', function(e) {
      e.preventDefault();
      const $form = $(this);
      const $loginBtn = $form.find('#sfLoginTrigger');

      $form.addClass('loading');
      $loginBtn.addClass('sf-hide');
      const $resWrapper = $form.find('.sf-response').addClass('sf-hide')
      const fields = $form.serializeArray().reduce(function(obj, item) {
        obj[item.name] = item.value;
        return obj;
      }, {});
      const data = Object.assign(fields, {action: 'sf_signup'});
      makeAjaxRequest(Sldg.ajax_url, data)
      .catch(err => err.responseJSON)
      .then(res => {
        $resWrapper.removeClass('sf-hide')
          .toggleClass('sf-success', res.success)
          .toggleClass('sf-failure', !res.success)
          .text(res.data.message)
        $form.removeClass('loading');
        if (res.data.code === 'userExists') {
          $form.find('#sfLoginTrigger').removeClass('sf-hide');
        }
      })
    })
  }
  
  function populateForms() {
    const currentYear = new Date().getFullYear();
    const firstYear = currentYear - 100;
    $('.sf-field.datepicker input').datepicker({
      changeYear: true,
      yearRange: `${firstYear}:${currentYear}`,
      dateFormat:  'yy-mm-dd',
    })
  }

  function prepareFormData($form) {
    const action = $form.attr('data-action');
    const fields = $form.serializeArray().reduce(function(obj, item) {
      obj[item.name] = item.value;
      return obj;
    }, {});
    const data = Object.assign(fields, {action});
    return data;
  }

  function setFormSection($form, containerCls, errors) {
    const errsMap = (errors || []).reduce((aggr, err) => {
      aggr[err.key] = err.msg;
      return aggr;
    }, {});
    $inputs = $form.find(':input[name]');
    $inputs.each(function() {
      const $this = $(this);
      if (errsMap[$this.attr('name')]) {
        $this.addClass('has-error');
      }
    })

    const $errAggr = $form.find('[data-err]');
    $errAggr.each(function() {
      const $this = $(this);
      if (errsMap[$this.attr('data-err')]) {
        $this.find(':input[name]').addClass('has-error');
      }
    })

    const errMessages = Object.values(errsMap);
    const liMessagesStr = errMessages.map(err => `<li class="sf-err-msg">${err}</li>`).join('\n');
    const errListElement = `<ul class="sf-err-list">${liMessagesStr}</ul>`;
    $form.find(containerCls).html(errListElement).toggleClass('sf-hide', !errMessages.length);
  }
  function setFormErrors($form, {errors, submitErrors}) {
    $form.find(':input[name]').removeClass('has-error');
    setFormSection($form, '.sf-form-errors', errors);
    setFormSection($form, '.sf-submit-errors', submitErrors);
  }
  function sendForms() {
    $('.sf-form.sf-form-default').submit(function(e) {
      e.preventDefault();
      const $form = $(this);
      const $resWrapper = $form.find('.sf-response').text('').addClass('sf-hide')
      $form.addClass('loading');
      const formData = prepareFormData($form);
      setFormErrors($form, {});
      console.log(formData);
      
      makeAjaxRequest(Sldg.ajax_url, formData)
      .catch(err => {
        const data = err.responseJSON;
        setFormErrors($form, data);
        return data;
      })
      .then(function(res) {
        if (res.data && res.data.message) {
          $resWrapper.removeClass('sf-hide')
            .toggleClass('sf-success', res.success)
            .toggleClass('sf-failure', !res.success)
            .text(res.data.message)
        }
        $form.removeClass('loading');
        if (res.success && $form.hasClass('sf-reset')) {
          $form.trigger("reset");
        }
      })
    })
  }

  function updatePaymentTotals($form) {
    const amountRaw = $form.find('[name=amount]').val() || '';
    const amount = parseFloat(amountRaw.split('_')[0]) || 0;
    const donationAmount = parseFloat($form.find('[name=donationAmount]').val().split('_')[0]) || 0;
    const donationAmountOpen = parseFloat($form.find('[name=donationAmountOpen]').val()) || 0;
    const total = (amount + donationAmount + donationAmountOpen).toFixed(2);
    $form.find('.sf-total-pay').text(total);
  }

  function toggleOpenDonation() {
    const $this = $(this);
    const $form = $this.closest('.sf-form');
    const val = $this.val();
    const hideOther = val !== 'other';
    $form.find('[name=donationAmountOpen]').toggleClass('sf-hide', hideOther).val('');
  }
  function handleMembershipForm() {
    $('.sf-payment, .sf-donation').find('select[name=donationAmount]').on('change reset', toggleOpenDonation)
    $('.sf-payment, .sf-donation').find(':input').on('keyup change', function() {
      const $this = $(this);
      const $form = $this.closest('.sf-form');
      updatePaymentTotals($form);
    });
    $('.sf-payment, .sf-donation').each(function() {updatePaymentTotals($(this))});
  }
  function updateCost() {
    $('.sf-form.sf-has-cost').find('[data-cost-multiplier]').change(function() {
      const $this = $(this);
      const multiplier = parseFloat($this.val() || 0);
      const $form = $this.closest('.sf-form');
      const cost = parseFloat($form.attr('data-sf-price'));
      const total = multiplier * cost;
      $form.find('.cost').text(total);
      $form.find('[name=amount]').val(total);
    })
    $('.sf-payment :input').on('keyup change', function() {
      const $this = $(this);
      const $form = $this.closest('.sf-payment');
      updatePaymentTotals($form);
    });
    $('.sf-payment').each(function() {updatePaymentTotals($(this))});
  }

  function enforceFieldPattern() {
    $(document).on('keyup', 'input[data-credit]', function(e) {
      const digitsOnly = e.target.value.replace(/[^\d]/g, '');
      const separatedDigits = (digitsOnly.match(/.{1,4}/g) || []).join('-').slice(0, e.target.maxLength);
      e.target.value = separatedDigits;
    })
  }

  function handleAccordions() {
    $('.sf-accordion').each(function() {
      const $accordion = $(this);
      $accordion.find('.sf-accordion-trigger').click(function() {
        $accordion.toggleClass('open');
      })
    })
  }

  function handleLoginForm() {
    $('.sf-login-btn').click(function() {
      const data = {action: 'sf_get_signup'};
      makeAjaxRequest(`${Sldg.ajax_url}?t=${Date.now()}`, data)
      .then(a => $('.sf-login-wrapper').html(a.data))
      .catch(err => {
        console.error(err)
        $('.sf-login-wrapper').html('<div style="text-align: center; color: red;">לא ניתן לבצע כרגע הרשמה</div>')
      })
    })
  }

$(function() {
    signupForm();
    populateForms();
    sendForms();
    handleMembershipForm();
    enforceFieldPattern();
    updateCost();
    handleAccordions();
    handleLoginForm()
})
})(jQuery);
