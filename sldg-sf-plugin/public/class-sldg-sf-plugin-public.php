<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://github.com/atlanteh
 * @since      1.0.0
 *
 * @package    Sldg_Sf_Plugin
 * @subpackage Sldg_Sf_Plugin/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Sldg_Sf_Plugin
 * @subpackage Sldg_Sf_Plugin/public
 * @author     Roi Nagar <atlanteh@gmail.com>
 */
class Sldg_Sf_Plugin_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Sldg_Sf_Plugin_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Sldg_Sf_Plugin_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/sldg-sf-plugin-public.css', array(), $this->version, 'all' );
		wp_enqueue_script( 'jquery-ui-datepicker' );
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Sldg_Sf_Plugin_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Sldg_Sf_Plugin_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/sldg-sf-plugin-public.js', array( 'jquery' ), $this->version, false );
		wp_localize_script( $this->plugin_name, 'Sldg', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
		wp_register_style( 'jquery-ui', 'https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css' );
		wp_enqueue_style('jquery-ui'); 
	}

	public function should_show_admin_bar() {
		$user = is_user_logged_in() ? wp_get_current_user() : false;
		if (empty($user)) return false;
		
		return !array_intersect(['sf_user', 'sf_agent'], $user->roles);
	}

	public function logout_shortcode() {
		return wp_logout_url();
	}
}
