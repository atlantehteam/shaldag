<?php

/**
 * Fired during plugin activation
 *
 * @link       https://github.com/atlanteh
 * @since      1.0.0
 *
 * @package    Sldg_Sf_Plugin
 * @subpackage Sldg_Sf_Plugin/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Sldg_Sf_Plugin
 * @subpackage Sldg_Sf_Plugin/includes
 * @author     Roi Nagar <atlanteh@gmail.com>
 */
class Sldg_Sf_Plugin_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		add_role( 'sf_user', __('משתמש עמותה', 'sldg-sf-plugin'), array());
		add_role( 'sf_agent', __('נציג', 'sldg-sf-plugin'), array());
	}

}
