<?php

class Sldg_SF_Admin_View {
    public function __construct() {
      $this->sfApi = new Sldg_SF_Api();
      $this->login = new Sldg_SF_Login();
    }

    private function get_search_form() {
      global $sldg_errors;
      ob_start();
      if ($sldg_errors->has_errors('page_errors')) {
        $errorSvg = '<svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 15h-2v-2h2v2zm0-4h-2V7h2v6z"/></svg>';
        $main_error = $sldg_errors->get_errors('page_errors')[0];
        echo "<div class='sldg-page-errors'>
          <div class='sldg-main-error'>$errorSvg<span>$main_error</span></div>
        </div>";
      }
      ?>
      <form class="sf-form sf-update-details" method="POST">
        <h2 class="sf-section-title"><?= __('חיפוש משתמש', 'sldg-sf-plugin') ?></h2>
        <div class="sf-loader"><div class="sf-loader-anim"></div></div>
        <div class="sf-row sf-row-collapse">
          <label class="sf-field">
            <span class="sf-field-title"><?= __('ת"ז', 'sldg-sf-plugin') ?></span>
            <input class="sf-field-el sf-field-input" name="nationalId" type="number" maxlength="9" />
          </label>
          <label class="sf-field">
            <span class="sf-field-title"><?= __('אימייל', 'sldg-sf-plugin') ?></span>
            <input class="sf-field-el sf-field-input" name="email" type="email" />
          </label>
        </div>
        <input type="hidden" name="sldg_action" value="search_user" />
        <button type="submit" class="sf-form-submit sf-primary"><?= __('חיפוש', 'sldg-sf-plugin') ?></button>
        <div class="sf-response sf-hide"></div>
      </form>
      <?php
      $output = ob_get_contents();   
      ob_end_clean();   
      return $output;
    }

    public function shortcodeAdminView() {
      if (!Sldg_Utils::can_see_admin_view()) {return __('לא ניתן להציג עמוד זה. יש להתחבר כמנהל', 'sldg-sf-plugin');}

      $search_class = empty($_GET['sfId']) ? 'open' : '';
      $details_class = empty($_GET['sfId']) ? '' : 'open';
      $output = '';
      $output .= '<div class="sf-accordion '.$search_class.'">
        <button class="sf-accordion-trigger">'.__('חיפוש', 'sldg-sf-plugin').'</button>
        <div class="sf-accordion-content">'.$this->get_search_form().'</div>'
      .'</div>';
      if (!empty($_GET['sfId'])) {
        $output .= '<div class="sf-accordion '.$details_class.'">
            <button class="sf-accordion-trigger">'.__('עדכון נתונים', 'sldg-sf-plugin').'</button>
            <div class="sf-accordion-content">'.do_shortcode('[shaldag_contact_form]').'</div>'
        .'</div>';

        $output .= '<div class="sf-accordion">
          <button class="sf-accordion-trigger">'.__('תשלום דמי חבר', 'sldg-sf-plugin').'</button>
          <div class="sf-accordion-content">'.do_shortcode('[shaldag_member_payment]').'</div>'
        .'</div>';

        $output .= '<div class="sf-accordion">
          <button class="sf-accordion-trigger">'.__('תרומה', 'sldg-sf-plugin').'</button>
          <div class="sf-accordion-content">'.do_shortcode('[shaldag_donation_payment]').'</div>'
        .'</div>';
      }
      return $output;
    }

    public function admin_actions()
    {
      if (in_array('sf_agent',  (array) wp_get_current_user()->roles)) {
        $admin_url = '/agent/';
        if (substr($_SERVER['REQUEST_URI'], 0, strlen($admin_url)) !== $admin_url) {
          wp_safe_redirect('/agent/');
        }
      }

      if (!Sldg_Utils::can_see_admin_view()) {return;}
      if (empty($_POST['sldg_action'])) {return;}

      if ($_POST['sldg_action'] == 'search_user') {
        $this->search_sf_user();
      }

    }

    private function search_sf_user() {
      $nationalId = $_POST['nationalId'] ?? '';
      $email = $_POST['email'] ?? '';
      $user_fields = compact('email', 'nationalId');
      $user_fields = array_filter($user_fields);
      if (empty($user_fields)) {return;}

      $user = $this->sfApi->getUserByFields($user_fields);

      if (!$user) {
        global $sldg_errors;
        $sldg_errors->add_error(
          'page_errors',
          __("משתמש לא נמצא", 'sldg-sf-plugin'),
        );
      } else {
        $sfId = $user['id'];
        $url_path = strtok($_SERVER['REQUEST_URI'], '?');
        header("Location: {$url_path}?sfId=$sfId", true, 303);
      }
    }

    public function redirect_on_login($login_redirect, $requested, $user)
    {
      if (!is_a($user, 'WP_User') || !$user->exists()) {
        return $login_redirect;
      }

      // agent redirect
      $valid_roles = array('sf_agent');
      if (array_intersect($valid_roles, (array) $user->roles)) {
        $pageId = jet_engine()->listings->data->get_option( 'theme-settings::agent_redirect_page' );
        if ($pageId) {
          wp_safe_redirect(get_permalink($pageId));
          exit;
        }
      }
      
      // amuta user redirect
      $sfUserId = $this->login->getSFUserIdById($user->ID);
      if (!$sfUserId) {
        return $login_redirect;
      }
      $updateDate = get_user_meta( $user->ID, 'sf_updated_profile_at', true);
      if ($updateDate) {
        return $login_redirect;
      }

      $account_page_id = jet_engine()->listings->data->get_option( 'theme-settings::sf_personal_deails_page' );
      if (!$account_page_id) {
        wp_safe_redirect('account-page-not-found');
      } else {
        wp_safe_redirect(get_permalink($account_page_id));
      }
      exit;
    }
}