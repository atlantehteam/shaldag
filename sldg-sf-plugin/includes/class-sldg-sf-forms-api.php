<?php

class Sldg_SF_Forms_Api {
    private $formToSObject = array(
        'events' => 'EventRegistration__c',
        'mentoring' => 'MentoringObject__c',
        'scholars' => 'Scholarships__c',
        'recruit' => 'RecruitReferral__c',
        'communities' => 'CommunityMember__c',
        'social' => 'Volunteers__c',
    );
    private $formToParent = array(
        'events' => 'Event__c',
        'mentoring' => 'CustomObject2__c',
        'scholars' => 'CustomObject3__c',
        'recruit' => 'RecruitCycle__c',
        'communities' => 'Community__c',
    );
    private $formParentField = array(
        'events' => 'Event__c',
        'mentoring' => 'Field13__c',
        'scholars' => 'Field17__c',
        'recruit' => 'RecruitCycle__c',
        'communities' => 'Community__c',
    );
    private $formContactField = array(
        'events' => 'Contact__c',
        'mentoring' => 'Field14__c',
        'scholars' => 'Field1__c',
        'communities' => 'Contact__c',
        'recruit' => 'Contact__c',
        'social' => 'Contact__c',
    );
    public function getFormParentObject($formName) {
        return $this->formToParent[$formName] ?? null;
    }
    
    public function getFormObject($formName) {
        return $this->formToSObject[$formName] ?? null;
    }

    public function prepareFormRequests($formName, $fields, $user, $parents) {
        $results = [];
        foreach ($parents as $parent) {
            $request = $fields;
            if (isset($this->formParentField[$formName])) {
                $request[$this->formParentField[$formName]] = $parent['Id'];
            }
            if (isset($this->formContactField[$formName])) {
                $request[$this->formContactField[$formName]] = $user['id'];
            }
            switch ($formName) {
                case 'events':
                    $request['Name'] = $parent['Name'].' ('.$user['displayName'].')';
                    break;
                case 'scholars':
                    $request['Name'] = $user['displayName'].'-'.$parent['Name'];
                    $request['Field2__c'] = 'הגיש בקשה';
                    break;
                case 'recruit':
                    $request['Name'] = $user['displayName'].' '.__('ממליץ על', 'sldg-sf-plugin').' '.$fields['FirstName__c']. ' '.$fields['LastName__c'];
                    break;
                case 'communities':
                    $request['Name'] = $user['displayName'].'-'.$parent['Name'];
                    $request['Status__c'] = 'Applied';
                    break;
                case 'social':
                    $request['Name'] = $user['displayName'];
                    $request['VolunteerType__c'] = str_replace(', ', ';', $request['VolunteerType__c'] ?? '');
                    $request['VolunteerArea__c'] = str_replace(', ', ';', $request['VolunteerArea__c'] ?? '');
                    $request['MentoringAreas__c'] = str_replace(', ', ';', $request['MentoringAreas__c'] ?? '');
                    break;
            }
            $results []= $request;
        }
        return $results;
    }
}