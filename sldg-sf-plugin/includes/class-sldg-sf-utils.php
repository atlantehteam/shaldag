<?php

class Sldg_Utils {
    public static function array_any(array $array, callable $fn) {
        foreach ($array as $value) {
            if($fn($value)) {
                return true;
            }
        }
        return false;
    }

    public static function starts_with($string, $startString) 
    { 
        $len = strlen($startString); 
        return (substr($string, 0, $len) === $startString); 
    }

    public static function ends_with( $haystack, $needle ) {
        $length = strlen( $needle );
        if( !$length ) {
            return true;
        }
        return substr( $haystack, -$length ) === $needle;
    }

    public static function array_pick($input, $keys) {
        $res = array();
        foreach($keys as $key) {
            $res[$key] = (is_object($input) ? $input->{$key} : $input[$key]);
        }
        return $res;
    }

    public static function is_site_admin() {
        return in_array('administrator',  wp_get_current_user()->roles);
    }

    public static function can_see_admin_view() {
        return array_intersect(['administrator', 'sf_agent'],  (array) wp_get_current_user()->roles);
    }

    public static function is_sf_user($user) {
		if (empty($user)) return false;
		return in_array('sf_user', $user->roles);
	}

    public static function is_simple_float($val) {
        $pattern = '/^\d+(\.\d+)?$/';
        return (!is_bool($val) && (is_float($val) || preg_match($pattern, trim($val))));
	}
}