<?php

class Wp_Sldg_Errors
{
  private $errors = array();
  public function add_error($group, $error)
  {
    if (!isset($this->errors[$group])) {
      $this->errors[$group] = array();
    }
    $this->errors[$group][] = $error;
  }
  public function get_errors($group)
  {
    return $this->errors[$group] ?? array();
  }
  public function has_errors($group)
  {
    return !empty($this->errors[$group]);
  }
}

$GLOBALS['sldg_errors'] = new Wp_Sldg_Errors();
