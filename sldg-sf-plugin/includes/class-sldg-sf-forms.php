<?php

class Sldg_SF_Forms {
    public function __construct() {
      $this->sfApi = new Sldg_SF_Api();
      $this->login = new Sldg_SF_Login();
    }

    public function contactForm() {
      if (!is_user_logged_in()) {
        return '<div>'.__('יש להתחבר על מנת לערוך פרטים', 'sldg-sf-plugin').'</div>';
      }
      $userId = $this->login->getSFUserId(true);
      if (!$userId) {
        return '<div>'.__('משתמש עמותה לא תקין', 'sldg-sf-plugin').'</div>';
      }
      
      $sfUser = $this->sfApi->getUserById($userId);
      if (!$sfUser) {
        return '<div>'.__('משתמש עמותה לא קיים', 'sldg-sf-plugin').'</div>';
      }
      $sfFields = $this->sfApi->getContactFields();
      $sfFieldsMap = array_combine(array_column($sfFields, 'name'), $sfFields);
      ob_start();
        ?>
          <form class="sf-contact-form sf-form sf-form-default" data-action="contact_form">
            <div class="sf-loader"><div class="sf-loader-anim"></div></div>
            <h3 class="sf-form-section-title"><?= __('חברות עמותה', 'sldg-sf-plugin') ?></h3>
            <div class="sf-row sf-row-collapse">
              <?php $this->processField($sfFieldsMap['membershipEndDate'], $sfUser); ?>
              <label class="sf-field"></label><label class="sf-field"></label>
            </div>
            <h3 class="sf-form-section-title"><?= __('פרטים אישיים', 'sldg-sf-plugin') ?></h3>
            <div class="sf-row sf-row-collapse">
              <?php $this->processField($sfFieldsMap['firstName'], $sfUser); ?>
              <?php $this->processField($sfFieldsMap['lastName'], $sfUser); ?>
              <?php $this->processField($sfFieldsMap['nationalId'], $sfUser); ?>
            </div>
            <div class="sf-row sf-row-collapse">
              <?php $this->processField($sfFieldsMap['gender'], $sfUser); ?>
              <?php $this->processField($sfFieldsMap['birthdate'], $sfUser); ?>
            </div>
            <div class="sf-row sf-row-collapse">
              <?php $this->processField($sfFieldsMap['maritalStatus'], $sfUser); ?>
              <?php $this->processField($sfFieldsMap['numKids'], $sfUser); ?>
            </div>
            <h3 class="sf-form-section-title"><?= __('פרטי יצירת קשר', 'sldg-sf-plugin') ?></h3>
            <div class="sf-row sf-row-collapse">
              <?php $this->processField($sfFieldsMap['mobilePhone'], $sfUser); ?>
              <?php $this->processField($sfFieldsMap['email'], $sfUser, ['comment' => __( 'לשינוי אימייל יש לפנות לליאור: lior@shaldag.org.il', 'sldg-sf-plugin')]); ?>
            </div>
            <h3 class="sf-form-section-title"><?= __('כתובת', 'sldg-sf-plugin') ?></h3>
            <div class="sf-row sf-row-collapse">
              <?php $this->processField($sfFieldsMap['street'], $sfUser); ?>
              <?php $this->processField($sfFieldsMap['city'], $sfUser); ?>
            </div>
            <div class="sf-row sf-row-collapse">
              <?php $this->processField($sfFieldsMap['zipCode'], $sfUser); ?>
              <?php $this->processField($sfFieldsMap['country'], $sfUser); ?>
            </div>
            <h3 class="sf-form-section-title"><?= __('לימודים ועבודה', 'sldg-sf-plugin') ?></h3>
            <div class="sf-row sf-row-collapse">
              <?php $this->processField($sfFieldsMap['enlistmentYear'], $sfUser); ?>
              <?php $this->processField($sfFieldsMap['studyField'], $sfUser); ?>
              <?php $this->processField($sfFieldsMap['studyInstitution'], $sfUser); ?>
            </div>
            <div class="sf-row sf-row-collapse">
              <?php $this->processField($sfFieldsMap['occupation'], $sfUser); ?>
              <?php $this->processField($sfFieldsMap['company'], $sfUser); ?>
            </div>
            <input type="hidden" name="sfId" value="<?=$userId?>" />
            <button type="submit" class="sf-form-submit sf-primary"><?= __('עדכון', 'sldg-sf-plugin') ?></button>
            <div class="sf-response sf-hide"></div>
          </form>
        <?php
        $output = ob_get_contents();   
        ob_end_clean();   
        return $output;
    }

    public function shortcodeEventForm() {
      global $post;
      ob_start();
      
      $eventForm = new Sldf_EventForm();
      $eventForm->form_shortcode($post->ID);
      
      $output = ob_get_contents();
      ob_end_clean();
      return $output;
    }

    public function process_register_event() {
      $form = new Sldf_EventForm();
      $form->process($_POST);
    }

    public function shortcodeScholarForm() {
      ob_start();

      $form = new Sldf_ScholarForm();
      $form->form_shortcode();

      $output = ob_get_contents();
      ob_end_clean();
      return $output;
    }

    public function shortcodeRecruitForm() {
      ob_start();

      $form = new Sldf_RecruitForm();
      $form->form_shortcode();

      $output = ob_get_contents();
      ob_end_clean();
      return $output;
    }

    public function shortcodeMentoringForm() {
      ob_start();
      
      $form = new Sldf_MentoringForm();
      $form->form_shortcode();

      $output = ob_get_contents();
      ob_end_clean();
      return $output;
    }

    private function processField($field, $sfUser, $options = []) {
      switch($field['type']) {
        case 'phone':
        case 'email':
        case 'string':
        case 'textarea':
        case 'date':
        case 'double':
          $this->processInputField($field, $sfUser, $options);
          break;
        case 'picklist':
          $this->processSelectField($field, $sfUser);
          break;
      }
    }

    private function is_field_editable($fieldName) {
      $disabled_fields = ['firstName', 'lastName', 'email', 'nationalId', 'membershipEndDate'];
      return !in_array($fieldName, $disabled_fields);
    }

    private function get_field_label($field) {
      switch($field['name']) {
        case 'firstName':
          return __( 'שם פרטי', 'sldg-sf-plugin');
        case 'lastName':
          return __( 'שם משפחה', 'sldg-sf-plugin');
        case 'salutation':
          return __( 'תואר', 'sldg-sf-plugin');
        case 'street':
          return __( 'רחוב', 'sldg-sf-plugin');
        case 'city':
          return __( 'עיר', 'sldg-sf-plugin');
        case 'state':
          return __( 'מחוז', 'sldg-sf-plugin');
        case 'zipCode':
          return __( 'מיקוד', 'sldg-sf-plugin');
        case 'country':
          return __( 'מדינה', 'sldg-sf-plugin');
        case 'mobilePhone':
          return __( 'טלפון סלולרי', 'sldg-sf-plugin');
        case 'homePhone':
          return __( 'טלפון בבית', 'sldg-sf-plugin');
        case 'email':
          return __( 'אימייל', 'sldg-sf-plugin');
        case 'birthdate':
          return __( 'יום הולדת', 'sldg-sf-plugin');
        case 'membershipEndDate':
          return __( 'תאריך סיום חברות עמותה', 'sldg-sf-plugin');
      default:
        return $field['label'];
      }
    }
    private function processInputField($field, $sfUser, $options = []) {
      $min = '';
      switch($field['type']) {
        case 'double':
          $type = 'number';
          $min = 'min="0"';
          break;
        case 'phone':
          $type = 'tel';
          break;
        case 'email':
          $type = 'email';
          break;
        default:
          $type = 'text';
      }

      $class = $field['type'] == 'date' ? 'datepicker' : '';
      $readonly = $field['type'] == 'date' ? 'readonly' : '';
      $name = $field['name'];
      $length = $field['length'] ?? '';
      $value = !empty($sfUser[$name]) ? 'value="'.$sfUser[$name].'"' : '';
      $label = $this->get_field_label($field);
      $disabled = $this->is_field_editable($name) ? '' : 'disabled="disabled"';
      $comment = !empty($options['comment']) ? '<span class="sf-field-comment">'.$options['comment'].'</span>' : '';
      ?>
      <label class="sf-field <?= $class ?>">
        <span class="sf-field-title"><?= $label ?></span>
        <input class="sf-field-el sf-field-input" name=<?= $name ?> type="<?= $type ?>" maxlength="<?= $length ?>" <?= "$value $min $disabled $readonly" ?> />
        <?= $comment ?>
      </label>
      <?php
    }

    private function processSelectField($field, $sfUser) {
      $name = $field['name'];
      $options = $field['picklistValues'];
      $value = $sfUser[$name] ?? null;
      $label = $this->get_field_label($field);
      ?>
      <label class="sf-field">
        <span class="sf-field-title"><?= $label ?></span>
        <select name=<?= $name ?> class="sf-field-el sf-field-select">
        <?php
          foreach ($options as $option) {
            $isSelected = !empty($value) ? $option['value'] == $value : !empty($option['defaultValue']);
            $selected = $isSelected ? 'selected="selected"' : '';
            echo '<option value="'.$option['value'].'"'.$selected.'>'.$option['label'].'</option>';
          }
        ?>
        </select>
      </label>
      <?php
    }
    	
    public function process_contact_form() {
      $sfUserId = $this->login->getSFUserId(true);
      if (!$sfUserId) {return;}

      $fields = $_POST;
      unset($fields['action']);
      foreach ($fields as $key => $field) {
        if (!$this->is_field_editable($key)) {
          unset($fields[$key]);
        }
      }
      try {
        $this->sfApi->updateContact($sfUserId, $fields);
        if ($this->login->getSFUserId()) { // if current user is not agent
          update_user_meta(get_current_user_id(), 'sf_updated_profile_at', current_time('mysql'));
        }

        wp_send_json(array('success' => true, 'data' => ['message' => __( 'הפרטים עודכנו בהצלחה', 'sldg-sf-plugin' )]));
      } catch (Error $err) {
        trigger_error($err);
        wp_send_json(array('success' => false, 'data' => ['message' => __( 'הפעולה נכשלה', 'sldg-sf-plugin' )]));
      }
    }
}