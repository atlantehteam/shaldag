<?php

class Sldg_SF_Api {
    private  $baseUrl = 'https://eu31.salesforce.com';
    // private  $baseUrl = 'https://cs129.salesforce.com';
    private $userModel = array(
        'Id' => 'id',
        'Name' => 'displayName',
        'Salutation' => 'salutation',
        'FirstName' => 'firstName',
        'LastName' => 'lastName',
        'Field3__c' => 'enlistmentYear',
        'Field15__c' => 'nationalId',
        'Field19__c' => 'gender',
        'Birthdate' => 'birthdate',
        'HomePhone' => 'homePhone',
        'MobilePhone' => 'mobilePhone',
        'Email' => 'email',
        'MailingStreet' => 'street',
        'MailingCity' => 'city',
        'MailingState' => 'state',
        'MailingPostalCode' => 'zipCode',
        'MailingCountry' => 'country',
        'Field1__c' => 'maritalStatus',
        'num_kids__c' => 'numKids',
        'Field18__c' => 'studyField',
        'Field20__c' => 'studyInstitution',
        'Field7__c' => 'occupation',
        'Field10__c' => 'company',
        'npo02__MembershipEndDate__c' => 'membershipEndDate',
    );

    private $eventModel = array(
        'Name' => 'name',
        'Id' => 'id',
        'Date__c' => 'date',
        'Time__c' => 'time',
        'Image__c' => 'imageUrl',
        'ImageName__c' => 'imageName',
        'Location__c' => 'location',
        'Description__c' => 'description',
        'ShowInHomepage__c' => 'showInHomepage',
        'Cost__c' => 'cost',
        'ViewOnly__c' => 'viewOnly',
    );

    private $settingsModel = array(
        'SplitPayment__c' => 'membershipPrice',
        'PaymentOptions__c' => 'memberPaymentOptions',
        'DonationPaymentOptions__c' => 'donationPaymentOptions',
    );

    private $sobjectModel = array(
        'name' => 'name',
        'label' => 'label',
        'custom' => 'isCustom',
    );

    private $customModel = array(
        'Id' => 'id',
        'Name' => 'label',
    );

    public function __construct() {
		$this->formsApi = new Sldg_SF_Forms_Api();
	}

    private function makeBasicCall($url, $options) {
        $method = $options['method'] ?? 'POST';
        $data = $options['data'] ?? null;
        $headers = $options['headers'] ?? array();
        $ch = curl_init();
        if (!$ch) {
            throw new Exception('failed to initialize');
        }
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_HTTPHEADER => $headers,
        ));
        if (!empty($data)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }

        // ********************* TO BE REMOVED *********************
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        // ********************* TO BE REMOVED *********************

        $strRes = curl_exec($ch);
        $statusCode = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);
        if ($statusCode > 299) {
            throw new Error("Salesforce api threw exception: $strRes");
        }
        $result = json_decode($strRes, true);
        return $result;
    }

    public function refreshToken() {
        $opts = array('data' => array(
            'grant_type' => 'password',
            'client_id' => '3MVG99qusVZJwhslj59z8QYySV69xHj0iokVwV_g.bUHdhxL7NMLU3ZrX8clEQy77RCv7yyNCahzny2BJTiiS',
            'client_secret' => '521FA8F8B0F43BFC283D20EB93EB112D4E87EBD061D1D85600D96D8F62471704',
            'username' => 'wpuser@shaldag.org.il',
            'password' => '33Fp&TAQe7qwTxroo0ZL73tYosEEHBWPBDM9P'
        ));
        $result = $this->makeBasicCall($this->baseUrl."/services/oauth2/token", $opts);
        return $result;
    }

    public function getObjects() {
        $url = $this->baseUrl."/services/data/v48.0/sobjects";
        $options = array('method' => 'GET');
        $response = $this->makeAuthCall($url, $options);
        return array_map(function($rec) {return $this->_fromObjectModel($rec);}, $response['sobjects']);
    }

    public function getParentFormItems($formName) {
        // $childUrl = $this->baseUrl."/services/data/v48.0/sobjects/$objName/describe";
        // $options = array('method' => 'GET');
        // $fieldsRes = $this->makeAuthCall($childUrl, $options);
        // $filteredFields = array_values(array_filter($fieldsRes['fields'], function($itm) {return $itm['cascadeDelete'] && $itm['custom'];}));
        // if (!count($filteredFields)) {return [];}
        // $masterField = $filteredFields[0];
        // $masterType = $masterField['referenceTo'][0];

        $parentObjectName = $this->formsApi->getFormParentObject($formName);
        if (!$parentObjectName) {
            throw new Error('Missing parent for form '. $formName);
        }
        return $this->_getCustomObjects($parentObjectName, $this->customModel, [$this,  '_fromCustomModel']);
    }

    public function submitParentedForm($formName, $fields, $sfUserId) {
        $user = $this->getUserById($sfUserId);
        if (!$user) {
            throw new Error("User $sfUserId doesn't exist");
        }

        $parents = $this->getFormParents($formName, $fields);
        $formObj = $this->formsApi->getFormObject($formName);
        $requests = $this->formsApi->prepareFormRequests($formName, $fields['simple'], $user, $parents);

        $this->submitFormRequests($requests, $user, $formObj, $fields);
    }

    public function submitNonParentedForm($formName, $fields, $sfUserId) {
        $user = $this->getUserById($sfUserId);


        $formObj = $this->formsApi->getFormObject($formName);
        $requests = $this->formsApi->prepareFormRequests($formName, $fields['simple'], $user, ['Dummy Send Once']);

        $this->submitFormRequests($requests, $user, $formObj, $fields);
    }

    public function submitFormRequests($requests, $user, $formObj, $fields) {
        foreach ($requests as $request) {
            $url = $this->baseUrl."/services/data/v48.0/sobjects/$formObj";
            $options = array('data' => json_encode($request), 'headers' => array('Content-Type: application/json'));
            $response = $this->makeAuthCall($url, $options);
            $formId = $response['id'];
            $this->_uploadRecordAttachments($formId, array_values($fields['uploads']));
        }
        $this->_removeRecordAttachments(array_values($fields['uploads']));
    }

    private function _uploadRecordAttachments($formId, $uploadFields) {
        foreach ($uploadFields as $field) {
            $filePath = $field['path'];
            $fileExt = pathinfo($field['value'], PATHINFO_EXTENSION);
            $fileName = $field['title'].'.'.$fileExt;
            $payload = [
                "Name" => $fileName,
                "body" => base64_encode(file_get_contents($filePath)),
                "parentId" => $formId,
            ];
            $url = $this->baseUrl."/services/data/v48.0/sobjects/Attachment";
            $options = array('data' => json_encode($payload), 'headers' => array('Content-Type: application/json'));
            $this->makeAuthCall($url, $options);
        }
    }
    private function _removeRecordAttachments($uploadFields) {
        foreach ($uploadFields as $field) {
            $filePath = $field['path'];
            if (!unlink($filePath)) {
                trigger_error('Failed to delete a SF file: '.$filePath);
            }
        }
    }

    private function getToken() {
        return $this->refreshToken()['access_token'];
    }

    private function makeAuthCall($url, $options) {
        if (!isset($options['headers'])) {
            $options['headers'] = array('Content-Type: application/json');
        }
        if (!Sldg_Utils::array_any($options['headers'], function($str) {return Sldg_Utils::starts_with($str, 'Authorization: Bearer');})) {
            $token = $this->getToken();
            $options['headers'] []= "Authorization: Bearer $token";
        }
        return $this->makeBasicCall($url, $options);
    }

    public function getEvents() {
        return $this->_getCustomObjects('Event__c', $this->eventModel, [$this,  '_fromEventModel']);
    }
    public function getRawEvents() {
        return $this->_getCustomObjects('Event__c', $this->eventModel, null);
    }

    public function getCommunities() {
        return $this->_getCustomObjects('Community__c', $this->customModel, [$this,  '_fromCustomModel']);
    }

    public function getSettings() {
        $keys = array_keys($this->settingsModel);
        $keysStr = join(',', $keys);
        $url = $this->baseUrl."/services/data/v48.0/query/?q=Select+$keysStr+from+Settings__c+WHERE+Id+!=+NULL+LIMIT+1";
        $options = array('method' => 'GET');

        $result = $this->makeAuthCall($url, $options);
        if ($result['totalSize']  == 0) {
            throw new Error('No settings found');
        }

        return $this->_fromSettingsModel($result['records'][0]);
    }

    public function getUserByNationalIdAndEmail($id, $email) {
        return $this->getUserByFields(array('nationalId' => $id, 'email' => $email));
    }

    public function getUserById($id) {
        if (!$id) {return NULL;}
        return $this->getUserByFields(array('id' => $id));
    }
    
    public function getUserByEmail($email) {
        if (!$email) {return NULL;}
        return $this->getUserByFields(array('email' => $email));
    }

    public function getUserByFields($fields) {
        $valid_fields = array();
        foreach ($this->userModel as $key => $value) {
            if (!empty($fields[$value])) {
                $valid_fields[$key] = $fields[$value];
            }
        }
        if (sizeof($valid_fields) == 0) {
            throw new Error("Invalid fields provided: ".json_encode($valid_fields));
        }
        $query = $this->_generateFilter($valid_fields);
        $userKeys = array_keys($this->userModel);
        $userKeysStr = join(',', $userKeys);
        $url = $this->baseUrl."/services/data/v48.0/query/?q=Select+$userKeysStr+from+contact+$query+LIMIT+1";
        $options = array('method' => 'GET');

        $result = $this->makeAuthCall($url, $options);
        if ($result['totalSize']  == 0) {
            return null;
        }

        return $this->_fromUserModel($result['records'][0]);
    }

    public function getContactFields() {
        $fields = $this->userModel;
        unset($fields['Id']);
        unset($fields['Name']);
        return $this->_getObjectFields('Contact', $fields);
    }

    public function updateContact($sfId, $fields) {
        $data = array();
        foreach ($this->userModel as $sfKey => $uiKey) {
            if (isset($fields[$uiKey])) {
                $data[$sfKey] = $fields[$uiKey];
            }
        }
        if (empty($data['Birthdate'])) {
            $data['Birthdate'] = null;
        }
        $url = $this->baseUrl."/services/data/v48.0/sobjects/Contact/$sfId";
        $options = array('method' => 'PATCH', 'data' => json_encode($data), 'headers' => array('Content-Type: application/json'));
        $this->makeAuthCall($url, $options);
    }

    public function makeMembershipPayment($fields) {
        $url = $this->baseUrl."/services/apexrest/makepayment?type=membership";
        $validKeys = [ 'creditCard', 'cvv', 'creditCardDateMmYy', 'donationAmount', 'amount', 'contactId'];
        $validFields = $this->_preparePaymentFields($fields, $validKeys);
        $options = array(
            'data' => json_encode(array('payment' => $validFields)),
        );
        $response = $this->makeAuthCall($url, $options);
        if (!empty($response['errors'])) {
            array_walk($response['errors'], function (& $err) {
                $err['msg'] = $err['value'];
                unset($err['value']);
            });
        }
        return $response;
    }

    public function makeDonationPayment($fields) {
        $url = $this->baseUrl."/services/apexrest/makepayment?type=donation";
        $validKeys = ['creditCard', 'cvv', 'creditCardDateMmYy', 'donationAmount', 'contactId'];
        $validFields = $this->_preparePaymentFields($fields, $validKeys);
        $options = array(
            'data' => json_encode(array('payment' => $validFields)),
        );
        $response = $this->makeAuthCall($url, $options);
        if (!empty($response['errors'])) {
            array_walk($response['errors'], function (& $err) {
                $err['msg'] = $err['value'];
                unset($err['value']);
            });
        }
        return $response;
    }

    public function registerEvent($fields) {
        $url = $this->baseUrl."/services/apexrest/registerEvent";
        $validKeys = ['creditCard', 'cvv', 'amount', 'creditCardDateMmYy', 'donationAmount', 'contactId', 'eventId', 'numParticipants'];
        $validFields = array_intersect_key($fields, array_flip($validKeys));
        $options = array(
            'data' => json_encode(array('registration' => $validFields)),
        );
        $response = $this->makeAuthCall($url, $options);
        if (!empty($response['errors'])) {
            array_walk($response['errors'], function (& $err) {
                $err['msg'] = $err['value'];
                unset($err['value']);
            });
        }
        return $response;
    }

    public function getFormParents($formName, $fields = null) {
        $parentObjectName = $this->formsApi->getFormParentObject($formName);
        if (!$parentObjectName) {
            throw new Error('Missing parent for form '. $formName);
        }

        $parents = $fields['parents'] ?? [];
        if (empty($parents)) {
            $query = 'Active__c=TRUE+LIMIT+1';
        } else {
            $parentsStr = "'".implode("','", $parents)."'";
            $query = "Id+IN+($parentsStr)";
        }
        $url = $this->baseUrl."/services/data/v48.0/query/?q=Select+Id,Name+FROM+$parentObjectName+WHERE+$query";
        $options = array('method' => 'GET');

        $result = $this->makeAuthCall($url, $options);
        if ($result['totalSize'] == 0) {
            return [];
        }
        return $result['records'];
    }

    public function hasActiveFormParent($formName) {
        $result = $this->getFormParents($formName);
        return !empty($result);
    }

    public function getVolunteerTypes() {
        return $this->getGlobalValuesSet('VolunteerTypes');
    }

    public function getVolunteerAreas() {
        return $this->getGlobalValuesSet('VolunteerAreas');
    }

    public function getMentoringAreas() {
        return $this->getGlobalValuesSet('MentoringAreas');
    }

    private function getGlobalValuesSet($setName) {
        $url = $this->baseUrl."/services/data/v41.0/tooling/query?q=select+Metadata+from+GlobalValueSet+WHERE+DeveloperName='$setName'+limit+1";
        $options = array('method' => 'GET');
        $result = $this->makeAuthCall($url, $options);

        if ($result['totalSize'] == 0) {
            throw new Error('Invalid DeveloperName dupplied:'.$setName);
        }

        $fields = $result['records'][0]['Metadata']['customValue'];
        $activeFields = array_filter($fields, function($fld) {return $fld['isActive'] !== false;});
        $mappedFields = array_map(function($fld) {return ['id' => $fld['valueName'], 'label' => $fld['label']];}, $fields);

        return $mappedFields;
    }
    private function _preparePaymentFields($fields, $validKeys) {
        $validFields = array_intersect_key($fields, array_flip($validKeys));
        if (in_array('donationAmount', $validKeys) && !empty($fields['donationAmountOpen'])) {
            $validFields['donationAmount'] = $fields['donationAmountOpen'];
        }
        return $validFields;
    }
    private function _generateFilter($fields) {
        if (!is_array($fields) || sizeof($fields) == 0) {
            return '';
        }
        $secureFields = array_map(function($val) {return esc_sql(trim($val));}, $fields);
        $queryElements = array();
        foreach ($secureFields as $key => $value) {
            if (empty($value)) {
                throw new Error("Invalid $key supplied '$value'");
            }
            $queryElements []= "$key+='$value'";
        }
        $query = join('+and+', $queryElements);
        return 'where+'.$query;
    }
    private function _getCustomObject($id, $type, $mapper = null) {
        $url = $this->baseUrl."/services/data/v48.0/sobjects/$type/$id";
        $options = array('method' => 'GET');
        $result = $this->makeAuthCall($url, $options);
        return $mapper ? $mapper($result) : (array) $result;
    }

    private function _getCustomObjects($type, $model, $mapper, $filter = array()) {
        $keys = array_keys($model);
        $keysStr = join(',', $keys);
        $url = $this->baseUrl."/services/data/v48.0/query/?q=Select+$keysStr+from+$type";
        $options = array('method' => 'GET');
        $result = $this->makeAuthCall($url, $options);
        
        return array_map(function($rec) use($mapper) {return is_callable($mapper) ? $mapper($rec) : $rec;}, $result['records']);
    }
    private function _mapField($field, $model) {
        $res = array(
            'name' => $model[$field['name']],
            'label' => $field['label'],
            'type' => $field['type'],
        );
        if (!empty($field['picklistValues'])) {
            $filtered = array_values(array_filter($field['picklistValues'], function($arg) {return $arg['active'];}));
            $pickListFields = ['defaultValue', 'label', 'value'];
            $res['picklistValues'] = array_map(function($obj) use($pickListFields) {return Sldg_Utils::array_pick($obj, $pickListFields);}, $filtered);
        }
        if (!empty($field['length'])) {
            $res['length'] = $field['length'];
        }
        return $res;
    }

    private function _getObjectFields($objId, $model) {
        $fields = array_keys($model);
        $eventKeys = array_keys($this->eventModel);
        $eventKeysStr = join(',', $eventKeys);
        $url = $this->baseUrl."/services/data/v48.0/sobjects/$objId/describe";
        $options = array('method' => 'GET');
        $response = $this->makeAuthCall($url, $options);
        $filteredFields = array_values(array_filter($response['fields'], function($rf) use ($fields) {return in_array($rf['name'], $fields);}));
        return array_map(function($arg) use ($model) {return $this->_mapField($arg, $model);}, $filteredFields);
    }

    private function _fromModel($record, $modelMap) {
        $result = array();
        foreach ($modelMap as $key => $mappedKey) {
            if (array_key_exists($key, $record)) {
                $result[$mappedKey] = $record[$key];
            }
        }
        return $result;
    }

    private function _fromUserModel($user) {
        return $this->_fromModel($user, $this->userModel);
    }
    private function _fromEventModel($event) {
        return $this->_fromModel($event, $this->eventModel);
    }
    private function _fromSettingsModel($event) {
        $settings = $this->_fromModel($event, $this->settingsModel);
        $settings['memberPaymentOptions'] = $this->_strToOptions($settings['memberPaymentOptions']);
        $settings['donationPaymentOptions'] = $this->_strToOptions($settings['donationPaymentOptions']);
        return $settings;
    }
    private function _fromObjectModel($rec) {
        return $this->_fromModel($rec, $this->sobjectModel);
    }
    private function _fromCustomModel($rec) {
        return $this->_fromModel($rec, $this->customModel);
    }
    private function _strToOptions($str) {
        $opts = explode("\n", $str);
        $result = array_map(function($itm) {
            list($value, $label) = explode('|', $itm);
            return array(
                'value' => trim($value),
                'label' => trim($label),
            );
        }, $opts);
        return $result;
    }
}