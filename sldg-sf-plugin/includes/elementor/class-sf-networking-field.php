<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

use ElementorPro\Modules\Forms\Module;

class CommunitesField extends SF_Field_Base {

	public function get_type() {
		return 'sf_communities_field';
	}

	public function get_name() {
		return __( 'בחירת קהילות ׁ(SF)', 'sldg-sf-plugin' );
	}

  protected function getApiOptions() {
    return $this->sfApi->getCommunities();
  }

  protected function isInline() {
    return true;
  }
}

$field = new CommunitesField();
Module::instance()->add_form_field_type( $field->get_type(),  $field);
