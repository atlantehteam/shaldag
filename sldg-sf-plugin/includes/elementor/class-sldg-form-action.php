<?php
/**
 * Class Sendy_Action_After_Submit
 * @see https://developers.elementor.com/custom-form-action/
 * Custom elementor form action after submit to add a subsciber to
 * Sendy list via API 
 */
class Sldg_Form_Action extends \ElementorPro\Modules\Forms\Classes\Action_Base {
	private $simpleForms = ['social'];

	public function __construct() {
		add_action( 'elementor/editor/after_enqueue_scripts', function() {
			wp_enqueue_script( 'salesforce-action', SLDG_SF_PLUGIN_URL.'admin/js/sldg-sf-form-action.js', ['elementor-editor'], '1.0.0', true);
		});

		$this->api = new Sldg_SF_Api();
		$this->login = new Sldg_SF_Login();
		$this->sfMail = new Sldg_Mail();
	}

	public function get_name() {
		return 'sf';
	}

	public function get_label() {
		return __( 'Salesforce', 'sldg-sf-plugin' );
	}

	public function run( $record, $ajax_handler ) {
		$settings = $record->get( 'form_settings' );

		if (!is_user_logged_in()) {
			$ajax_handler->add_error_message( __('חובה להיות מחובר', 'sldg-sf-plugin') );
			return;
		}
		
		$sfUserId = $this->login->getSFUserId();
		if (!$sfUserId)  {
			$ajax_handler->add_error_message( __('משתמש זה אינו רשאי למלא את הטופס', 'sldg-sf-plugin') );
			return;
		}

		$formName = $settings['sf_action_form_type'] ?? null;
		if ( empty( $formName ) ) {
			$ajax_handler->add_admin_error_message( __('סוג טופס חסר', 'sldg-sf-plugin') );
			return;
		}

		$rawFields = $record->get( 'fields' );
		$fields = array('simple' => [], 'uploads' => [], 'parents' => []);
		foreach ($rawFields as $id => $field ) {
			if (empty($id)) {
				continue;
			}
			if ($field['type'] == 'upload') {
				if (!empty($field['value'])) {
					$fields['uploads'][ $id ] = array(
						'title' => $field['title'],
						'value' => $field['value'],
						'path' => $field['raw_value'],
					);
				}
			} else if ($id == 'parentId') {
				$parents = $field['raw_value'];
				array_push($fields['parents'], ...$parents);
			} else if (Sldg_Utils::ends_with($id, '__c')) {
				$fields['simple'][ $id ] = $field['value'];
			}
		}

		$isSimpleForm = in_array($formName, $this->simpleForms);

		if ($isSimpleForm) {
			$this->api->submitNonParentedForm($formName, $fields, $sfUserId);
		} else if ($this->api->hasActiveFormParent($formName)) {
			$this->api->submitParentedForm($formName, $fields, $sfUserId);
		} else {
			$ajax_handler->add_error_message( __('הרשמה אינה זמינה. יש לפנות למנהל המערכת', 'sldg-sf-plugin') );
		}
		$this->sfMail->send_form_mail($formName);

	}


	public function register_settings_section( $widget ) {
		$widget->start_controls_section(
			'section_sf_action',
			[
				'label' => __( 'Salesforce', 'sldg-sf-plugin' ),
				'condition' => [
					'submit_actions' => $this->get_name(),
				],
			]
		);

		$widget->add_control(
			'sf_action_form_type',
			array(
				'label'   => __( 'סוג טופס', 'sldg-sf-plugin' ),
				'type'    => \Elementor\Controls_Manager::SELECT,
				'default' => '',
				'render_type'  => 'template',
				'required' => true,
				'options' => array(
					'recruit' => __( 'המלצה לגיוס', 'sldg-sf-plugin' ),
					'social' => __( 'מעורבות חברתית', 'sldg-sf-plugin' ),
					'mentoring' => __( 'מנטורינג', 'sldg-sf-plugin' ),
					'communities' => __( 'קהילות', 'sldg-sf-plugin' ),
					'scholars' => __( 'מלגות', 'sldg-sf-plugin' ),
				),
			)
		);
		
		$widget->end_controls_section();
	}

	public function on_export( $element ) {
		unset(
			$element['sf_action_form_type'],
		);
	}
}
