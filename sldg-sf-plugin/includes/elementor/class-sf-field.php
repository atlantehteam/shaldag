<?php

use Elementor\Controls_Manager;
use ElementorPro\Plugin;
use ElementorPro\Modules\Forms\Classes\Form_Base;
use ElementorPro\Modules\Forms\Fields\Field_Base;

class CustomizedForm extends Form_Base {
  public function __construct() {/* Don't call parent constructor */}
  function get_name() {return 'SLDG_PLEASE_ALERT_IF_YOU_SEE_ME';}
  function make_checkbox_field($item, $item_index) {
    return $this->make_radio_checkbox_field($item, $item_index, 'checkbox');
  }
}

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

abstract class SF_Field_Base extends Field_Base {

  public function __construct() {
    parent::__construct();
    $this->sfApi = new Sldg_SF_Api();
  }

  public function validation( $field, $record, $ajax_handler ) {
	  if (empty($field['value'])) {
		  $ajax_handler->add_error( $field['id'], $ajax_handler::get_default_messages()['required_field'] );
	  }
  }
  
	public function update_controls( $widget ) {
		$elementor = Plugin::elementor();

		$control_data = $elementor->controls_manager->get_control_from_stack( $widget->get_unique_name(), 'form_fields' );

		if ( is_wp_error( $control_data ) ) {
			return;
		}

		$field_controls = [
			'field_min' => [
				'name' => 'field_min',
				'label' => __( 'Min. Value', 'elementor-pro' ),
				'type' => Controls_Manager::NUMBER,
				'condition' => [
					'field_type' => $this->get_type(),
				],
				'tab' => 'content',
				'inner_tab' => 'form_fields_content_tab',
				'tabs_wrapper' => 'form_fields_tabs',
			],
			'field_max' => [
				'name' => 'field_max',
				'label' => __( 'Max. Value', 'elementor-pro' ),
				'type' => Controls_Manager::NUMBER,
				'condition' => [
					'field_type' => $this->get_type(),
				],
				'tab' => 'content',
				'inner_tab' => 'form_fields_content_tab',
				'tabs_wrapper' => 'form_fields_tabs',
			],
		];

		$control_data['fields'] = $this->inject_field_controls( $control_data['fields'], $field_controls );
		$widget->update_control( 'form_fields', $control_data );
	}

  abstract protected function getApiOptions();
  abstract protected function isInline();

	public function render( $item, $item_index, $form ) {
    $apiOptions = $this->getApiOptions();
    $options = [];
    foreach ($apiOptions as $value) {
      $options []= $value['label'] .'|'. $value['id'];
    }
    $item['field_options'] = join("\n", $options);
    $item['inline_list'] = $this->isInline() ? 'elementor-subgroup-inline' : '';
    $results = (new CustomizedForm())->make_checkbox_field( $item, $item_index );
    $results = str_replace('class="elementor-field-subgroup', 'id="form-field-'.$item['custom_id'].'" class="elementor-field-subgroup', $results);
    echo $results;
	}
}
