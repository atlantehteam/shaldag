<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

use ElementorPro\Modules\Forms\Module;

class VolunteerTypesField extends SF_Field_Base {

	public function get_type() {
		return 'sf_volunteer_types_field';
	}

	public function get_name() {
		return __( 'סוג התנדבות ׁ(SF)', 'sldg-sf-plugin' );
	}

  protected function getApiOptions() {
    return $this->sfApi->getVolunteerTypes();
  }

  protected function isInline() {
    return false;
  }
}

$field = new VolunteerTypesField();
Module::instance()->add_form_field_type( $field->get_type(),  $field);