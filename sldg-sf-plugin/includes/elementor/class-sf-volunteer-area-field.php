<?php

if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}

use ElementorPro\Modules\Forms\Module;

class VolunteerAreasField extends SF_Field_Base {

  public function get_type() {
    return 'sf_volunteer_areas_field';
  }

  public function get_name() {
    return __( 'מיקום התנדבות ׁ(SF)', 'sldg-sf-plugin' );
  }

  protected function getApiOptions() {
    return $this->sfApi->getVolunteerAreas();
  }

  protected function isInline() {
    return true;
  }
}

$field = new VolunteerAreasField();
Module::instance()->add_form_field_type( $field->get_type(),  $field);