<?php

if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}

use ElementorPro\Modules\Forms\Module;

class MentoringAreasField extends SF_Field_Base {

  public function get_type() {
    return 'sf_mentoring_areas_field';
  }

  public function get_name() {
    return __( 'מיקום התנדבות ׁמנטורינג (SF)', 'sldg-sf-plugin' );
  }

  protected function getApiOptions() {
    return $this->sfApi->getMentoringAreas();
  }

  protected function isInline() {
    return true;
  }
}

$field = new MentoringAreasField();
Module::instance()->add_form_field_type( $field->get_type(),  $field);