<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://github.com/atlanteh
 * @since      1.0.0
 *
 * @package    Sldg_Sf_Plugin
 * @subpackage Sldg_Sf_Plugin/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Sldg_Sf_Plugin
 * @subpackage Sldg_Sf_Plugin/includes
 * @author     Roi Nagar <atlanteh@gmail.com>
 */
class Sldg_Sf_Plugin {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Sldg_Sf_Plugin_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'SLDG_SF_PLUGIN_VERSION' ) ) {
			$this->version = SLDG_SF_PLUGIN_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'sldg-sf-plugin';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();
		$this->define_admin_view_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Sldg_Sf_Plugin_Loader. Orchestrates the hooks of the plugin.
	 * - Sldg_Sf_Plugin_i18n. Defines internationalization functionality.
	 * - Sldg_Sf_Plugin_Admin. Defines all hooks for the admin area.
	 * - Sldg_Sf_Plugin_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-sldg-sf-utils.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-sldg-sf-transform.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-sldg-sf-validate.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-errors.php';
		
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-sldg-sf-forms-api.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-sldg-sf-api.php';
		
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-mail.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/forms/form-utils.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-sldg-sf-payment.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-sldg-sf-login.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/forms/events-forms.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/forms/scholars-forms.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/forms/recruit-form.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/forms/mentoring-form.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-sldg-sf-forms.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-admin-view.php';
		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-sldg-sf-plugin-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-sldg-sf-plugin-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-sldg-sf-plugin-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-sldg-sf-plugin-public.php';

		$this->loader = new Sldg_Sf_Plugin_Loader();

	}

	private function set_locale() {

		$plugin_i18n = new Sldg_Sf_Plugin_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	private function define_admin_hooks() {
		$plugin_admin = new Sldg_Sf_Plugin_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		$this->loader->add_action( 'init', $plugin_admin, 'register_events' );
		$this->loader->add_action( 'wp_ajax_nopriv_import_events', $plugin_admin, 'import_events' );
		$this->loader->add_action( 'elementor_pro/init', $plugin_admin, 'load_elementor_hooks' );
		// $this->loader->add_action( 'wp_ajax_nopriv_sldg_roi_playground', $plugin_admin, 'roi_playground' );
	}

	private function define_admin_view_hooks() {
		$admin_view = new Sldg_SF_Admin_View( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_shortcode( 'shaldag_admin_view', $admin_view, 'shortcodeAdminView' );
		$this->loader->add_action( 'wp_ajax_sldg_search_user', $admin_view, 'process_search_sf_user' );
		$this->loader->add_action('template_redirect', $admin_view, 'admin_actions', 2);
		$this->loader->add_action('login_redirect', $admin_view, 'redirect_on_login', 10, 3);
	}

	private function define_public_hooks() {

		$plugin_public = new Sldg_Sf_Plugin_Public( $this->get_plugin_name(), $this->get_version() );
		$login = new Sldg_SF_Login();
		$forms = new Sldg_SF_Forms();
		$payment = new Sldg_SF_Payment();

		$this->loader->add_filter( 'show_admin_bar', $plugin_public, 'should_show_admin_bar' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
		$this->loader->add_action( 'wp_ajax_nopriv_sf_signup', $login, 'signUpUser' );
		$this->loader->add_action( 'wp_ajax_nopriv_sf_get_signup', $login, 'getSignUpForm' );
		$this->loader->add_filter( 'wp_authenticate_user', $login, 'verifySFUserExists', 100, 2);
		$this->loader->add_shortcode( 'shaldag_signup', $login, 'signupShortcode' );
		$this->loader->add_shortcode( 'shaldag_contact_form', $forms, 'contactForm' );
		$this->loader->add_action( 'wp_ajax_contact_form', $forms, 'process_contact_form' );
		$this->loader->add_shortcode( 'sldg_logout_link', $plugin_public, 'logout_shortcode' );

		$this->loader->add_shortcode( 'shaldag_event_form', $forms, 'shortcodeEventForm' );
		$this->loader->add_action( 'wp_ajax_sldg_register_event', $forms, 'process_register_event' );
		$this->loader->add_action( 'wp_ajax_nopriv_sldg_register_event', $forms, 'process_register_event' );
		
		$this->loader->add_shortcode( 'shaldag_scholar_form', $forms, 'shortcodeScholarForm' );
		$this->loader->add_shortcode( 'shaldag_recruit_form', $forms, 'shortcodeRecruitForm' );
		$this->loader->add_shortcode( 'shaldag_mentoring_form', $forms, 'shortcodeMentoringForm' );

		$this->loader->add_shortcode( 'shaldag_member_payment', $payment, 'memberPaymentShortcode' );
		$this->loader->add_shortcode( 'shaldag_donation_payment', $payment, 'donationPaymentShortcode' );
		$this->loader->add_action( 'wp_ajax_make_payment', $payment, 'makePayment' );
		$this->loader->add_action( 'wp_ajax_make_donation', $payment, 'makeDonation' );
	}

	public function run() {
		$this->loader->run();
	}

	public function get_plugin_name() {
		return $this->plugin_name;
	}

	public function get_loader() {
		return $this->loader;
	}

	public function get_version() {
		return $this->version;
	}

}
