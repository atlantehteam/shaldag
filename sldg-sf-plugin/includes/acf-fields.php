<?php
if( function_exists('acf_add_local_field_group') ):
    $jsonStr = file_get_contents(SLDG_SF_PLUGIN_DIR.'/acf/acf-fields.json');
    $fields = json_decode($jsonStr, true);
    foreach ($fields as $group) {
        acf_add_local_field_group($group);
    }
endif;
