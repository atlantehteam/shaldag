<?php

class Sldg_Mail
{
	public function send_event_mail($post_id, $registration)
	{
		$event_title = get_the_title( $post_id );
		$event_date = get_post_meta( $post_id, 'date', true );
		$bcc = jet_engine()->listings->data->get_option( 'theme-settings::sf_event_email_to' );
		$subject_raw = jet_engine()->listings->data->get_option( 'theme-settings::sf_event_email_subject' );
		$body_raw = jet_engine()->listings->data->get_option( 'theme-settings::sf_event_email_body' );
		
		if ($event_date) {
			$event_date = date("j-m-y", $event_date);
		}
		if (!$bcc || !$subject_raw || !$body_raw) {
			trigger_error("Will not send event mail. Missing data");
			return;
		}
		$user = wp_get_current_user();
		$first_name = $user->display_name;
		$email = $user->user_email;
		$templates = [
			'{name}' => $first_name,
			'{email}' => $email,
			'{eventTitle}' => $event_title,
			'{eventDate}' => $event_date,
			'{numParticipants}' => $registration['numParticipants'],
		];

		$subject = str_replace(array_keys($templates), array_values($templates), $subject_raw);
		$body = str_replace(array_keys($templates), array_values($templates), $body_raw);

		$body = str_replace("\n", '<br/>', $body);

		$headers = array('Content-Type: text/html; charset=UTF-8');

		if (!empty($bcc)) {
			$headers[] = "Bcc:$bcc";
		}

		return wp_mail([], $subject, $body, $headers);
	}

	public function send_form_mail($form_name)
	{
		$bcc = jet_engine()->listings->data->get_option( "theme-settings::sf_{$form_name}_email_bcc" );
		$subject_raw = jet_engine()->listings->data->get_option( "theme-settings::sf_{$form_name}_email_subject" );
		$body_raw = jet_engine()->listings->data->get_option( "theme-settings::sf_{$form_name}_email_body" );
		
		if (!$subject_raw || !$body_raw) {
			trigger_error("Will not send event mail. Missing data");
			return;
		}
		$user = wp_get_current_user();
		$first_name = $user->display_name;
		$email = $user->user_email;
		$templates = [
			'{name}' => $first_name,
			'{email}' => $email,
		];

		$subject = str_replace(array_keys($templates), array_values($templates), $subject_raw);
		$body = str_replace(array_keys($templates), array_values($templates), $body_raw);

		$body = str_replace("\n", '<br/>', $body);

		$headers = array('Content-Type: text/html; charset=UTF-8');

		if (!empty($bcc)) {
			$headers[] = "Cc:$bcc";
		}

		return wp_mail([$email], $subject, $body, $headers);
	}
}
