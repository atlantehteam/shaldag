<?php

class Sldf_RecruitForm {
    public function __construct() {
      $this->sfApi = new Sldg_SF_Api();
    }
    
    public function form_shortcode() {
      if (!$this->sfApi->hasActiveFormParent('recruit')) {
        echo '<div class="sf-center">'.__('ההרשמה סגורה כעת', 'sldg-sf-plugin').'</div>';
        return;
      }
      $shortcode = jet_engine()->listings->data->get_option( 'theme-settings::recruit_form_shortcode' );
      echo do_shortcode($shortcode);
    }
}