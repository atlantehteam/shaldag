<?php

function sldg_get_creditcard_year_exp_options() {
  $currentYear = date("Y");
  $years = range($currentYear, $currentYear + 8);
  $opts = array_map(function($year) {return array('value' => str_split($year, 2)[1],'label' => $year);}, $years);
  array_unshift($opts, ['label' => 'שנה', 'value' => '']);
  return sldg_get_select_opts($opts);
}
function sldg_get_creditcard_month_exp_options() {
  $months = array_map(function($item){return str_pad($item, 2, '0', STR_PAD_LEFT);}, range(1, 12));
  array_unshift($months, ['label' => 'חודש', 'value' => '']);
  return sldg_get_select_opts($months);
}
function sldg_get_select_opts($options) {
  return array_reduce($options, function($aggr, $item) {
    $value = $item['value'] ?? $item;
    $label = $item['label'] ?? $item;
    return $aggr."<option value='$value'>$label</option>";
  }, '');
}

function sldg_print_creditcard_fields() {
  
  ?>
  <label class="sf-form-section">
      <span class="sf-label"><?= __('מספר כרטיס', 'sldg-sf-plugin') ?></span>
      <input name="creditCard" type="text" placeholder="מספר כרטיס אשראי" autocomplete="cc-number" required data-credit maxlength="19" />
  </label>
  <div class="sf-row sf-row-collapse">
    <label class="sf-form-section">
        <span class="sf-label"><?= __('תוקף', 'sldg-sf-plugin') ?></span>
        <div class="sf-row" data-err="creditCardDateMmYy">
          <select required name="expMonth" autocomplete="cc-exp-month" required class="sf-fit-content"><?= sldg_get_creditcard_month_exp_options() ?></select>
          <select required name="expYear" autocomplete="cc-exp-year" required class="sf-fit-content"><?= sldg_get_creditcard_year_exp_options() ?></select>
        </div>
    </label>
    <label class="sf-form-section">
        <span class="sf-label"><?= __('קוד אימות (cvv)', 'sldg-sf-plugin') ?></span>
        <input required name="cvv" type="text" placeholder="cvv" autocomplete="cc-csc" maxlength="4" />
    </label>
  </div>
  <?php
}
