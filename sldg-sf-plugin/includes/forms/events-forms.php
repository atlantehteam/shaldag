<?php

class Sldf_EventForm {
    public function __construct() {
		  $this->formsApi = new Sldg_SF_Forms_Api();
      $this->login = new Sldg_SF_Login();
      $this->sfApi = new Sldg_SF_Api();
      $this->sfMail = new Sldg_Mail();
    }
    
    public function form_shortcode($postId) {

      $isViewOnly = get_post_meta($postId, 'view_only', true);
      if ($isViewOnly) {
        echo "לא ניתן להרשם לאירוע זה";
        return;
      }
      $sfId = get_post_meta($postId, 'sfEventId', true);
      $cost = get_post_meta($postId, 'eventCost', true);
      if (!$sfId) {
        return;
      }

      $class = 'sf-event-form ' . ($cost ? 'sf-has-cost' : 'sf-no-cost');
      ?>
        <form class="sf-form sf-form-default <?= $class ?>" data-sf-price="<?= $cost ?>" data-action="sldg_register_event">
          <div class="sf-loader"><div class="sf-loader-anim"></div></div>
            <div class="sf-form-errors sf-hide"></div>
            <div class="sf-form-fields">
              <?php
                $this->processInputField('numParticipants', __('מספר משתתפים', 'sldg-sf-plugin'), 'number', [
                  'min' => 1, 'required' => true, 'data-cost-multiplier' => true, 'step' => 1, 'max' => 20,
                  ]);
                if ($cost) {
                  sldg_print_creditcard_fields();
                }
                $amountVal = floatval($cost);
              ?>
              <input type="hidden" name="eventId" value="<?=$postId?>" />
              <input type="hidden" name="amount" value="<?=$amountVal?>" />
              <?php
                if ($cost) {
                  ?>
                  <div class="cost-wrapper">
                    <div class="cost-label"><?=__('סכום לתשלום:', 'sldg-sf-plugin')?> <span class="cost"><?= $amountVal ?></span> ש"ח</div>
                  </div>
                  <?php
                }
              ?>
              <button class="sf-primary" type="submit"><?=__('הרשמה', 'sldg-sf-plugin')?></button>
            </div>
            <div class="sf-submit-errors sf-hide"></div>
            <div class="sf-response sf-hide"></div>
            <?php wp_nonce_field( 'sf-event-nonce', 'security' ); ?>
        </form>
      <?php
    }
    
    public function process($fields) {
      if (!check_ajax_referer( 'sf-event-nonce', 'security', false)) {
        trigger_error('!!!!!eventPayment - check_ajax_referer failed!!!!');
        wp_send_json_error( array('code' => 'invalidNonce', 'message' => 'כרטיס האשראי שלך לא חויב. אנא רענן את העמוד ונסה שנית'), 400);
      }

      $postId = $fields['eventId'] ?? '';
      $userId = $this->login->getSFUserId();
      Sldg_SF_Validate::validateSFUser($userId);
      
      $event_post = get_post($postId);
      if (!$event_post) {
        wp_send_json(array('success' => false, 'errors' => [array('key' => 'missingEvent', 'msg' => __('אירוע לא קיים', 'sldg-sf-plugin'))]), 400);
      }
      
      $sfEventId = get_post_meta($postId, 'sfEventId', true);

      $expMonth = $fields['expMonth'] ?? '';
      $expYear = $fields['expYear'] ?? '';
      $fields = array(
        'creditCard' => Sldg_SF_Transform::digitsOnly($fields['creditCard'] ?? ''),
        'cvv' => $fields['cvv'] ?? '',
        'creditCardDateMmYy' => $expMonth.$expYear,
        'contactId' => $userId,
        'eventId' => $sfEventId,
        'amount' => $fields['amount'] ?? '',
        'numParticipants' => $fields['numParticipants'] ?? '',
      );
      Sldg_SF_Validate::validateRegisterEventFields($fields);
      $registerResult = $this->sfApi->registerEvent($fields);

      $this->sfMail->send_event_mail($postId, $fields);

      sldg_send_payment_reponse($registerResult, __( 'ההרשמה לאירוע בוצעה בהצלחה', 'sldg-sf-plugin' ));
    }
     function processInputField($name, $label, $type, $opts) {
      $numFields = '';
      switch($type) {
        case 'number':
          $numFields .= 'min="'.($opts['min'] ?? '0').'"';
          $numFields .= !empty($opts['step']) ? 'step="'.$opts['step'].'"' : '';
          $numFields .= empty($opts['max']) ? '' : 'max="'.$opts['max'].'"';
          break;
        case 'tel':
          $type = 'tel';
          break;
        case 'email':
          $type = 'email';
          break;
        default:
          $type = 'text';
      }

      $class = $type == 'date' ? 'datepicker' : '';
      $length = $opts['length'] ?? '';
      $required = !empty($opts['required']) ? 'required="required"' : ''; 
      $value = !empty($opts['value']) ? 'value="'.$opts['value'].'"' : '';
      $data = '';
      foreach ($opts as $key => $value) {
        if (strpos($key, 'data-') === 0) {
          $data .= " $key='$value' ";
        }
      }

      ?>
      <label class="sf-field <?= $class ?>">
        <span class="sf-label"><?= $label ?></span>
        <input class="sf-field-el sf-field-input" placeholder="<?=$label?>" name="<?=$name?>" type="<?=$type?>" maxlength="<?=$length?>" <?=$value.' '.$numFields.' '.$required.$data ?> />
      </label>
      <?php
    }
}