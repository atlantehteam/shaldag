<?php

class Sldg_SF_Payment {
    public function __construct() {
      $this->sfApi = new Sldg_SF_Api();
      $this->login = new Sldg_SF_Login();
    }

    public function printStartForm($cls, $action) {
      ?>
        <div class="sf-pay-wrapper">
          <form class="sf-form sf-form-default sf-reset <?= $cls ?>" method="post" data-action="<?= $action ?>">
            <div class="sf-loader"><div class="sf-loader-anim"></div></div>
            <div class="sf-form-errors sf-hide"></div>
      <?php
    }

    public function printEndForm($userId) {
      ?>
          <section class="sf-form-section">
            <div><?= __('סה"כ לתשלום:', 'sldg-sf-plugin') ?> <span class="sf-total-pay"></span></div>
          </section>
          <input type="hidden" name="sfId" value="<?=$userId?>" />
          <button class="sf-primary" type="submit"><?= __('בצע תשלום', 'sldg-sf-plugin') ?></button>
          <div class="sf-response sf-hide"></div>
          <div class="sf-submit-errors sf-hide"></div>
          <?php wp_nonce_field( 'sf-payment-nonce', 'security' ); ?>
        </form>
      <?php
    }

    public function printPaymentSection($settings) {
      ?>
        <section class="sf-form-section">
          <span class="sf-label"><?= __('תשלום', 'sldg-sf-plugin') ?></span>
          <select name="amount"><?= $this->_generateOptions($settings['memberPaymentOptions']) ?></select>
        </section>
      <?php
    }

    public function printDonationSection($settings, $positiveOnly) {
      ?>
        <section class="sf-form-section">
          <span class="sf-label"><?= __('תרומה', 'sldg-sf-plugin') ?></span>
          <div class="sf-row">
            <select name="donationAmount" class="sf-fit-content"><?= $this->_getDonationOptions($settings, $positiveOnly) ?></select>
            <input name="donationAmountOpen" class="sf-fit-content sf-hide" type="number" min="1" step="0.01" />
          </div>
        </section>
      <?php
    }
  
    public function printCreditCardSection() {
      $months = array_map(function($item){return str_pad($item, 2, '0', STR_PAD_LEFT);}, range(1, 12));
      ?>
      <section class="sf-form-section">
        <span class="sf-label"><?= __('מספר כרטיס', 'sldg-sf-plugin') ?></span>
        <input name="creditCard" type="text" placeholder="0000-0000-0000-0000" autocomplete="cc-number" required data-credit maxlength="19" />
      </section>
      <section class="sf-form-section">
        <span class="sf-label"><?= __('תוקף', 'sldg-sf-plugin') ?></span>
        <div class="sf-row" data-err="creditCardDateMmYy">
          <select name="expMonth" autocomplete="cc-exp-month" required class="sf-fit-content"><?= $this->_generateOptions($months) ?></select>
          <select name="expYear" autocomplete="cc-exp-year" required class="sf-fit-content"><?= $this->_generateExpYearOptions() ?></select>
        </div>
      </section>
      <section class="sf-form-section">
        <span class="sf-label"><?= __('קוד אימות (cvv)', 'sldg-sf-plugin') ?></span>
        <input name="cvv" placeholder="cvv" type="text" autocomplete="cc-csc" maxlength="4" />
      </section>
      <?php
    }
  
    public function memberPaymentShortcode($id) {
        $settings = $this->sfApi->getSettings();
        ob_start();
        $userId = $this->login->getSFUserId(true);
        if (!$userId) {
          echo '<div>'
            .__('רק חברי עמותה רשומים יכולים לשלם', 'sldg-sf-plugin') 
          .'</div>';
        } else {
        ?>
            <?php
              $this->printStartForm('sf-payment', 'make_payment');
              $this->printCreditCardSection();
              $this->printPaymentSection($settings);
              $this->printDonationSection($settings, false);
              $this->printEndForm($userId);
            ?>
          </div>
        <?php
        }
        $output = ob_get_contents();   
        ob_end_clean();   
        return $output;
    }
  
    public function donationPaymentShortcode($id) {
        $settings = $this->sfApi->getSettings();
        ob_start();
        $userId = $this->login->getSFUserId(true);
        if (!$userId) {
          echo '<div>'
            .__('רק חברי עמותה רשומים יכולים לתרום', 'sldg-sf-plugin') 
          .'</div>';
        } else {
        ?>
            <?php
              $this->printStartForm('sf-donation', 'make_donation');
              $this->printCreditCardSection();
              $this->printDonationSection($settings, true);
              $this->printEndForm($userId);
            ?>
          </div>
        <?php
        }
        $output = ob_get_contents();   
        ob_end_clean();   
        return $output;
    }

    public function makePayment() {
      if (!check_ajax_referer( 'sf-payment-nonce', 'security', false)) {
        trigger_error('!!!!!makePayment - check_ajax_referer failed!!!!');
        wp_send_json_error( array('code' => 'invalidNonce', 'message' => 'כרטיס האשראי שלך לא חויב. אנא רענן את העמוד ונסה שנית'), 400);
      }
      $userId = $this->login->getSFUserId(true);
      Sldg_SF_Validate::validateSFUser($userId);
      
      $expMonth = Sldg_SF_Transform::digitsOnly($_POST['expMonth'] ?? '');
      $expYear = Sldg_SF_Transform::digitsOnly($_POST['expYear'] ?? '');
      $fields = array(
        'creditCard' => Sldg_SF_Transform::digitsOnly($_POST['creditCard'] ?? ''),
        'cvv' => Sldg_SF_Transform::digitsOnly($_POST['cvv'] ?? ''),
        'creditCardDateMmYy' => $expMonth.$expYear,
        'donationAmount' => $_POST['donationAmount'] ?? '',
        'donationAmountOpen' => trim($_POST['donationAmountOpen'] ?? ''),
        'amount' => trim($_POST['amount'] ?? ''),
        'contactId' => $userId,
      );
      Sldg_SF_Validate::validateMembershipFields($fields);
      $paymentResult = $this->sfApi->makeMembershipPayment($fields);
      sldg_send_payment_reponse($paymentResult, __( 'התשלום בוצע בהצלחה', 'sldg-sf-plugin' ));
    }

    public function makeDonation() {
      if (!check_ajax_referer( 'sf-payment-nonce', 'security', false)) {
        trigger_error('!!!!!makeDonation - check_ajax_referer failed!!!!');
        wp_send_json_error( array('code' => 'invalidNonce', 'message' => 'כרטיס האשראי שלך לא חויב. אנא רענן את העמוד ונסה שנית'), 400);
      }
      $userId = $this->login->getSFUserId(true);
      Sldg_SF_Validate::validateSFUser($userId);
      
      $expMonth = Sldg_SF_Transform::digitsOnly($_POST['expMonth'] ?? '');
      $expYear = Sldg_SF_Transform::digitsOnly($_POST['expYear'] ?? '');
      $fields = array(
        'creditCard' => Sldg_SF_Transform::digitsOnly($_POST['creditCard'] ?? ''),
        'cvv' => Sldg_SF_Transform::digitsOnly($_POST['cvv'] ?? ''),
        'creditCardDateMmYy' => $expMonth.$expYear,
        'donationAmount' => $_POST['donationAmount'] ?? '',
        'donationAmountOpen' => trim($_POST['donationAmountOpen'] ?? ''),
        'contactId' => $userId,
      );
      Sldg_SF_Validate::validateDonationFields($fields);
      $paymentResult = $this->sfApi->makeDonationPayment($fields);
      sldg_send_payment_reponse($paymentResult, __( 'התשלום בוצע בהצלחה', 'sldg-sf-plugin' ));
    }

    private function _generateExpYearOptions() {
      $currentYear = date("Y");
      $years = range($currentYear, $currentYear + 8);
      $opts = array_map(function($year) {return array('value' => str_split($year, 2)[1],'label' => $year);}, $years);
      return $this->_generateOptions($opts);
    }
    private function _generateOptions($options) {
      return array_reduce($options, function($aggr, $item) {
        $value = $item['value'] ?? $item;
        $label = $item['label'] ?? $item;
        return $aggr."<option value='$value'>$label</option>";
      }, '');
    }

    private function _getDonationOptions($settings, $positiveOnly) {
      $options = $settings['donationPaymentOptions'];
      if ($positiveOnly) {
        $options = array_values(array_filter($options, function($opt) {
          return !is_float($opt['value']) || floatval($opt['value'] > 0);
        }));
      }
      $options []= array('value' => 'other', 'label' => __('אחר', 'sldg-sf-plugin'));
      $options = $this->_generateOptions($options);
      return $options;
    }
}

function sldg_send_payment_reponse($paymentResult, $successMessage = '') {
  if (!empty($paymentResult['errors'])) {
    $paymentErrors = array_filter($paymentResult['errors'], function($err) { return $err['key'] == 'paymentFailure'; });
    $paymentResult['errors'] = array_filter($paymentResult['errors'], function($err) { return $err['key'] != 'paymentFailure'; });
    $paymentResult['submitErrors'] = $paymentErrors;
  }
  
  $status = $paymentResult['success'] ? 200 : 400;
  if ($paymentResult['success']) {
    $paymentResult['data'] = array('message' => $successMessage);
 }
  wp_send_json($paymentResult, $status);
}