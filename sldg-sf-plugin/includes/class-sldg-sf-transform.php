<?php

class Sldg_SF_Transform {
  public static function digitsOnly($str) {
    $stripped = preg_replace("/\D/", "", $str);
    return $stripped;
  }
}