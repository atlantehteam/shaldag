<?php

class Sldg_SF_Login {
    public function __construct() {
      $this->sfApi = new Sldg_SF_Api();
    }

    public function verifySFUserExists($user, $password) {
      if (is_wp_error( $user)) {return $user;}
      if (Sldg_Utils::is_sf_user($user)) {
        $id = $this->getSFUserIdById($user->ID);
        $sfUser = $this->sfApi->getUserById($id);
        if (!$sfUser) {
          return new WP_Error(
              'invalid_username',
                    __( 'משתמש לא קיים במערכת העמותה', 'sldg-sf-plugin')
          );
        }
      }
      return $user;
    }

    public function getSignUpForm() {
      ob_start();
      if (is_user_logged_in()) {
          echo '<div class="sf-login-msg">'
            .__('אתה כבר מחובר', 'sldg-sf-plugin')
            .' (<a class="sf-login-logout" href="'.wp_logout_url().'">'.__('התנתק', 'sldg-sf-plugin').'</a>)'
          .'</div>';
      } else {
        ?>
          <form class="sf-form sf-signup">
            <div class="sf-loader"><div class="sf-loader-anim"></div></div>
            <div class="sf-row sf-row-collapse">
              <label class="sf-field">
                <input class="sf-field-el sf-field-input" name="email" type="email" placeholder="האימייל שלך" required />
              </label>
              <label class="sf-field">
                <input class="sf-field-el sf-field-input" name="id" type="number" placeholder="תעודת הזהות שלך" required />
              </label>
            </div>
            <button class="sf-submit sf-primary" type="submit">הרשמה</button>
            <?php wp_nonce_field( 'sf-signup-nonce', 'security' ); ?>
            <div class="sf-response sf-hide"></div>
          </form>
        <?php
      }
      $signupForm = ob_get_contents();   
      ob_end_clean(); 
      wp_send_json(array('success' => true, 'data' => $signupForm));
    }
  
    public function signupShortcode() {
        ob_start();
        if (is_user_logged_in()) {
          echo '<div class="sf-login-msg">'
            .__('אתה כבר מחובר', 'sldg-sf-plugin')
            .' (<a class="sf-login-logout" href="'.wp_logout_url().'">'.__('התנתק', 'sldg-sf-plugin').'</a>)'
          .'</div>';
        } else {
        ?>
          <div class="sf-login-wrapper">
            <div class="sf-loader"><div class="sf-loader-anim"></div></div>
          </div>
        <?php
        }
        $output = ob_get_contents();   
        ob_end_clean();   
        return $output;
    }

    public function signUpUser() {
      if (!check_ajax_referer( 'sf-signup-nonce', 'security', false)) {
        trigger_error('!!!!!signUpUser - check_ajax_referer failed!!!!');
        wp_send_json_error( array('code' => 'invalidNonce', 'message' => 'שגיאה: נראה שהתרחשה תקלה. נסה לרענן את העמוד ולנסות שוב.'), 400);
      }
      $id = $_POST['id'];
      $email = $_POST['email'];

      $user = $this->sfApi->getUserByNationalIdAndEmail($id, $email);
      if (!$user) {
        $supporEmail = jet_engine()->listings->data->get_option( 'theme-settings::main_support_email' );
        wp_send_json_error( array('code' => 'invalidUser', 'message' => 'שגיאה: נראה שאינך רשום במערכת. אנא פנה למייל: '.$supporEmail), 400);
      }
      if (email_exists($email)) {
        wp_send_json_error( array('code' => 'userExists', 'message' => 'שגיאה: נראה שנרשמת בעבר. יש לעבור להתחברות'), 400);
      }
      $sfId = $user['id'];
      $newUser = wp_insert_user(array(
        'role' => 'sf_user',
        'user_login' => $email,
        'user_email' => $email,
        'user_pass' => NULL,
        'first_name' => $user['firstName'],
      ));
      if (is_wp_error($newUser)) {
        trigger_error($newUser);
        wp_send_json_error( array('code' => 'serverError'), 500);
      }
      add_user_meta( $newUser, 'sf_id', $sfId);
      wp_new_user_notification($newUser, NULL, 'user');
      wp_send_json(array('success' => true, 'data' => ['message' => 'סיסמה ראשונית נשלחה אלייך למייל']));
    }

    public function getSFUserId($queryOverride = false) {
      if ($queryOverride && Sldg_Utils::can_see_admin_view() && (!empty($_GET['sfId']) || !empty($_POST['sfId']))) {
        return $_GET['sfId'] ?? $_POST['sfId'];
      }
      return $this->getSFUserIdById(get_current_user_id());
    }

    public function getSFUserIdById($wpUserId) {
      $id = get_user_meta( $wpUserId, 'sf_id', true);
      return $id;
    }
}
