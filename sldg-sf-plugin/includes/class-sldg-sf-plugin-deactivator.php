<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://github.com/atlanteh
 * @since      1.0.0
 *
 * @package    Sldg_Sf_Plugin
 * @subpackage Sldg_Sf_Plugin/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Sldg_Sf_Plugin
 * @subpackage Sldg_Sf_Plugin/includes
 * @author     Roi Nagar <atlanteh@gmail.com>
 */
class Sldg_Sf_Plugin_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
		remove_role('sf_user');
		remove_role('sf_agent');
	}

}
