<?php

class Sldg_SF_Validate {
  public static function validateMembershipFields($fields) {
    $errors = array();

    $errors []= Sldg_SF_Validate::notEmpty($fields, 'creditCard', __('מספר כרטיס לא חוקי', 'sldg-sf-plugin'));
    $errors []= Sldg_SF_Validate::notEmpty($fields, 'cvv', __('קוד אימות לא חוקי', 'sldg-sf-plugin'));
    $errors []= Sldg_SF_Validate::mmyyNotExpired($fields, 'creditCardDateMmYy', __('תוקף הכרטיס אינו יכול להיות בעבר', 'sldg-sf-plugin'));
    $errors []= Sldg_SF_Validate::notEmpty($fields, 'amount', __('תשלום אינו חוקי', 'sldg-sf-plugin'));
    $errors []= Sldg_SF_Validate::notEmptyOpenAmount($fields);
    Sldg_SF_Validate::validateErrors($errors);
  }
  public static function validateDonationFields($fields) {
    $errors = array();

    $errors []= Sldg_SF_Validate::notEmpty($fields, 'creditCard', __('מספר כרטיס לא חוקי', 'sldg-sf-plugin'));
    $errors []= Sldg_SF_Validate::notEmpty($fields, 'cvv', __('קוד אימות לא חוקי', 'sldg-sf-plugin'));
    $errors []= Sldg_SF_Validate::mmyyNotExpired($fields, 'creditCardDateMmYy', __('תוקף הכרטיס אינו יכול להיות בעבר', 'sldg-sf-plugin'));
    $errors []= Sldg_SF_Validate::positiveDonationAmount($fields);
    $errors []= Sldg_SF_Validate::notEmptyOpenAmount($fields);
    Sldg_SF_Validate::validateErrors($errors);
  }
  public static function validateRegisterEventFields($fields) {
    $errors = array();

    $amount = $fields['amount'] ?? '';
    if (floatval($amount) > 0) {
      $errors []= Sldg_SF_Validate::notEmpty($fields, 'creditCard', __('מספר כרטיס לא חוקי', 'sldg-sf-plugin'));
      $errors []= Sldg_SF_Validate::notEmpty($fields, 'cvv', __('קוד אימות לא חוקי', 'sldg-sf-plugin'));
      $errors []= Sldg_SF_Validate::mmyyNotExpired($fields, 'creditCardDateMmYy', __('תוקף הכרטיס אינו יכול להיות בעבר', 'sldg-sf-plugin'));
    }
    $errors []= Sldg_SF_Validate::positiveIntValue($fields, 'numParticipants', __('מספר משתתפים אינו חוקי', 'sldg-sf-plugin'));
    Sldg_SF_Validate::validateErrors($errors);
  }
  
  public static function validateSFUser($userId) {
    if (!$userId) {
      wp_send_json(array('success' => false, 'errors' => [array('key' => 'validationFailure', 'msg' => __('אינך משתמש עמותה', 'sldg-sf-plugin'))]), 400);
    }
  }

  private static function validateErrors($errors) {
    $realErrors = array_values(array_filter($errors));
    if (sizeof($realErrors)) {
      wp_send_json(array('success' => false, 'errors' => $realErrors), 400);
    }
  }
  public static function notEmpty($fields, $key, $err) {
    if (empty($fields[$key] ?? '')) {
      return array('key' => $key, 'msg' => $err);
    }
  }
  public static function mmyyNotExpired($fields, $key, $err) {
    $mmyy = str_split($fields[$key], 2);
    $yymm = $mmyy[1].$mmyy[0];
    $currentYYMM = date('ym');

    if ($yymm < $currentYYMM) {
      return array('key' => $key, 'msg' => $err);
    }
  }
  public static function positiveDonationAmount($fields) {
    $donationAmount = $fields['donationAmount'] ?? '';
    $donationAmountOpen = $fields['donationAmountOpen'] ?? '';
    if ($donationAmount != 'other' && (!Sldg_Utils::is_simple_float($donationAmount) || floatval($donationAmount) <= 0)) {
      return array('key' => 'donationAmount', 'msg' => __('סכום תרומה אינו חוקי', 'sldg-sf-plugin'));
    }
  }
  public static function positiveIntValue($fields, $key, $err) {
    $value = $fields[$key] ?? '';
    if ((!ctype_digit($value) && !is_int($value)) || intval($value) <= 0) {
      return array('key' => $key, 'msg' => $err);
    }
  }
  public static function nonNegativeIntValue($fields, $key, $err) {
    $value = $fields[$key] ?? '';
    if ((!ctype_digit($value) && !is_int($value)) || intval($value) < 0) {
      return array('key' => $key, 'msg' => $err);
    }
  }

  public static function notEmptyOpenAmount($fields) {
    $donationAmount = $fields['donationAmount'] ?? '';
    $donationAmountOpen = $fields['donationAmountOpen'] ?? '';
    if ($donationAmount == 'other' && (!Sldg_Utils::is_simple_float($donationAmountOpen) || floatval($donationAmountOpen) <= 0)) {
      return array('key' => 'donationAmountOpen', 'msg' => __('תרומה אינה חוקית', 'sldg-sf-plugin'));
    }
  }
}