<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://github.com/atlanteh
 * @since             1.0.0
 * @package           Sldg_Sf_Plugin
 *
 * @wordpress-plugin
 * Plugin Name:       Shaldag SF Integration
 * Plugin URI:        //
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Roi Nagar
 * Author URI:        https://github.com/atlanteh
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       sldg-sf-plugin
 * Domain Path:       /languages
 */
// error_reporting( E_ALL );
// ini_set( 'display_errors', 1 );
	  
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'SLDG_SF_PLUGIN_VERSION', '1.0.3' );
define( 'SLDG_SF_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'SLDG_SF_PLUGIN_URL', plugin_dir_url( __FILE__ ) );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-sldg-sf-plugin-activator.php
 */
function activate_sldg_sf_plugin() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-sldg-sf-plugin-activator.php';
	Sldg_Sf_Plugin_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-sldg-sf-plugin-deactivator.php
 */
function deactivate_sldg_sf_plugin() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-sldg-sf-plugin-deactivator.php';
	Sldg_Sf_Plugin_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_sldg_sf_plugin' );
register_deactivation_hook( __FILE__, 'deactivate_sldg_sf_plugin' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-sldg-sf-plugin.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_sldg_sf_plugin() {

	$plugin = new Sldg_Sf_Plugin();
	$plugin->run();

}
run_sldg_sf_plugin();
