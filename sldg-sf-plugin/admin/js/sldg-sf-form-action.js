(function($) {
    $(function() {

        function addControlSpinner(controlView) {
            const $el = controlView.$el;
            const $input = $el.find(':input');
        
            if ($input.attr('disabled')) {
              return;
            }
        
            $input.attr('disabled', true);
            $el.find('.elementor-control-title').after('<span class="elementor-control-spinner"><i class="eicon-spinner eicon-animation-spin"></i>&nbsp;</span>');
        }

        function removeControlSpinner(controlView) {
            const $controlEl = controlView.$el;
            $controlEl.find(':input').attr('disabled', false);
            $controlEl.find('.elementor-control-spinner').remove();
        }

        function makeAjaxRequest(data) {
            return new Promise((resolve, reject) => {
                $.post(elementorCommon.ajax.getSettings().url, data)
                .done(resolve)
                .fail(err => {
                    console.error(err);
                    reject(err);
                })
            })
        }
        const cache = {};
        function updateApiData(form) {
            const editor = elementor.getPanelView().getCurrentPageView();
            const controlView = editor.getControlViewByName('sf_action_parent');
            const container = controlView.getOption('container');
            const isShowing = elementor.helpers.isActiveControl(controlView.model, container.settings.attributes);
            if (!isShowing) {return;}
            addControlSpinner(controlView);
            let promise
            if (cache[form]) {
                promise = Promise.resolve(cache[form])
            } else {
                promise = makeAjaxRequest({action: 'get_salesforce_parent', form})
            }
            return promise
            .then(data => {
                cache[form] = data;
                controlView.model.set('options', data)
                controlView.render()
                removeControlSpinner(controlView);
            })
        }

        elementor.channels.editor.on('change:form', function(controlView, elementView) {
            if(controlView.model.get('name') !== 'sf_action_form_type') {return;}
            const value = controlView.getControlValue();
            updateApiData(value)
        })
        elementor.channels.editor.on('section:activated', function(sectionName, editor) {
            let model = editor.getOption('editedElementView').getEditModel()
            let type = currentElementType = model.get('elType');
            if (type === 'widget' && model.get('widgetType') === 'form' && sectionName === 'section_sf_action') {
                const controlView = editor.getControlViewByName('sf_action_form_type')
                const value = controlView.getControlValue();
                updateApiData(value)
            }
        })
    })
})(jQuery);