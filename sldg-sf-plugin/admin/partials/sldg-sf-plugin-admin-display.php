<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://github.com/atlanteh
 * @since      1.0.0
 *
 * @package    Sldg_Sf_Plugin
 * @subpackage Sldg_Sf_Plugin/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
