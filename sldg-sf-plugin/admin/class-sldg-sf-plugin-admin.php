<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://github.com/atlanteh
 * @since      1.0.0
 *
 * @package    Sldg_Sf_Plugin
 * @subpackage Sldg_Sf_Plugin/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Sldg_Sf_Plugin
 * @subpackage Sldg_Sf_Plugin/admin
 * @author     Roi Nagar <atlanteh@gmail.com>
 */
class Sldg_Sf_Plugin_Admin {

  private $plugin_name;
  private $version;


  public function __construct( $plugin_name, $version ) {

    $this->plugin_name = $plugin_name;
    $this->version = $version;

  }

  public function enqueue_styles() {
    wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/sldg-sf-plugin-admin.css', array(), $this->version, 'all' );
  }


  public function enqueue_scripts() {
    wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/sldg-sf-plugin-admin.js', array( 'jquery' ), $this->version, false );
    wp_localize_script( $this->plugin_name, 'Sldg', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
  }

  public function register_events() {
    $labels = array(
      'name'                  => _x( 'אירועים', 'Post Type General Name', 'sldg-sf-plugin' ),
      'singular_name'         => _x( 'אירוע', 'Post Type Singular Name', 'sldg-sf-plugin' ),
    );
    $args = array(
      'label'                 => __( 'אירוע', 'sldg-sf-plugin' ),
      'labels'                => $labels,
      'supports'              => array( 'title', 'editor', 'thumbnail'),
      'taxonomies'            => array('category'),
      'hierarchical'          => false,
      'public'                => true,
      'show_ui'               => true,
      'show_in_menu'          => true,
      'menu_position'         => 5,
      'menu_icon'             => 'dashicons-calendar-alt',
      'show_in_admin_bar'     => true,
      'show_in_nav_menus'     => true,
      'can_export'            => true,
      'has_archive'           => true,
      'exclude_from_search'   => false,
      'publicly_queryable'    => true,
      'capability_type'       => 'page',
    );
    register_post_type( 'event', $args );
  }

  function uploadRemoteImage($sfEvent) {
    if (!isset($sfEvent['imageUrl'])) {
      trigger_error('!!!No Image');
      return;
    }
    $image_url = $sfEvent['imageUrl'];
    $image_name = $sfEvent['imageName'];

    $queryArgs = array(
      'post_type'   => 'attachment',
      'post_status' => 'inherit',
      'numberposts' => 1,
      'meta_query'  => array(
          array(
              'key'     => 'sf_url',
              'value'   => $image_url
          )
      )
    );
    $attachments = get_posts($queryArgs);
    if (sizeof($attachments) > 0) {
      $attach_id = $attachments[0]->ID;
      return $attach_id;
    }


    $wpFileType = wp_check_filetype($image_name, null);
    $type = $wpFileType['type'];
    if (!$type) {
      trigger_error('!!!Invalid Image type');
      return false;
    }

    trigger_error('!!!New Image');

    $content = file_get_contents( $image_url );
  
    $mirror = wp_upload_bits( $image_name, '', $content );
    $attachment = array(
      'post_title'=> $image_name,
      'post_mime_type' => $type
    );
  
    $attach_id = wp_insert_attachment( $attachment, $mirror['file'], null, true);
  
    require_once(ABSPATH . 'wp-admin/includes/image.php');
    $attach_data = wp_generate_attachment_metadata( $attach_id, $mirror['file'] );
    wp_update_attachment_metadata( $attach_id, $attach_data );

    update_post_meta( $attach_id, 'sf_url', $image_url);
    return $attach_id;
  }

  public function import_events() {
    $sfApi = new Sldg_SF_Api();
    $sfEvents = $sfApi->getEvents();
    $homepageCatSlug = jet_engine()->listings->data->get_option( 'theme-settings::event_homepage_cat' );
    $homepageCat = get_category_by_slug($homepageCatSlug)->term_id;
    $sfEventsMap = array_column($sfEvents, NULL, 'id');
    $wpEvents = get_posts(array(
      'numberposts' => -1,
      'post_type' => 'event',
      'post_status' => 'any',
    ));

    // Remove deleted wp events
    $sf2WpMap = array();
    foreach ($wpEvents as $wpEvent) {
      $sfId = get_post_meta($wpEvent->ID, 'sfEventId', true);
      if (!isset($sfEventsMap[$sfId])) {
        wp_delete_post( $wpEvent->ID);
      } else {
        $sf2WpMap[$sfId] = $wpEvent->ID;
      }
    }

    // Add/Update wp events
    foreach ($sfEvents as $sfEvent) {
      $postDate = NULL;
      $postDateFormat = 'Y-m-d';
      if (isset($sfEvent['date'])) {
        $postDate = $sfEvent['date'];
      }
      if (isset($sfEvent['time'])) {
        $postDate .= ' '.substr($sfEvent['time'], 0, 8);
        $postDateFormat .= ' H:i:s';
      }

      $postId = wp_insert_post( array(
        'ID' => $sf2WpMap[$sfEvent['id']] ?? 0,
        'post_type' => 'event',
        'post_status' => 'publish',
        'post_title' => wp_strip_all_tags($sfEvent['name']),
        'post_content' => wp_strip_all_tags($sfEvent['description']),
        'post_category' => $sfEvent['showInHomepage'] ? array($homepageCat) : array(),
        'meta_input' => array(
          'sfEventId' => $sfEvent['id'],
          'eventCost' => $sfEvent['cost'],
        ),
      ), true);
      if (is_wp_error($postId)) {
        trigger_error($postId->get_error_message());
        continue;
      }

      if (isset($sfEvent['location'])) {
        update_post_meta($postId, 'location', $sfEvent['location']);
      }

      if (isset($sfEvent['date'])) {
        $unixTs = DateTime::createFromFormat($postDateFormat, $postDate)->format('U');
        update_post_meta($postId, 'date', $unixTs);
      }

      if (isset($sfEvent['time'])) {
        update_post_meta($postId, 'time', substr($sfEvent['time'], 0, 5));
      }

      if (isset($sfEvent['viewOnly'])) {
        update_post_meta($postId, 'view_only', $sfEvent['viewOnly']);
      }
      $imageId = $this->uploadRemoteImage($sfEvent);
      if (!empty($imageId)) {
        set_post_thumbnail($postId, $imageId);
      }
    }
    wp_send_json($sfEventsMap);
  }

  function load_elementor_hooks() {
    // Here its safe to include our action class file
    require_once plugin_dir_path( dirname( __FILE__ ) ) . '/includes/elementor/class-sldg-form-action.php';
    require_once plugin_dir_path( dirname( __FILE__ ) ) . '/includes/elementor/class-sf-field.php';
    require_once plugin_dir_path( dirname( __FILE__ ) ) . '/includes/elementor/class-sf-networking-field.php';
    require_once plugin_dir_path( dirname( __FILE__ ) ) . '/includes/elementor/class-sf-volunteer-type-field.php';
    require_once plugin_dir_path( dirname( __FILE__ ) ) . '/includes/elementor/class-sf-volunteer-area-field.php';
    require_once plugin_dir_path( dirname( __FILE__ ) ) . '/includes/elementor/class-sf-mentoring-area-field.php';

    // Instantiate the action class
    $action = new Sldg_Form_Action();

    // Register the action with form widget
    \ElementorPro\Plugin::instance()->modules_manager->get_modules( 'forms' )->add_form_action( $action->get_name(), $action );
  }

  function roi_playground() {
    $sfApi = new Sldg_SF_Api();
    $fields = $sfApi->getEvents();
    wp_send_json( $fields );
  }
}
